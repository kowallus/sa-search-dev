#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=x86_64-w64-mingw32-gcc
CCC=x86_64-w64-mingw32-c++
CXX=x86_64-w64-mingw32-c++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug-bench
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/fe6d8f47/SAHelpers.o \
	${OBJECTDIR}/_ext/55691568/TextIndex.o \
	${OBJECTDIR}/_ext/55691568/saBench.o \
	${OBJECTDIR}/_ext/55691568/testingBench.o \
	${OBJECTDIR}/_ext/fe6d8f47/kAryBench.o \
	${OBJECTDIR}/_ext/74e0902d/common.o \
	${OBJECTDIR}/_ext/74e0902d/hash.o \
	${OBJECTDIR}/_ext/74e0902d/huff.o \
	${OBJECTDIR}/_ext/74e0902d/patterns.o \
	${OBJECTDIR}/_ext/74e0902d/sais.o \
	${OBJECTDIR}/_ext/74e0902d/timer.o \
	${OBJECTDIR}/_ext/74e0902d/xxhash.o \
	${OBJECTDIR}/src/KarySASearchStrategies.o \
	${OBJECTDIR}/src/SASearchStrategyBase.o \
	${OBJECTDIR}/src/SAkaryHelpers.o \
	${OBJECTDIR}/src/testingRoutines.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-mavx
CXXFLAGS=-mavx

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=asmlib/libacof64.lib

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests.exe: asmlib/libacof64.lib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/fe6d8f47/SAHelpers.o: /D/Glony/sa_dev/SADevTests/src/SAHelpers.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/fe6d8f47
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/fe6d8f47/SAHelpers.o /D/Glony/sa_dev/SADevTests/src/SAHelpers.cpp

${OBJECTDIR}/_ext/55691568/TextIndex.o: /D/Glony/sa_dev/SADevTests/src/bench/TextIndex.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/55691568
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/55691568/TextIndex.o /D/Glony/sa_dev/SADevTests/src/bench/TextIndex.cpp

${OBJECTDIR}/_ext/55691568/saBench.o: /D/Glony/sa_dev/SADevTests/src/bench/saBench.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/55691568
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/55691568/saBench.o /D/Glony/sa_dev/SADevTests/src/bench/saBench.cpp

${OBJECTDIR}/_ext/55691568/testingBench.o: /D/Glony/sa_dev/SADevTests/src/bench/testingBench.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/55691568
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/55691568/testingBench.o /D/Glony/sa_dev/SADevTests/src/bench/testingBench.cpp

${OBJECTDIR}/_ext/fe6d8f47/kAryBench.o: /D/Glony/sa_dev/SADevTests/src/kAryBench.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/fe6d8f47
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/fe6d8f47/kAryBench.o /D/Glony/sa_dev/SADevTests/src/kAryBench.cpp

${OBJECTDIR}/_ext/74e0902d/common.o: /D/Glony/sa_dev/SADevTests/src/shared/common.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/common.o /D/Glony/sa_dev/SADevTests/src/shared/common.cpp

${OBJECTDIR}/_ext/74e0902d/hash.o: /D/Glony/sa_dev/SADevTests/src/shared/hash.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/hash.o /D/Glony/sa_dev/SADevTests/src/shared/hash.cpp

${OBJECTDIR}/_ext/74e0902d/huff.o: /D/Glony/sa_dev/SADevTests/src/shared/huff.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/huff.o /D/Glony/sa_dev/SADevTests/src/shared/huff.cpp

${OBJECTDIR}/_ext/74e0902d/patterns.o: /D/Glony/sa_dev/SADevTests/src/shared/patterns.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/patterns.o /D/Glony/sa_dev/SADevTests/src/shared/patterns.cpp

${OBJECTDIR}/_ext/74e0902d/sais.o: /D/Glony/sa_dev/SADevTests/src/shared/sais.c
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/sais.o /D/Glony/sa_dev/SADevTests/src/shared/sais.c

${OBJECTDIR}/_ext/74e0902d/timer.o: /D/Glony/sa_dev/SADevTests/src/shared/timer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/timer.o /D/Glony/sa_dev/SADevTests/src/shared/timer.cpp

${OBJECTDIR}/_ext/74e0902d/xxhash.o: /D/Glony/sa_dev/SADevTests/src/shared/xxhash.c
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/xxhash.o /D/Glony/sa_dev/SADevTests/src/shared/xxhash.c

${OBJECTDIR}/src/KarySASearchStrategies.o: src/KarySASearchStrategies.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/KarySASearchStrategies.o src/KarySASearchStrategies.cpp

${OBJECTDIR}/src/SASearchStrategyBase.o: src/SASearchStrategyBase.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/SASearchStrategyBase.o src/SASearchStrategyBase.cpp

${OBJECTDIR}/src/SAkaryHelpers.o: src/SAkaryHelpers.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/SAkaryHelpers.o src/SAkaryHelpers.cpp

${OBJECTDIR}/src/testingRoutines.o: src/testingRoutines.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DBENCH_BUILD -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/testingRoutines.o src/testingRoutines.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
