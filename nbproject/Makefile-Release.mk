#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=x86_64-w64-mingw32-gcc
CCC=x86_64-w64-mingw32-c++
CXX=x86_64-w64-mingw32-c++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/fe6d8f47/386.o \
	${OBJECTDIR}/_ext/fe6d8f47/SAHelpers.o \
	${OBJECTDIR}/_ext/74e0902d/huff.o \
	${OBJECTDIR}/_ext/fe6d8f47/testingBench.o \
	${OBJECTDIR}/common/xxhash.o \
	${OBJECTDIR}/src/KarySASearchStrategies.o \
	${OBJECTDIR}/src/SASearchStrategyBase.o \
	${OBJECTDIR}/src/SAkaryHelpers.o \
	${OBJECTDIR}/src/searchSA.o \
	${OBJECTDIR}/src/testingRoutines.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=asmlib/libacof64.lib

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests.exe: asmlib/libacof64.lib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sadevtests ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/fe6d8f47/386.o: /D/Glony/sa_dev/SADevTests/src/386.c
	${MKDIR} -p ${OBJECTDIR}/_ext/fe6d8f47
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/fe6d8f47/386.o /D/Glony/sa_dev/SADevTests/src/386.c

${OBJECTDIR}/_ext/fe6d8f47/SAHelpers.o: /D/Glony/sa_dev/SADevTests/src/SAHelpers.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/fe6d8f47
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/fe6d8f47/SAHelpers.o /D/Glony/sa_dev/SADevTests/src/SAHelpers.cpp

${OBJECTDIR}/_ext/74e0902d/huff.o: /D/Glony/sa_dev/SADevTests/src/shared/huff.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/74e0902d
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/74e0902d/huff.o /D/Glony/sa_dev/SADevTests/src/shared/huff.cpp

${OBJECTDIR}/_ext/fe6d8f47/testingBench.o: /D/Glony/sa_dev/SADevTests/src/testingBench.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/fe6d8f47
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/fe6d8f47/testingBench.o /D/Glony/sa_dev/SADevTests/src/testingBench.cpp

${OBJECTDIR}/common/xxhash.o: common/xxhash.cpp
	${MKDIR} -p ${OBJECTDIR}/common
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/xxhash.o common/xxhash.cpp

${OBJECTDIR}/src/KarySASearchStrategies.o: src/KarySASearchStrategies.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/KarySASearchStrategies.o src/KarySASearchStrategies.cpp

${OBJECTDIR}/src/SASearchStrategyBase.o: src/SASearchStrategyBase.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/SASearchStrategyBase.o src/SASearchStrategyBase.cpp

${OBJECTDIR}/src/SAkaryHelpers.o: src/SAkaryHelpers.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/SAkaryHelpers.o src/SAkaryHelpers.cpp

${OBJECTDIR}/src/searchSA.o: src/searchSA.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/searchSA.o src/searchSA.cpp

${OBJECTDIR}/src/testingRoutines.o: src/testingRoutines.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/testingRoutines.o src/testingRoutines.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
