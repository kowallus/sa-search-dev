#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW-Windows
CND_ARTIFACT_NAME_Debug=sadevtests
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW-Windows/sadevtests
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW-Windows/package
CND_PACKAGE_NAME_Debug=sadevtests.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW-Windows/package/sadevtests.tar
# Release configuration
CND_PLATFORM_Release=MinGW-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-Windows
CND_ARTIFACT_NAME_Release=sadevtests
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-Windows/sadevtests
CND_PACKAGE_DIR_Release=dist/Release/MinGW-Windows/package
CND_PACKAGE_NAME_Release=sadevtests.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-Windows/package/sadevtests.tar
# Debug-bench configuration
CND_PLATFORM_Debug-bench=MinGW-Windows
CND_ARTIFACT_DIR_Debug-bench=dist/Debug-bench/MinGW-Windows
CND_ARTIFACT_NAME_Debug-bench=sadevtests
CND_ARTIFACT_PATH_Debug-bench=dist/Debug-bench/MinGW-Windows/sadevtests
CND_PACKAGE_DIR_Debug-bench=dist/Debug-bench/MinGW-Windows/package
CND_PACKAGE_NAME_Debug-bench=sadevtests.tar
CND_PACKAGE_PATH_Debug-bench=dist/Debug-bench/MinGW-Windows/package/sadevtests.tar
# Release-bench configuration
CND_PLATFORM_Release-bench=MinGW-Windows
CND_ARTIFACT_DIR_Release-bench=dist/Release-bench/MinGW-Windows
CND_ARTIFACT_NAME_Release-bench=kAryBench
CND_ARTIFACT_PATH_Release-bench=dist/Release-bench/MinGW-Windows/kAryBench
CND_PACKAGE_DIR_Release-bench=dist/Release-bench/MinGW-Windows/package
CND_PACKAGE_NAME_Release-bench=sadevtests.tar
CND_PACKAGE_PATH_Release-bench=dist/Release-bench/MinGW-Windows/package/sadevtests.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
