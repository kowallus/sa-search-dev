/* 
 * File:   karyLUTSASearchStrategies.h
 * Author: coach
 *
 * Created on 29 maja 2015, 14:04
 */

#ifndef KARYLUTSASEARCHSTRATEGIES_H
#define	KARYLUTSASEARCHSTRATEGIES_H

#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"
#include "SABinarySearchStrategies.h"

#define ELEMENTSINNODE k - 1

using namespace std;

namespace SASearch {

    namespace kary {    
        
        template<int k, int step, class StringComparisonStrategyClass, int plusK = 0>
        class KarySASearch4LUTSA: public KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KarySASearch4LUTSA<k, step, StringComparisonStrategyClass, plusK>> {
        private:
            typedef KarySASearch4LUTSA<k, step, StringComparisonStrategyClass, plusK> ThisType;

            
        public:
            KarySASearch4LUTSA(unsigned char* txt)
            : KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, ThisType>(txt, "SA")
            {}

            ~KarySASearch4LUTSA() { }
            
            inline void setKaryConfiguration(unsigned int* karySA, unsigned int size) {
                this->txtSize = size;
                this->karySA = karySA;
                this->lastNode = (this->txtSize - 1) / (k-1);
            }
            
            inline void setKaryConfiguration(unsigned int* karySA, unsigned int size, unsigned char* ctxt) {
                this->txtSize = size;
                this->karySA = karySA;
                this->lastNode = (this->txtSize - 1) / (k-1);
                this->txt = ctxt;
            }
            
            inline int searchNodeImpl(const unsigned char* pattern, int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + startElement + (step - 1);

                int c;
                for(c = startElement + step - 1; c < ELEMENTSINNODE && j < this->txtSize; c += step) {
                    if (this->isPatternLowerThanSuffix(pattern, patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }
                
                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(pattern, patternLength, j)) {
                            if (j < this->txtSize)
                                i = j;
                            break;
                        }
                        j++;
                    }
                }
                
                return c;
            }
               
        };
        
        template<int k, int step, class StringComparisonStrategyClass, class PreSearchStrategyClass, int plusK, class KarySearchStrategyClass>
        class KaryLUTSASearchTemplate: public SASearchStrategyTemplate<KarySearchStrategyClass, StringComparisonStrategyClass> {
        protected:
            typedef SASearchStrategyInterface<PreSearchStrategyClass> PreSearchStrategy;
            PreSearchStrategy* preSearch;
            typedef SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING> StandardSASearchStrategy;
            StandardSASearchStrategy* stdSearch;
            typedef KarySASearch4LUTSA<k, step, StringComparisonStrategyClass, plusK> KarySASearchStrategy;
            KarySASearchStrategy* karySearch;
            
            bool externalAddress4Data = false;
            unsigned int* karyLUTSA;
            unsigned int* cKaryLUTSA;
            unsigned char* ctxt;
            unsigned int ctxtSize;
            int cLastNode;
            int startLCP;
            
            unsigned char* cPatternPtr; 
            int cPatternLength;
            
        public:
            KaryLUTSASearchTemplate(PreSearchStrategyClass* preSearch, unsigned int *sa, unsigned char* txt, unsigned int txtSize, string name = "unnamed")
            : SASearchStrategyTemplate<KarySearchStrategyClass, StringComparisonStrategyClass>(sa, txt, txtSize, toString(k) + "-ary " + name + plusKString(plusK) + " with " + preSearch->getStrategyName()),
            preSearch((PreSearchStrategy*) preSearch),
            karySearch(new KarySASearchStrategy(txt + preSearch->getLCP()))
            {
                this->sizeInBytes += preSearch->getSizeInBytes();
                this->karyLUTSA = createSAkaryForLUT<k>(sa, txt, txtSize, preSearch);
//                this->karyLUTSA = (unsigned int*) malloc128((txtSize + k) * sizeof(unsigned int));
//                std::copy(sa, sa + txtSize, this->karyLUTSA);
                this->startLCP = preSearch->getLCP();
                ctxt = this->txt + startLCP;

                this->stdSearch = new StandardSASearchStrategy(this->karyLUTSA, txt, txtSize);
                this->stdSearch->setStartLCP(preSearch->getLCP());
            }
            
            inline void useAddress4DataImpl(unsigned int* addr) {
                this->stdSearch->useAddress4Data(addr);
                this->preSearch->useAddress4Data(addr);
                #ifdef ALIGN_MEMORY
                free2n(this->karyLUTSA);
                #else
                delete[] this->karyLUTSA;
                #endif
                this->karyLUTSA = addr;
                externalAddress4Data = true;
            }
            
            // for use with karyLUTLCPSA strategies
            KaryLUTSASearchTemplate(PreSearchStrategyClass* preSearch, unsigned char* txt, unsigned int txtSize, string name = "unnamed")
            : SASearchStrategyTemplate<KarySearchStrategyClass, StringComparisonStrategyClass>(NULL, txt, txtSize, toString(k) + "-ary " + name + plusKString(plusK) + " with " + preSearch->getStrategyName()),
            preSearch((PreSearchStrategy*) preSearch),
            karySearch(new KarySASearchStrategy(txt + preSearch->getLCP()))
            {
                this->sizeInBytes += preSearch->getSizeInBytes();
                this->startLCP = preSearch->getLCP();
                ctxt = this->txt + startLCP;
            }
            
            ~KaryLUTSASearchTemplate() {
                if (!externalAddress4Data) {
                    #ifdef ALIGN_MEMORY
                    free2n(this->karyLUTSA);
                    #else
                    delete[] this->karyLUTSA;
                    #endif
                }
                
                karySearch->nullKaryConfiguration();
                delete karySearch;
            }
            
            string getParamsString() { 
                return this->preSearch->getParamsString() + " " + toString(k) + " " + toString(step) + " " + toString(plusK);
            }
            
            bool isValidFor(unsigned char* pattern, int patternLength) { 
                return preSearch->isValidFor(pattern, patternLength);
            }

            inline bool isPatternLowerThanSuffix(unsigned int suffixIndex) {
                // patternLength + 1 is a tweak to use with strncmp strategies
                return this->strcmpStrategy(cPatternPtr, this->ctxt + this->cKaryLUTSA[suffixIndex], cPatternLength + 1) <= 0;
            }
            
            inline bool incrementKaryIndex(unsigned int& i) {
                int c = numOfElementInNode<k>(i);
                unsigned int node = index2node<k>(i);

                unsigned int cNode = childNode<k>(node, c + 1);
                unsigned int j = node2index<k>(cNode);
                if (j < this->ctxtSize) {
                    do {
                        i = j;
                        cNode = childNode<k>(cNode, 0);
                        j = node2index<k>(cNode);
                    } while (j < this->ctxtSize);
                    return true;
                }

                if (c < k - 2) {
                    i++;
                    if (i < this->ctxtSize)
                        return true; 
                }

                while (node > 0) {
                    c = parentsChildNum<k>(node);
                    node = parentNode<k>(node);
                    i = node2index<k>(node) + c;
                    if (c < k - 1)
                        return true;
                };

                i = this->ctxtSize;
                return false;
            }
        
            inline void findFirstLinearly(unsigned int &i, unsigned int end) 
            { 
                int j;
                for(j = i; j < end; j++) 
                    if (this->isPatternLowerThanSuffix(j)) {
                        i = j;
                        return;
                    }
                i = end;
            }
            
            inline void linearSearchOnStandardLayout(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                cKaryLUTSA = karyLUTSA + beg;
                cPatternPtr = pattern + startLCP;
                this->cPatternLength = patternLength - startLCP;
                unsigned int i = 0;
                findFirstLinearly(i, ctxtSize);
                if (!incrementPattern(pattern, patternLength)) {
                    decrementPattern(pattern, patternLength);
                    return;
                };
                findFirstLinearly(end = i, ctxtSize);
                end += beg;
                beg += i;
                decrementPattern(pattern, patternLength);
            }
            
            inline void searchKaryLayout(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                karySearch->setKaryConfiguration(this->karyLUTSA + beg, end - beg);
                this->cPatternPtr = pattern + this->startLCP;
                this->cPatternLength = patternLength - startLCP;
                unsigned int partBeg = beg;
                karySearch->searchImpl(this->cPatternPtr, this->cPatternLength, beg, end);                
                beg += partBeg;
                end += partBeg;
            }
            
            inline int localQueryCount(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {     
                if (ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH)
                        linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    else
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    return end - beg;
                }
                
                karySearch->setKaryConfiguration(this->karyLUTSA + beg, ctxtSize);
                karySearch->searchImpl(pattern + this->startLCP, patternLength - startLCP, beg, end);                
                
                return karySearch->resultCount(beg, end);
            }

            void localQueryLocate(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                if (ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH)
                        linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    else
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    res.insert(res.end(), this->karyLUTSA + beg, this->karyLUTSA + end);
                    return;
                }
                
                karySearch->setKaryConfiguration(this->karyLUTSA + beg, ctxtSize);
                karySearch->searchImpl(pattern + this->startLCP, patternLength - startLCP, beg, end);
                karySearch->resultLocate(beg, end, res);
            }
            
            inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
    /*            preSearch->search(pattern, patternLength, beg, end); 
                this->stdSearch->search(pattern, patternLength, beg, end);
                return end - beg;
  */          
                preSearch->search(pattern, patternLength, beg, end); 
                ctxtSize = end - beg;
                return this->localQueryCount(pattern, patternLength, beg, end);
            }

            void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                preSearch->search(pattern, patternLength, beg, end); 
                ctxtSize = end - beg;
                
                this->localQueryLocate(pattern, patternLength, beg, end, res);
            }
            
            virtual unsigned int getSuffixOffset(unsigned int index) { return this->karyLUTSA[index]; };
            
            
            inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                preSearch->search(pattern, patternLength, beg, end); 
                
                ctxtSize = end - beg;
                
                if (ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH)
                        linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    else
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    return;
                }
                
                searchKaryLayout(pattern, patternLength, beg, end);
            }
            
            int resultCountImpl(unsigned int beg, unsigned int end) {
                
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + getSuffixOffset(beg), startLCP);
                unsigned int begIndex, endIndex;
                preSearch->search(prefixPtr, startLCP, begIndex, endIndex);

                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT)
                    return end - beg;
                
                karySearch->setKaryConfiguration(0, endIndex - begIndex);
                return karySearch->resultCount(beg - begIndex, end - begIndex);
            }
            
            void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + getSuffixOffset(beg), startLCP);
                unsigned int begIndex, endIndex;
                preSearch->search(prefixPtr, startLCP, begIndex, endIndex);
                
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT) {
                    res.insert(res.end(), this->karyLUTSA + beg, this->karyLUTSA + end);
                    return;
                }
                
                karySearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                karySearch->resultLocate(beg - begIndex, end - begIndex, res);
            }
                
            void getIndexRanksImpl(unsigned int& beg, unsigned int& end) {
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + getSuffixOffset(beg), startLCP);
                unsigned int begIndex, endIndex;
                preSearch->search(prefixPtr, startLCP, begIndex, endIndex);
                
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT)
                    return;
                
                beg = begIndex + getKaryIndexRank<k>(beg - begIndex, endIndex - begIndex, index2node<k>(endIndex - begIndex));
                end = begIndex + getKaryIndexRank<k>(end - begIndex, endIndex - begIndex, index2node<k>(endIndex - begIndex));
            }
            
            unsigned int getIndexRankImpl(unsigned int i) {
                unsigned int beg, end;
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + getSuffixOffset(i), startLCP);
                preSearch->search(prefixPtr, startLCP, beg, end);
                
                return beg + getKaryIndexRank<k>(i - beg, end - beg, index2node<k>(end - beg));
            }
            
            unsigned int* getKaryLUTSA() { return this->karyLUTSA; }
        };
        
        template<int k, int step, class StringComparisonStrategyClass, class PreSearchStrategyClass, int plusK = 0>
        class KaryLUTSASearch: public KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, plusK, KaryLUTSASearch<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, plusK>> {
        public:
            KaryLUTSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize, PreSearchStrategyClass* preSearch)
            : KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, plusK, 
                    KaryLUTSASearch<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, plusK>>(preSearch, sa, txt, txtSize, "SA")
            {
            }
        };
        
        template<int k, int step, class StringComparisonStrategyClass, class PreSearchStrategyWithSATwistClass, int plusK = 0>
        class KaryLUTWithSATwistSearch: public KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, plusK, KaryLUTWithSATwistSearch<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, plusK>> {
            private:
            typedef KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, plusK, KaryLUTWithSATwistSearch<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, plusK>> BaseClass;
            
        public:
            KaryLUTWithSATwistSearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize, PreSearchStrategyWithSATwistClass* preSearch)
            : BaseClass(preSearch, sa, txt, txtSize, "SA")
            {
            }

            inline void searchKaryLayout(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                this->karySearch->setKaryConfiguration(this->karyLUTSA + beg, end - beg);
                this->cPatternPtr = pattern + this->startLCP;
                this->cPatternLength = patternLength - this->startLCP;
                unsigned int partBeg = beg;
                this->karySearch->searchImpl(this->cPatternPtr, this->cPatternLength, beg, end);                
                beg += partBeg;
                end += partBeg;
            }
            
            inline int localQueryCount(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {     
                this->startLCP = (((PreSearchStrategyWithSATwistClass*) this->preSearch)->getCurrentLCP());
                if (this->ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (this->ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH) {
                        this->ctxt = this->txt + this->startLCP;
                        this->linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    } else {
                        ((typename BaseClass::StandardSASearchStrategy*) (this->stdSearch))->setStartLCP(this->startLCP);
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    }
                    return end - beg;
                }
                
                this->karySearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize, this->txt + this->startLCP);
                this->karySearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);                

                return this->karySearch->resultCount(beg, end);
            }

            void localQueryLocate(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                this->startLCP = (((PreSearchStrategyWithSATwistClass*) this->preSearch)->getCurrentLCP());
                if (this->ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (this->ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH) {
                        this->ctxt = this->txt + this->startLCP;
                        this->linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    }else {
                        ((typename BaseClass::StandardSASearchStrategy*) (this->stdSearch))->setStartLCP(this->startLCP);
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    }
                    res.insert(res.end(), this->karyLUTSA + beg, this->karyLUTSA + end);
                    return;
                }
                
                this->karySearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize, this->txt + this->startLCP);
                this->karySearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);
                this->karySearch->resultLocate(beg, end, res);
            }

            inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
                this->preSearch->search(pattern, patternLength, beg, end); 
                this->ctxtSize = end - beg;
                return this->localQueryCount(pattern, patternLength, beg, end);
            }

            void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                this->preSearch->search(pattern, patternLength, beg, end); 
                this->ctxtSize = end - beg;
                
                this->localQueryLocate(pattern, patternLength, beg, end, res);
            }
   
            
        };
   
    }
}

#endif	/* KARYLUTSASEARCHSTRATEGIES_H */

