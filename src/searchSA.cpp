#include "testingRoutines.h"
#include "testPlainKarySuite.h"
#include "testKaryLCPSASuite.h"
#include "testKaryLUTSASuite.h"

template class SATwoStageSearchStrategy<NulSearchStrategy, NulSearchStrategy>;
template class KarySASearch<9, 1, AsmLibStrCmp>;

void binarySearchVariantsTestsSet();
void testDifferentTxtSizes();

int main(int argc, char* argv[]) {
    if (argc < 4) {
            cout << "Usage syntax:"<< endl;
            cout << "searchSA myFile patL patCat" << endl;
            cout << "where:" << endl;
            cout << "\tpatL - length of pattern"<< endl;
            cout << "\tpatCat - catalog with patterns" << endl;
            return 1;
    }

    initTestingEnvironment(argv[1], argv[3], argv[2]);

//    testDifferentTxtSizes();

//    binarySearchVariantsTestsSet();

    
//    karyVariantsPlannedTestsSet<AsmLibStrCmp, 0>();

//    karyLCPSAVariantsPlannedTestsSet<AsmLibStrCmp, 0>();
/*    
    karyLCPSAVariantsDiffPrefixPlannedTestsSet<AsmLibStrCmp, 0>();
    
    karyLCPSAVariantsSameSizePlannedTestsSet<AsmLibStrCmp, 0>();
    
    karyVariantsPlusKPlannedTestsSet<AsmLibStrCmp>();
    karyLCPSAVariantsPlusKPlannedTestsSet<AsmLibStrCmp>();
/**/    
    
    
//    karyLCPLUTSAVariantsDiffKPlannedTestsSet<33, 5, AsmLibStrCmp, 0>();
    
    karyLUTSAVariantsPlannedTestsSet<AsmLibStrCmp, 0>();
    
//    karyLCPLUTSAVariantsDiffPrefixPlannedTestsSet<5, 1, AsmLibStrCmp, 0>();
//    karyLCPLUTSAVariantsDiffPrefixPlannedTestsSet<33, 5, AsmLibStrCmp, 0>();
    
//    karyLCPLUTSAVariantsDiffKPlannedTestsSet<5, 1, AsmLibStrCmp, 0>();
/**/    
//    variousPlainKaryVariantsTestsSet<AsmLibStrCmp>();

//    variousKaryLCPSAVariantsTestsSet<AsmLibStrCmp>();
    
//    variousKaryLUTSAVariantsTestsSet<AsmLibStrCmp>();
    
//    preliminaryKaryVariantsTestsSet<BSwap32StrNECmp>();
    
//    preliminaryKaryLCPSAVariantsTestsSet<BSwap32StrNECmp>();
    
//    preliminaryKaryLUTSAVariantsTestsSet<BSwap32StrNECmp>();

    bestVariantsRanking();

    disposeTestingEnvironment();    
}

void binarySearchVariantsTestsSet() {

    cout << endl << "Comparison using standard C strcmp and strncmp implementations." << endl;

    testSASearchStrategy<SASimpleBinarySearch<StandardCStrCmp>>();
    testSASearchStrategy<SASmartBinarySearchNCmp<StandardCStrNCmp>>();

    cout << endl << "Comparison using various strcmp and strncmp implementations." << endl;

    testSASearchStrategy<SASimpleBinarySearch<BSwap32StrNECmp>>();

    testSASearchStrategy<SASmartBinarySearchNCmp<AsmLibModStrNCmp>>();
    testSASearchStrategy<SASmartBinarySearchNCmp<LCPStrNCmp32>>();

    testSASearchStrategy<SASmartLCPBinarySearch<LCPStrNCmp32>>();
    
    cout << endl << "The best combinations." << endl;
    
    testSASearchStrategy<SASimpleBinarySearch<BSwap32StrNECmp, -2>>();
    testSASearchStrategy<SASimpleBinarySearch<BSwap32StrNECmp, PLUSK_DOUBLING>>();
    testSASearchStrategy<SASimpleBinarySearch<AsmLibStrCmp, -2>>();
    testSASearchStrategy<SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>();
    testSASearchStrategy<SASmartLCPBinarySearch<LCPStrNCmp32, -2>>();
    testSASearchStrategy<SASmartBinarySearchNCmp<LCPStrNCmp32, -2>>();
    testSASearchStrategy<SASmartBinarySearchNCmp<AsmLibModStrNCmp, -2>>();
    testSASearchStrategy<SASmartBinarySearchNCmp<AsmLibModStrNCmp, -2>>();
    
    cout << endl << "The best combinations with LCP3." << endl;
    
    testSATwoStageSearchStrategy<SALookupOn3Symbols, SASmartLCPBinarySearch<LCPStrNCmp32>>(saLUT3);
    testSATwoStageSearchStrategy<SALookupOn3Symbols, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saLUT3);
    testSATwoStageSearchStrategy<SALookupOn3Symbols, SASmartLCPBinarySearch<LCPStrNCmp32, -2>>(saLUT3);    
}

void testDifferentTxtSizes() {
    int txtSizeTmp = txtSize;
    
    for (int i = 0; i < 5; i++) {
        cout << endl << "TXTSIZE = " << txtSize;
        refTime = -1;
        refSize = -1;
        cout << endl << "Reference implementation." << endl;
        refSize = txtSize;
        refTime = testSASearchStrategy<SASimpleBinarySearch<AsmLibStrCmp>>();

        testSASearchStrategy<SASimpleBinarySearch<BSwap32StrNECmp, PLUSK_DOUBLING>>();
        testSASearchStrategy<KarySASearch<3, 1, BSwap32StrNECmp, 0>>();
//            testSASearchStrategy<KarySASearch<5, 1, BSwap32StrNECmp, 0>>();
        txtSize /= 2;
    }
    
    txtSize = txtSizeTmp;
}