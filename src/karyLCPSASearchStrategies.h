/* 
 * File:   karyLCPSASearchStrategies.h
 * Author: coach
 *
 * Created on 3 czerwca 2015, 17:20
 */

#ifndef KARYLCPSASEARCHSTRATEGIES_H
#define	KARYLCPSASEARCHSTRATEGIES_H

#include <string>
#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"
#include "karySASearchStrategies.h"

//#define LCP_STATS_ON

using namespace std;

namespace SASearch {

    namespace kary {    
 
        template<int k, int lcpLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass>
        class LCPSASearchCoordinator {
        protected:
            unsigned char* txtStart;
            string searchStrategyName;
            
            typedef StringComparisonStrategyInterface<StrNCmp32bitsComparisonStrategyClass> StrNCmpComparisionStrategy;
            StrNCmpComparisionStrategy* strncmpCompare;

            prefixarray<prefixLengthInUints>* prefixes;
            unsigned int prefixesCount;
            unsigned int prefixesSizeInUints;
            unsigned int lastNodeWithPrefixes;
            
            prefixarray<prefixLengthInUints> minPrefix;
            prefixarray<prefixLengthInUints> maxPrefix;
            
            // lcp
            unsigned int lcp;
            unsigned int lLCP;
            unsigned int rLCP;
            unsigned char* lPrefix;
            unsigned char* rPrefix;
            const unsigned char* patternPtr;
            
            const unsigned char* ptrn;
            unsigned char* stxt;
            
            // statistics
            int statsCache[lcpLevels] = { };
            int statsMisses[lcpLevels] = { };
            int statsLCP[lcpLevels] = { };
            int statsCount[lcpLevels] = { };
                        
        public:

            
            LCPSASearchCoordinator(unsigned int *karySA, unsigned char* txt, unsigned int txtSize, string name = "unnamed")
            :       txtStart(txt),
                    searchStrategyName(name),
                    strncmpCompare(StrNCmp32bitsComparisonStrategyClass::getInstance()),
                    prefixes(createPrefixesCache<k, prefixLengthInUints>(karySA, txt, txtSize, lcpLevels)) 
            {
                if (lcpLevels < karyLevels<k>(txtSize)) 
                    this->prefixesCount = pow(k, lcpLevels) - 1;
                else
                    this->prefixesCount = txtSize;
                        
                prefixesSizeInUints = this->prefixesCount * prefixLengthInUints * sizeof(unsigned int);
                this->lastNodeWithPrefixes = (this->prefixesCount - 1) / (k - 1);
                
                for(int i = 0; i < prefixLengthInUints * sizeof(unsigned int); i++) {
                    minPrefix[i] = 0;
                    maxPrefix[i] = 255;
                }
            }

            // for use with karyKULLCPSA strategies
            LCPSASearchCoordinator(unsigned char* txt, string name = "unnamed")
            :       txtStart(txt),
                    searchStrategyName(name),
                    strncmpCompare(StrNCmp32bitsComparisonStrategyClass::getInstance()),
                    prefixes(NULL) 
            {
                this->prefixesCount = pow(k, lcpLevels) - 1;
                prefixesSizeInUints = this->prefixesCount * prefixLengthInUints;
                this->lastNodeWithPrefixes = (this->prefixesCount - 1) / (k - 1);
                
                for(int i = 0; i < prefixLengthInUints * sizeof(unsigned int); i++) {
                    minPrefix[i] = 0;
                    maxPrefix[i] = 255;
                }
            }

            unsigned int getPrefixesCount() {
                return prefixesCount;
            }
            
            unsigned int getPrefixesSizeInBytes() {
                return prefixesCount * prefixLengthInUints * sizeof(unsigned int);
            }
            
            ~LCPSASearchCoordinator() {
                #ifdef ALIGN_MEMORY
                free2n(prefixes);
                #else
                delete[] prefixes;
                #endif
            } 
            
            inline void lcpInit(const unsigned char* pattern, int patternLength) {
                lcp = 0;
                lLCP = 0;
                rLCP = 0;
                lPrefix = minPrefix;
                rPrefix = maxPrefix;
                patternPtr = pattern;
                ptrn = pattern;
                stxt = this->txtStart;
            }
            
            inline void lcpDisable() {
                ptrn -= lcp;
                lcp = 0;
                stxt = this->txtStart;
            }
            
            inline void lcpUpdate(int child, unsigned char* prevPrefix, unsigned char* currentPrefix) {
                if (child > 0) {
                    lPrefix = prevPrefix;
                    lLCP = lcp;
                    if (child < k - 1) {
                        rPrefix = currentPrefix;
                        rLCP = lcp;
                        lcp += strlcp(lPrefix, rPrefix, prefixLengthInUints * sizeof(unsigned int));
                    } else {
                        int lcpShift = lcp - rLCP;
                        lcp += strlcp(lPrefix, rPrefix + lcpShift, prefixLengthInUints * sizeof(unsigned int) - lcpShift);
                    }
                } else {
                    rPrefix = currentPrefix;
                    rLCP = lcp;
                    int lcpShift = lcp - lLCP;
                    lcp += strlcp(lPrefix + lcpShift, rPrefix, prefixLengthInUints * sizeof(unsigned int) - lcpShift);
                }
                ptrn = patternPtr + lcp;
                stxt = this->txtStart + lcp;
            }
            
            void updateStats(int cmpres, int level) {
                if (cmpres != 0)
                    statsCache[level]++;
                else
                    statsMisses[level]++;
                statsLCP[level] += lcp;
                statsCount[level]++;
            }
            
            void printStats() {
                cout << searchStrategyName << " prefix cache statistics: " << endl;
                for (int i = 0; i < lcpLevels; i++) 
                    cout << i << ".\tcache " << statsCache[i] << "\tmisses " << statsMisses[i] << "\tlcp " << (double) statsLCP[i] / statsCount[i] << endl;
            }
        };
   
        template<int k, int step, int lcpLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK, class KaryLCPSASearchClass>
        class KaryLCPSASearchTemplate: public KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KaryLCPSASearchClass>,
            public LCPSASearchCoordinator<k, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass> {
            
        public:
            KaryLCPSASearchTemplate(unsigned int *sa, unsigned char* txt, unsigned int txtSize, string name = "unnamed")
            :       KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KaryLCPSASearchClass>(sa, txt, txtSize, "[LCP: " + toString(lcpLevels) + "-levels " + toString(prefixLengthInUints * sizeof(unsigned int)) + "-chars (" + StrNCmp32bitsComparisonStrategyClass::getInstance()->getStrategyName() + ")] " + name),
                    LCPSASearchCoordinator<k, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass>(this->karySA, txt, txtSize, this->name)
            {
                this->sizeInBytes += getKaryPrefixesSizeInBytes<k>(prefixLengthInUints * sizeof(unsigned int), lcpLevels, txtSize);
            }

            inline void useAddress4DataImpl(unsigned int* addr) {
                std::copy(this->karySA, this->karySA + this->txtSize, addr);
                #ifdef ALIGN_MEMORY
                free2n(this->karySA);
                #else
                delete[] this->karySA;
                #endif
                this->karySA = addr;
                this->externalAddress4Data = true;
            }
            
            // for use with karyKULLCPSA strategies
            KaryLCPSASearchTemplate(unsigned char* txt, string name = "unnamed")
            :       KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KaryLCPSASearchClass>(txt, "[LCP: " + toString(lcpLevels) + "-levels " + toString(prefixLengthInUints * sizeof(unsigned int)) + "-chars (" + StrNCmp32bitsComparisonStrategyClass::getInstance()->getStrategyName() + ")] " + name),
                    LCPSASearchCoordinator<k, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass>(txt, this->name)
            {
            }

            inline void setKaryConfiguration(unsigned int* karyLCPSA, unsigned int size) {
                this->txtSize = size - this->prefixesSizeInUints;
                this->prefixes = (prefixarray<prefixLengthInUints>*) (karyLCPSA + this->txtSize);
                this->karySA = karyLCPSA;
                this->lastNode = (this->txtSize - 1) / (k-1);
            }
            
            inline void setKaryConfiguration(unsigned int* karyLCPSA, unsigned int size, unsigned char* ctxt) {
                this->txtSize = size - this->prefixesSizeInUints;
                this->prefixes = (prefixarray<prefixLengthInUints>*) (karyLCPSA + this->txtSize);
                this->karySA = karyLCPSA;
                this->lastNode = (this->txtSize - 1) / (k-1);
                this->txtStart = ctxt;
            }
            
            
            inline void index2suffixIndex(unsigned int &index) {}
            
            inline void suffixIndex2index(unsigned int &suffixIndex) {}
            
            void indexRankingCorrection() {
                while (this->karySA[this->txtSize - 1] == this->karySA[this->txtSize - 2])
                    this->txtSize--;
            }
            
            void nullKaryConfiguration() {
                this->prefixes = NULL;
                this->karySA = NULL;
            }
            
            unsigned int* createLCPSAkary(unsigned int *sa, unsigned int saSize, unsigned int* dest, int currentLcp = 0) { 
                unsigned int* karySA = createSAkarySimple<k>(sa, saSize);
                int size = (((saSize - 1) / (k - 1)) + 1) * (k - 1);
                char* prefixes = (char*) createPrefixesCache<k, prefixLengthInUints>(karySA, this->txt + currentLcp, saSize, lcpLevels);
                                
                std::copy(karySA, karySA + size, dest);
                std::copy((unsigned int*) prefixes, (unsigned int*) prefixes + this->prefixesSizeInUints, dest + size);

                #ifdef ALIGN_MEMORY
                free2n(karySA);
                free2n(prefixes);
                #else
                delete[] karySA;
                delete[] prefixes;
                #endif
            }
            
            static const unsigned int kValue = k;
            static const unsigned int plusKvalue = plusK;
            
            ~KaryLCPSASearchTemplate() {
            }
            
            string getParamsString() { 
                return toString(k) + " " + toString(step) + " " + toString(plusK) + " " + toString(lcpLevels) + " " + toString(prefixLengthInUints);
            }
            
            inline bool isPatternLowerThanSuffix(int patternLength, unsigned int* sufOffset) {
                return (this->strcmpStrategy(this->ptrn, this->stxt + *sufOffset, patternLength - this->lcp + 1) <= 0);
            }
            
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned char* prefixPtr, unsigned int *sufOffset) {
                int cmpres = this->strncmpCompare->compare(this->ptrn, prefixPtr, prefixLengthInUints * sizeof(unsigned int));
                #ifdef LCP_STATS_ON
                this->updateStats(cmpres, level);
                #endif
                return (cmpres < 0 || (cmpres == 0 && this->strcmpStrategy(this->ptrn + prefixLengthInUints * sizeof(unsigned int), this->stxt + *sufOffset + prefixLengthInUints * sizeof(unsigned int), patternLength - this->lcp - prefixLengthInUints * sizeof(unsigned int) + 1) <= 0));
            }   
            
            inline bool isPatternLowerThanSuffix(int patternLength, unsigned int suffixIndex) {
                return isPatternLowerThanSuffix(patternLength, this->karySA + suffixIndex);
            }
       
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned int suffixIndex) {
                return isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->prefixes[suffixIndex], this->karySA + suffixIndex);
            }
            
            inline int searchNode(int patternLength, unsigned int &i, unsigned int node, int startElement = 0) { return static_cast<KaryLCPSASearchClass*>(this)->searchNodeImpl(patternLength, i, node, startElement); }
            
            inline int searchNodeUsingPrefixesCache(int patternLength, unsigned int &i, unsigned int node, int startElement = 0) { return static_cast<KaryLCPSASearchClass*>(this)->searchNodeUsingPrefixesCacheImpl(patternLength, i, node, startElement); }
            
            inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                unsigned int node = 0;
                beg = this->txtSize;

                static_cast<KaryLCPSASearchClass*>(this)->lcpInit(pattern, patternLength);     
                
                while (node <= this->lastNode)
                    if (node <= this->lastNodeWithPrefixes)
                        node = childNode<k>(node, this->searchNodeUsingPrefixesCache(patternLength, beg, node, 0));
                    else 
                        node = childNode<k>(node, this->searchNode(patternLength, beg, node, 0));

                if (!incrementPattern(pattern, patternLength)) {
                    decrementPattern(pattern, patternLength);
                    return;
                };
                int c;
                end = beg;
                unsigned int i = end;
                this->lcpDisable();
                
                if (plusK == 0) {
                    unsigned int endNode = index2node<k>(end);

                    while (true) {
                        if (isPatternLowerThanSuffix(patternLength, end))
                            break;
                        i = end + 1;
                        node = endNode;
                        if (!this->levelUpKaryNode(endNode, c)) {
                            end = this->txtSize;
                            break;
                        }
                        end = node2index<k>(endNode) + c;
                    }

                    if (end == beg) {
                        decrementPattern(pattern, patternLength);
                        return;
                    }

                } else {
                    if (this->findPlusKOccurrence(pattern, patternLength, i, plusK)) {
                        end = i;
                        decrementPattern(pattern, patternLength);
                        return;
                    }

                    unsigned int endNode = index2node<k>(end);
                    while (true) {
                        i = end + 1;
                        node = endNode;
                        if (!this->levelUpKaryNode(endNode, c)) {
                            end = this->txtSize;
                            break;
                        }
                        end = node2index<k>(endNode) + c;
                        if (isPatternLowerThanSuffix(patternLength, end))
                            break;
                    }

                }

                c = numOfElementInNode<k>(i);

                node = childNode<k>(node, searchNode(patternLength, end, node, c));
                while (node <= this->lastNode)
                    node = childNode<k>(node, searchNode(patternLength, end, node, 0));
                decrementPattern(pattern, patternLength);
            }
        };
        
        template<int k, int step, int lcpLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK = 0>
        class KaryLCPSASearch: public KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, KaryLCPSASearch<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK>> {
        private:
            typedef KaryLCPSASearch<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK> ThisType;

        public:
            KaryLCPSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "LCPSA")
            { }

            KaryLCPSASearch(unsigned char* txt)
            : KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(txt, "LCPSA")
            { }
            
            ~KaryLCPSASearch() {
            }
            
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + startElement + (step - 1);

                int c;
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }
                
                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;

                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                this->lcpUpdate(c, this->prefixes[j - 1], this->prefixes[j]);
                return c;
            }
            
            inline int searchNodeImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + startElement + (step - 1);
                int c;
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                return c;
            }
               
        };
    }
}

#endif	/* KARYLCPSASEARCHSTRATEGIES_H */

