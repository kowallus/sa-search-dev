/* 
 * File:   SASearchStrategyBase.h
 * Author: coach
 *
 * Created on 7 marca 2015, 13:14
 */

#ifndef SASEARCHSTRATEGYBASE_H
#define	SASEARCHSTRATEGYBASE_H

#include <vector>
#include <string>
#include "SAHelpers.h"

#define PLUSK_DOUBLING -3

using namespace std;

namespace SASearch {
    
    string plusKString(int plusK);
    
    template<class SASearchStrategyClass>
    class SASearchStrategyInterface
    {
    protected:

        string name;
        unsigned int sizeInBytes;

    public:

        SASearchStrategyInterface(string name = "unnamed", unsigned int sizeInBytes = 0) 
        : name(name), sizeInBytes(sizeInBytes)
        {}

        virtual ~SASearchStrategyInterface() {}

        inline void search(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) { static_cast<SASearchStrategyClass*>(this)->searchImpl(pattern, patternLength, beg, end); }

        inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
            this->search(pattern, patternLength, beg, end);
            return this->resultCount(beg, end);
        }
        
        inline void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
            this->search(pattern, patternLength, beg, end);
            this->resultLocate(beg, end, res);
        }
        
        string getStrategyName() { return name; }

        virtual string getParamsString() { return ""; }
        
        virtual unsigned int getSizeInBytes() { return sizeInBytes; }

        virtual bool isValidFor(unsigned char* pattern, int patternLength) { return true; }
        
        unsigned int getIndexRankImpl(unsigned int i) { return i; }
        
        void getIndexRanksImpl(unsigned int& beg, unsigned int& end) {
            beg = this->getIndexRank(beg);
            end = this->getIndexRank(end);            
        }
 
        inline unsigned int getIndexRank(unsigned int i) { return static_cast<SASearchStrategyClass*>(this)->getIndexRankImpl(i); }
        
        inline void getIndexRanks(unsigned int& beg, unsigned int& end) { static_cast<SASearchStrategyClass*>(this)->getIndexRanksImpl(beg, end); }
        
        inline int queryCount(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) { return static_cast<SASearchStrategyClass*>(this)->queryCountImpl(pattern, patternLength, beg, end); }
        
        inline void queryLocate(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) { static_cast<SASearchStrategyClass*>(this)->queryLocateImpl(pattern, patternLength, beg, end, res); }
        
        inline int resultCount(unsigned int beg, unsigned int end) { return static_cast<SASearchStrategyClass*>(this)->resultCountImpl(beg, end); }
        
        inline void resultLocate(unsigned int beg, unsigned int end, vector<unsigned int>& res) { static_cast<SASearchStrategyClass*>(this)->resultLocateImpl(beg, end, res); }
        
        virtual unsigned int getSuffixOffset(unsigned int index) { return 0; }
        
        bool isNegative(unsigned int beg, unsigned int end) {
            getIndexRanks(beg, end);
            return (beg == 0) || (end - beg) == 0; 
        }

        inline void useAddress4DataImpl(unsigned int* addr) {}
        
        inline void useAddress4Data(unsigned int* addr) { static_cast<SASearchStrategyClass*>(this)->useAddress4DataImpl(addr); } 
    };
    
    class NulSearchStrategy: public SASearchStrategyInterface<NulSearchStrategy> 
    {        
    public:

        inline void searchImpl(unsigned char* &pattern, int patternLength, unsigned int &beg, unsigned int &end) {}

        inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {};
        void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {}
        
        inline int resultCountImpl(unsigned int beg, unsigned int end) {};
        void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {}        
        
        inline void useAddress4DataImpl(unsigned int* addr) {};
        
        inline int getLCP() { return 0; }
        
        inline void setStartLCP(int preSearchLCP) { }
    };
    
    template<class StringComparisonClass>
    class StringComparisonStrategyInterface
    {
    private:
        static StringComparisonClass* instance;
        string name;

    public:

        StringComparisonStrategyInterface(string name = "unnamed") 
        : name(name)
        {}

        virtual ~StringComparisonStrategyInterface() {}

        inline int compare(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) { return static_cast<StringComparisonClass*>(this)->compareImpl(pattern, suffix, patternLength); }

        string getStrategyName() { return name; };
        
        static StringComparisonClass* getInstance() {
            if (instance == 0)
                instance = new StringComparisonClass();
            return instance;
        } 
    };
    
    template<class StringComparisonClass> 
    StringComparisonClass* StringComparisonStrategyInterface<StringComparisonClass>::instance = 0;
    
    template<class SASearchStrategyClass, class StringComparisonStrategyClass>
    class SASearchStrategyTemplate: public SASearchStrategyInterface<SASearchStrategyClass> {

    private:
        typedef StringComparisonStrategyInterface<StringComparisonStrategyClass> StringComparisionStrategy;

        StringComparisionStrategy* stringCompare;

    protected:
        unsigned int *sa;
        unsigned char* txt;
        unsigned int txtSize;
        
        inline int strcmpStrategy(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) { return stringCompare->compare(pattern, suffix, patternLength); }
        
    public:
        SASearchStrategyTemplate(unsigned int *sa, 
                unsigned char* txt, unsigned int txtSize, string name = "unnamed")
        : SASearchStrategyInterface<SASearchStrategyClass>(name + " (" + StringComparisonStrategyClass::getInstance()->getStrategyName() + ")", txtSize * (sizeof(char) + sizeof(unsigned int))),
                stringCompare(StringComparisonStrategyClass::getInstance()),
                sa(sa),
                txt(txt),
                txtSize(txtSize)
        {}
        
        unsigned int getSuffixOffset(unsigned int index) { return this->sa[index]; };
        
        inline int resultCountImpl(unsigned int beg, unsigned int end) {
            return end - beg;
        }
        
        void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
            res.insert(res.end(), sa + beg, sa + end);
        }
        
        inline void useAddress4DataImpl(unsigned int* addr) {
            std::copy(sa, sa + txtSize, addr);
            sa = addr;
        }
        
        virtual ~SASearchStrategyTemplate() {}
    };
    
    template<class PreSearchStrategyClass, class BinarySearchStrategyClass>
    class SATwoStageSearchStrategy: public SASearchStrategyInterface<SATwoStageSearchStrategy<PreSearchStrategyClass, BinarySearchStrategyClass>> {

    private:
        typedef SATwoStageSearchStrategy<PreSearchStrategyClass, BinarySearchStrategyClass> ThisType;

        typedef SASearchStrategyInterface<PreSearchStrategyClass> PreSearchStrategy;
        typedef SASearchStrategyInterface<BinarySearchStrategyClass> BinarySearchStrategy;

        PreSearchStrategy* preSearch;
        BinarySearchStrategy* binSearch;

    public:
        SATwoStageSearchStrategy(PreSearchStrategyClass* preSearch, BinarySearchStrategyClass* binSearch)
                : SASearchStrategyInterface<ThisType>(binSearch->getStrategyName() + " with " + preSearch->getStrategyName(), preSearch->getSizeInBytes() + binSearch->getSizeInBytes()),
        preSearch((PreSearchStrategy*) preSearch),
        binSearch((BinarySearchStrategy*) binSearch)
        {
            binSearch->setStartLCP(preSearch->getLCP());
        }

        string getParamsString() { 
            return this->preSearch->getParamsString() + " " + this->binSearch->getParamsString();
        }

        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return preSearch->isValidFor(pattern, patternLength);
        }
        
        inline void searchImpl(unsigned char* &pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            preSearch->search(pattern, patternLength, beg, end);
            binSearch->search(pattern, patternLength, beg, end);
        }
        
        inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
            preSearch->search(pattern, patternLength, beg, end);
            return binSearch->queryCount(pattern, patternLength, beg, end);
        }
        
        inline void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
            preSearch->search(pattern, patternLength, beg, end);
            //binSearch->queryLocate(pattern, patternLength, beg, end, res);
        }
        
        inline int resultCountImpl(unsigned int beg, unsigned int end) {
            return binSearch->resultCount(beg, end);
        }
        
        inline void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
            binSearch->resultLocate(beg, end, res);
        }
        
        inline void useAddress4DataImpl(unsigned int* addr) {
            preSearch->useAddress4Data(addr);
            binSearch->useAddress4Data(addr);
        }  
        
        unsigned int getSuffixOffset(unsigned int index) { return binSearch->getSuffixOffset(index); }
        
    };
    
}

#endif	/* SASEARCHSTRATEGYBASE_H */

