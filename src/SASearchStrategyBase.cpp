#include "SASearchStrategyBase.h"
#include "SAHelpers.h"

namespace SASearch {
    
    string plusKString(int plusK) {
        if (plusK == PLUSK_DOUBLING) return " PlusDoubling";
        if (plusK == -2) return " Plus2PowerSmart";
        if (plusK == -1) return " Plus2PowerUnsafe";
        return (plusK > 0?(" Plus" + toString(plusK)):"");
    }

}
