/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "saBench.h"
#include "../shared/timer.h"
#include "../SAHelpers.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

unsigned char **patterns;
int patternsNumber, patternLength;

vector<Patterns*> PList;

unsigned int *addr4Data;

ChronoStopWatch timer;

vector<long long int> refCounts;
vector<double> refTimes;
double refSize = -1;

vector<vector<double>> times;
vector<vector<string>> timesDesc;
vector<vector<unsigned int>> timesMem;

fstream* resultFile = NULL;

void setResultFile(string resultFileName) {
    delete resultFile;
    resultFile = new fstream(resultFileName.c_str(), ios::out | ios::binary | ios::app);
}

void initTestingEnvironment(const char *textFileName, unsigned int queriesNum) {

    patternsNumber = queriesNum;
    
    fstream textFile(textFileName, ios::in | ios::binary | ios::ate);
    if (!textFile) {
            cout << "There was a problem opening file " << textFileName << " for reading." << endl;
            exit(1);
    }
    unsigned int textSize = textFile.tellg();
    refSize = textSize;
    
    #ifdef ALIGN_MEMORY
    addr4Data = (unsigned int*) malloc2n(2 * textSize * sizeof(unsigned int));
    #else
    addr4Data = new unsigned int[2* textSize];
    #endif
}

void initTestingEnvironment(const char *textFileName, unsigned int queriesNum, unsigned int minM, unsigned int maxM) {
    for(int m = minM; m <= maxM; m++) {
#ifdef SMALL_M_STEP
        if (m % 4)
            continue;
#endif
        if ((m > 16) && (m % 4))
            continue;
        PList.push_back(new Patterns(textFileName, queriesNum, m));
        times.push_back(vector<double>());
        timesDesc.push_back(vector<string>());
        timesMem.push_back(vector<unsigned int>());
    }
    patternLength = minM;
    
    initTestingEnvironment(textFileName, queriesNum);
}

void initTestingEnvironment(const char *textFileName, unsigned int queriesNum, unsigned int m) {
    PList.push_back(new Patterns(textFileName, queriesNum, m));
    patternLength = m;
    times.push_back(vector<double>());
    timesDesc.push_back(vector<string>());
    timesMem.push_back(vector<unsigned int>());
    
    initTestingEnvironment(textFileName, queriesNum);
}


void disposeTestingEnvironment() {
    #ifdef ALIGN_MEMORY
    free2n(addr4Data);
    #else   
    delete[] addr4Data;
    #endif

    for(int i = 0; i < PList.size(); i++) 
        delete PList[i];
    delete resultFile;
}

int timesRankingP;

bool timesRankingComp (int i, int j) { 
    return (times[timesRankingP][i] < times[timesRankingP][j]); 
}

void bestVariantsRanking() {
    for (int p = 0; p < times.size(); p++) {

        if (times[p].size() <= 1)
            return;

        cout << endl << "* * * * R A N K I N G * * * * m = " << PList[p]->getM() << endl;
        vector<int> timesRanking;
        for (unsigned int i = 0; i < times[p].size(); i++)
            timesRanking.push_back(i);

        timesRankingP = p;
        std::sort(timesRanking.begin(), timesRanking.end(), timesRankingComp);

        int ranking_eq_mem_count = 3;

        unsigned int cMem = UINT32_MAX;
        int counter = 0;

        for (unsigned int i = 0; i < times[p].size(); i++) {
            int j = timesRanking[i];
            if (cMem > timesMem[p][j]) {
                cMem = timesMem[p][j];
                counter = 0;
                ranking_eq_mem_count = 2;
            }

            if (cMem == timesMem[p][j] && counter++ < ranking_eq_mem_count)
                cout << (i+1) << ". " << timesDesc[p][j] << endl;
        }
        
    }
}

double testSASearchStrategy(TextIndex* searchStrategy, int repeats) {
    searchStrategy->useAddress4Data(addr4Data);

    double minMedT = 99999999;
    
    for(int p = 0; p < PList.size(); p++) {
        patterns = PList[p]->getPatterns();    
        patternLength = PList[p]->getM();
    
        bool inValid = false;
        for (int i = 0; i < patternsNumber; i++) 
            if (!searchStrategy->isValidFor(patterns[i], patternLength))
                inValid = true;
        if (inValid) continue;
        
        double minT = 99999999;
        double medT, locMedT;
        double countTimes[99];
        double locateTimes[99];

        long long count = 0, locateCount = 0;

        int locRepeats = repeats;

        do {
            for (int i = 0; i < repeats; i++) {
#ifdef SHOWPROGRESS                
                cout << "count\n";
#endif
                //CACHE CLEAN
#ifdef CLEAN_CACHE
                const int ccSize = 10*1024*1024; // Allocate 10M. Set much larger then L2
                const int ccRepeats = 0xff;
                char *cc = (char *)malloc(ccSize);
                for (int cci = 0; cci < ccRepeats; cci++)
                  for (int ccj = 0; ccj < ccSize; ccj++)
                    cc[ccj] = cci+ccj;
                free(cc);
#endif
                //COUNT QUERY
                count = 0;

                timer.startTimer();         
                for (int i = 0; i < patternsNumber; i++) 
                    count += searchStrategy->queryCount(patterns[i], patternLength);
                timer.stopTimer();

                countTimes[i] = timer.getElapsedTime();
                if (timer.getElapsedTime() < minT)
                    minT = timer.getElapsedTime();

        #ifndef SKIP_LOCATE_QUERY          
                if (i < locRepeats) {
#ifdef SHOWPROGRESS                                    
                    cout << "locate\n";
#endif                    
                    //LOCATE QUERY
                    locateCount = 0; 
                    vector<unsigned int> locations;
                    locations.reserve(100000);
                    timer.startTimer();         

                    for (int i = 0; i < patternsNumber; i++) {
                        searchStrategy->queryLocate(patterns[i], patternLength, locations);
                        locateCount += locations.size();
                        locations.clear();
                    }
                    timer.stopTimer();

                    locateTimes[i] = timer.getElapsedTime();

                    if (timer.getElapsedTime() / minT > 3) {
                        locRepeats = 2 * repeats / (timer.getElapsedTime() / minT);
                        if (locRepeats < 3)
                            locRepeats = 1;
                    }               
                }    
        #else
                locRepeats = 1;
                locateTimes[0] = 0;
        #endif
            }
            std::sort(countTimes, countTimes + repeats);
            std::sort(locateTimes, locateTimes + locRepeats);
            medT = countTimes[repeats / 2];
            locMedT = locateTimes[locRepeats / 2];
            if (countTimes[0] * MED2MIN_GUARD < medT || locateTimes[0] * MED2MIN_GUARD < locMedT)
                cout << "Test disruption: " << countTimes[0] << " " << medT << "; " << locateTimes[0] << " " << locMedT << "\n";
        } while (countTimes[0] * MED2MIN_GUARD < medT || locateTimes[0] * MED2MIN_GUARD < locMedT);

        std::ostringstream res;
        unsigned int *indexCounts = new unsigned int[patternsNumber];
        for (int i = 0; i < patternsNumber; i++) 
            indexCounts[i] = searchStrategy->queryCount(patterns[i], patternLength);
        unsigned int differences = PList[p]->getErrorCountsNumber(indexCounts);
    
#ifdef SKIP_LOCATE_QUERY
        locateCount = count;  
#endif        
        if (differences > 0 || locateCount!=count)
            res << "\tCNTERROR (diff: " << differences << "; sum: " << count << (locateCount!=count?("!=" + toString(locateCount) + "L"):"") << ")\t";
        
        res << (medT/patternsNumber*1000000000) << "\t" << (locMedT/patternsNumber*1000000000) << "\t";
        if (refSize != -1) 
            res << ((searchStrategy->getSizeInBytes())/refSize) << "\t";
        else 
            res << "\t\t";

        res << patternLength << "\t" << patternsNumber << "\t";
        res << searchStrategy->getParamsString() << "\t\t";
        res << searchStrategy->getStrategyName() << "\t\t";

        res << count << "\t\t";

        if (refTimes.size() > p)
            res << "(x" << (refTimes[p]/medT) << "\tx" << (refTimes[p]/locMedT) << ")\t";
        else {
            res << "\t" << "(loc:\tx" << (medT/locMedT) << ")\t";
            refTimes.push_back(medT);
            refCounts.push_back(count);
        }
        res << (searchStrategy->getSizeInBytes() / 1000000.0) << "[MB]\t";

        res << "\tmin: " << (minT/patternsNumber*1000000000)  << " [ns]\t\t";

        delete[] indexCounts;

    #ifdef VERIFY_LOCATIONS
        if (locateCount == count) {
            readPatternsToArray();
            vector<unsigned int> locations;
            for (int i = 0; i < patternsNumber; i++) {
                searchStrategy->resultLocate(begSA[i], endSA[i], locations);
                for (int j = 0; j < locations.size(); j++) {                 
                    if (locations[j] >= txtSize) {
                        res << endl << "ERRORNEOUS LOCATION: " << locations[j] << " (j=" << j << " |" << locations.size() << "|, i=" << i << ", " << patterns[i] << ")";
                        locations.clear();
                        searchStrategy->resultLocate(begSA[i], endSA[i], locations);
                        break;   
                    }
                    if (strncmp(patterns[i].c_str(), (char*) txt + locations[j], patternLeng) != 0) {
                        string suf((char*) txt + locations[j], patternLeng);
                        res << endl << "ERRORNEOUS LOCATION: " << locations[j] << " (j=" << j << " |" << locations.size() << "|, i=" << i << ", "  << patterns[i] << " != " << suf << ")";
                        locations.clear();
                        searchStrategy->resultLocate(begSA[i], endSA[i], locations);
                        break;
                    }                    
                }

                locations.clear();
            }
        }
    #endif

        cout << res.str() << endl;
        if (resultFile != NULL)
            *resultFile << res.str() << endl;

        if (minMedT > medT)
            minMedT = medT;
        
        times[p].push_back(medT);
        timesDesc[p].push_back(res.str());
        timesMem[p].push_back(searchStrategy->getSizeInBytes());
    //    cout << (count / patternsNumber) << endl;
    }
    
    return minMedT;
}
