/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   saBench.h
 * Author: coach
 *
 * Created on 23 stycznia 2016, 13:18
 */

#ifndef SABENCH_H
#define SABENCH_H

#include <string>
#include "../shared/Patterns.h"
#include "TextIndex.h"

//#define SKIP_LOCATE_QUERY
//#define SMALL_M_STEP
//#define CLEAN_CACHE
#define REPEATS_DEFAULT 11
#define MAX_PATTERN_LENGTH 1024
// 1.015 za mało dla dna200 m=8
#define MED2MIN_GUARD 1.030 
//#define SHOWPROGRESS

#ifndef ALIGN_MEMORY
#define ALIGN_MEMORY
#endif

#ifndef ALIGN_MEM2N
#define ALIGN_MEM2N 12
#endif

using namespace sa;

extern int patternsNumber, patternLength;
extern unsigned int *addr4Data;

void setResultFile(string resultFileName);
void initTestingEnvironment(const char *textFileName, unsigned int queriesNum, unsigned int mMin, unsigned int mMax);
void initTestingEnvironment(const char *textFileName, unsigned int queriesNum, unsigned int m);
void disposeTestingEnvironment();

double testSASearchStrategy(TextIndex* searchStrategy, int repeats = REPEATS_DEFAULT);

void bestVariantsRanking();


#endif /* SABENCH_H */

