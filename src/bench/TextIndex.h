/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TextIndex.h
 * Author: coach
 *
 * Created on 25 stycznia 2016, 11:15
 */

#ifndef TEXTINDEX_H
#define TEXTINDEX_H

#include <string>
#include <vector>

using namespace std;

namespace sa {

    class TextIndex {
    public:
        TextIndex();
        virtual ~TextIndex() = 0;
    
        virtual string getStrategyName() = 0;

        virtual string getParamsString() = 0;
        
        virtual unsigned int getSizeInBytes() = 0;

        virtual bool isValidFor(unsigned char* pattern, int patternLength) = 0;
        
        virtual int queryCount(unsigned char* pattern, int patternLength) = 0;
        
        virtual void queryLocate(unsigned char* pattern, int patternLength, vector<unsigned int>& res) = 0;
    
        virtual void useAddress4Data(unsigned int* addr) = 0;
        
    private:

    };

}
    
#endif /* TEXTINDEX_H */

