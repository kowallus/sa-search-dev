/* 
 * File:   testingBench.h
 * Author: coach
 *
 * Created on 13 luty 2015, 21:05
 */

#ifndef TESTINGBENCH_H
#define	TESTINGBENCH_H

#include "saBench.h"
#include "SASearchAdapter.h"
#include "../SALookupStrategies.h"

#define REPEATS_DEFAULT 11
#define MAX_PATTERN_LENGTH 1024

//#define NEGATIVES
//#define VERIFY_LOCATIONS

using namespace SASearch;

extern unsigned int *saArray;
extern int txtSize;
extern unsigned char* txt;

extern SALookupOn1Symbol* saLUT1;
extern SALookupOn2Symbols* saLUT2;
extern SALookupOn3Symbols* saLUT3;

void initTestingBench(const char* textFileName, const char* filePrefix, unsigned int queriesNum, unsigned int mMin, unsigned int mMax);
void initTestingBench(const char* textFileName, const char* filePrefix, unsigned int queriesNum, unsigned int m);
void disposeTestingBench();

template<class SASearchStrategyClass>
double testSASearchStrategy(SASearchStrategyClass* saSearchStrategy, int repeats = REPEATS_DEFAULT);

#ifndef uchar
#define uchar unsigned char
#endif
#ifndef ulong
#define ulong unsigned long
#endif

template<class SASearchStrategyClass>
double testSASearchStrategy(SASearchStrategyClass* saSearchStrategy, int repeats) {
    
    TextIndex* idx = new SASearchAdapter<SASearchStrategyClass>(saSearchStrategy, txtSize);
    double res = testSASearchStrategy(idx); 
    delete idx;
   
    return res;
}

#endif	/* TESTINGBENCH_H */
