/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SASearchAdapter.h
 * Author: coach
 *
 * Created on 13 lutego 2016, 21:43
 */

#ifndef SASEARCHADAPTER_H
#define SASEARCHADAPTER_H

#include "TextIndex.h"
#include "../SASearchStrategyBase.h"

using namespace sa;

namespace SASearch {

    template<class SASearchStrategyClass>
    class SASearchAdapter: public TextIndex {
    private:
        SASearchStrategyInterface<SASearchStrategyClass>* saIndex;
        int txtSize;
        
    public:
        SASearchAdapter(SASearchStrategyClass* saIndex, int txtSize) {
            this->saIndex = saIndex;
            this->txtSize = txtSize;
        }
        
        virtual ~SASearchAdapter() {}
        
        string getStrategyName() {
            return this->saIndex->getStrategyName();
        }
        
        string getParamsString() {
            return this->saIndex->getParamsString();
        }

        unsigned int getSizeInBytes() {
            return this->saIndex->getSizeInBytes();
        }

        bool isValidFor(unsigned char* pattern, int patternLength) {
            return this->saIndex->isValidFor(pattern, patternLength);
        }
        
        int queryCount(unsigned char* pattern, int patternLength) {
            return this->saIndex->queryCount(pattern, patternLength, 0, txtSize);
        }
        
        void queryLocate(unsigned char* pattern, int patternLength, vector<unsigned int>& res) {
            this->saIndex->queryLocate(pattern, patternLength, 0, txtSize, res);
        }
    
        void useAddress4Data(unsigned int* addr) {
            this->saIndex->useAddress4Data(addr);
        }        
    };
}

#endif /* SASEARCHADAPTER_H */

