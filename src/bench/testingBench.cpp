#include "testingBench.h"

#include <iostream>
#include <fstream>
#include <cstring>
#include "../SAkaryHelpers.h"

unsigned int *saArray;

int saFileSize, t, txtSize, saSize;

unsigned char* txt;

SALookupOn1Symbol* saLUT1;
SALookupOn2Symbols* saLUT2;
SALookupOn3Symbols* saLUT3; 

using namespace std;

void loadData(string filePrefix);

void initTestingBench(const char* textFileName, const char* filePrefix) {
         
    loadData(string(filePrefix));
       
    #ifdef ALIGN_MEMORY
    cout << "K-ary variants memory blocks aligned to 128 bytes." << endl;
    #endif         

    cout << "Building lookups ..." << endl;  
        
    saLUT1 = new SALookupOn1Symbol(saArray, txt, txtSize);
    saLUT2 = new SALookupOn2Symbols(saArray, txt, txtSize);
    saLUT3 = new SALookupOn3Symbols(saArray, txt, txtSize); 
}

void initTestingBench(const char* textFileName, const char* filePrefix, unsigned int queriesNum, unsigned int mMin, unsigned int mMax) {
    initTestingEnvironment(textFileName, queriesNum, mMin, mMax);
    
    initTestingBench(textFileName, filePrefix);
}

void initTestingBench(const char* textFileName, const char* filePrefix, unsigned int queriesNum, unsigned int m) {
    initTestingEnvironment(textFileName, queriesNum, m);
    
    initTestingBench(textFileName, filePrefix);
}

void disposeTestingBench() {
        
    disposeTestingEnvironment();
	
    delete[] txt;
    #ifdef ALIGN_MEMORY
    free2n(saArray);
    #else   
    delete[] saArray;
    #endif
}

void loadData(string filePrefix) {
    string saFileName = filePrefix + ".sa";
    string txtFileName = filePrefix + ".txt";

    fstream saFile(saFileName.c_str(), ios::in | ios::binary | ios::ate);
    if (!saFile) {
            cout << "There was a problem opening file " << saFileName << " for reading." << endl;
            exit(0);
    }
    cout << "Opened " << saFileName << " for reading." << endl;
    saFileSize = int(saFile.tellg());
    saSize = saFileSize / 4;
    saFile.seekg(0, ios::beg);
    #ifdef ALIGN_MEMORY
    saArray = (unsigned int*) malloc2n(saFileSize / 4 * sizeof(unsigned int));
    #else
    saArray = new unsigned int[saFileSize / 4];
    #endif
    
    cout << "Address for data: " << addr4Data << endl;
    
    saFile.read((char *)saArray, saFileSize);
    saFile.close();

    fstream txtFile(txtFileName.c_str(), ios::in | ios::binary | ios::ate);
    txtSize = txtFile.tellg();
    txt = (unsigned char*) malloc(txtSize + 1 + MAX_PATTERN_LENGTH);
    txtFile.seekg(0, ios::beg);
    txtFile.read((char*) txt, txtSize);
    txtFile.close();
    
    //add guard
    memset(txt + txtSize + 1, 255, MAX_PATTERN_LENGTH);
}

