/* 
 * File:   SAHuffLookupStrategies.h
 * Author: coach
 *
 * Created on 11 maja 2016, 12:53
 */

#ifndef SAHUFFLOOKUPSTRATEGIES_H
#define	SAHUFFLOOKUPSTRATEGIES_H

#define MAX_LOOKUP_LCP 32

#include <string>
#include "SABinarySearchStrategies.h"
#include "StrCmpStrategies.h"
#include "SASearchStrategyBase.h"
#include "shared/huff.h"
#include "SAHelpers.h"

using namespace std;

namespace SASearch {
 
    template<class HuffCoderClass, int bits>
    class SAHuffLookup: public SASearchStrategyInterface<SAHuffLookup<HuffCoderClass, bits>> {
    private:
        unsigned int lutBeg[POW(2, bits) + 1] = { 0 }; // beginning of the range
        unsigned int lutEnd[POW(2, bits) + 1] = { 0 }; // length of the range
        
        HuffCoderClass* huffCoder;
        bool manageHuffCoder;
        
        void fillLut(unsigned int *sa, unsigned char* txt, unsigned int txtSize) {
            
            unsigned int lutIdx = huffCoder->encode((const char*) (txt + sa[1]), bits);
            lutBeg[lutIdx] = 1;
            for (unsigned int i = 2; i <= txtSize; i++) {
                unsigned int idx = huffCoder->encode((const char*) (txt + sa[i]), bits);
                if (idx != lutIdx) {
                    lutEnd[lutIdx] = i;
                    lutIdx = idx;
                    if (lutBeg[lutIdx] == 0) 
                        lutBeg[lutIdx] = i;
                }
            }
            lutEnd[lutIdx] = txtSize;
        }
        
    public:
        
        SAHuffLookup(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
        : SASearchStrategyInterface<SAHuffLookup<HuffCoderClass, bits>>("LUT" + HuffCoderClass::getName() + "SA" + toString(bits), HuffCoderClass::getSizeInBytes() + (POW(2, bits) + 1) * 2 * sizeof(unsigned int)) {
            manageHuffCoder = true;
            huffCoder = new HuffCoderClass((const char*) txt, txtSize);
            fillLut(sa, txt, txtSize);
        }

        SAHuffLookup(unsigned int *sa, unsigned char* txt, unsigned int txtSize, HuffCoderClass* huffCoder)
        : SASearchStrategyInterface<SAHuffLookup<HuffCoderClass, bits>>("LUT" + HuffCoderClass::getName() + "SA" + toString(bits), HuffCoderClass::getSizeInBytes() + (POW(2, bits) + 1) * 2 * sizeof(unsigned int)) {
            manageHuffCoder = false;
            huffCoder = new HuffCoderClass((const char*) txt, txtSize);
            fillLut(sa, txt, txtSize);
        }

        
        ~SAHuffLookup() {
            if (manageHuffCoder)
                delete huffCoder;
        }
        
        string getParamsString() { 
            return "0 0";
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            int codedSymbolsCount = 0;
            huffCoder->encode((const char*) pattern, bits, codedSymbolsCount);
            return codedSymbolsCount <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int lutIdx = huffCoder->encode((const char*) pattern, bits);
            beg = lutBeg[lutIdx];
            end = lutEnd[lutIdx];
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
        }
        
        inline void finalizeUpdate(unsigned int *sa) {}
        
        // TODO: find shortest LUT index
        inline int getLCP() { return 0; }
    };
    
    template<class HuffCoderClass, int bits>
    class SAHuffLookupWithSATwist: public SASearchStrategyInterface<SAHuffLookupWithSATwist<HuffCoderClass, bits>> {
    private:
        unsigned int lutBeg[POW(2, bits) + 1] = { 0 }; // beginning of the range
        unsigned int lutEnd[POW(2, bits) + 1] = { 0 }; // length of the range
        
        HuffCoderClass* huffCoder;
        bool manageHuffCoder;
        
        int empty;
        double meanLog, varLog;
        
        int currentLCP = 0;
        
        unsigned int *twistedSA;
        
        void fillLutAndTwist(unsigned int *sa, unsigned char* txt, unsigned int txtSize) {
            unsigned int* lutCount = new unsigned int[POW(2, bits) + 1]();
            for (unsigned int i = 1; i <= txtSize; i++) {
                unsigned int idx = huffCoder->encode((const char*) (txt + sa[i]), bits);
                lutCount[idx]++;
            }
            
            meanLog = 0;
            empty = 0;
            for (unsigned int i = 0; i < POW(2, bits) + 1; i++) 
                if (!lutCount[i]) 
                    empty++;
                else 
                    meanLog += log2((double) lutCount[i]) * lutCount[i];
            
//            cout << empty << "\t" << (double) empty * 100.0 / POW(2, bits) << "%\n";
            meanLog /= txtSize;
            
            varLog = 0;
            for (unsigned int i = 0; i < POW(2, bits) + 1; i++) 
                if (lutCount[i]) 
                    varLog += pow((log2((double) lutCount[i]) - meanLog), 2) * lutCount[i];
            
            //varLog /= POW(2, bits) - empty - 1;
            varLog /= txtSize;
            varLog = sqrt(varLog);
            
//            cout << meanLog << "\t" << varLog << "\n";
            
            #ifdef ALIGN_MEMORY
            twistedSA = (unsigned int*) malloc2n((txtSize + 1) * sizeof(unsigned int));
            #else
            twistedSA = new unsigned int[txtSize + 1];
            #endif
            
            twistedSA[0] = sa[0];
            int tSApos = 1;
            for (unsigned int i = 1; i <= txtSize; i++) {
                unsigned int idx = huffCoder->encode((const char*) (txt + sa[i]), bits);
                if (lutBeg[idx] == 0) {
                    lutBeg[idx] = tSApos;
                    lutEnd[idx] = tSApos;
                    tSApos += lutCount[idx];
                }
                twistedSA[lutEnd[idx]++] = sa[i];
            }
            delete lutCount;
        }
        
    public:
        
        SAHuffLookupWithSATwist(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
        : SASearchStrategyInterface<SAHuffLookupWithSATwist<HuffCoderClass, bits>>("LUT" + HuffCoderClass::getName() + "SAtwist" + toString(bits), HuffCoderClass::getSizeInBytes() + (POW(2, bits) + 1) * 2 * sizeof(unsigned int)) {
            manageHuffCoder = true;
            huffCoder = new HuffCoderClass((const char*) txt, txtSize);
            fillLutAndTwist(sa, txt, txtSize);
        }
        
        SAHuffLookupWithSATwist(unsigned int *sa, unsigned char* txt, unsigned int txtSize, HuffCoderClass* huffCoder)
        : SASearchStrategyInterface<SAHuffLookupWithSATwist<HuffCoderClass, bits>>("LUT" + HuffCoderClass::getName() + "SAtwist" + toString(bits), HuffCoderClass::getSizeInBytes() + (POW(2, bits) + 1) * 2 * sizeof(unsigned int)) {
            manageHuffCoder = false;
            this->huffCoder = huffCoder;
            fillLutAndTwist(sa, txt, txtSize);
        }
        
        ~SAHuffLookupWithSATwist() {
            if (manageHuffCoder)
                delete huffCoder; 
            delete twistedSA;
        }
        
        string getParamsString() { 
            return toString(bits) + "b " + HuffCoderClass::getParamsString() + " " + toStringDouble((double) empty * 100.0 / POW(2, bits)) + " " + toStringDouble(meanLog) + " " + toStringDouble(varLog);
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            int codedSymbolsCount = 0;
            huffCoder->encode((const char*) pattern, bits, codedSymbolsCount);
            return codedSymbolsCount <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int lutIdx = huffCoder->encode((const char*) pattern, bits, currentLCP);
            
            beg = lutBeg[lutIdx];
            end = lutEnd[lutIdx];
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
            unsigned int lutIdx = huffCoder->encode((const char*) pattern, bits, currentLCP);
            lutBeg[lutIdx] = beg;
            lutEnd[lutIdx] = end;
        }
        
        inline void finalizeUpdate(unsigned int *sa) {}
        
        // TODO: find shortest LUT index? 77-various
        inline int getLCP() { return 77; }
        
        unsigned int* getTwistedSA() { return twistedSA; }
        
        // TODO: find shortest LUT index?
        inline int getCurrentLCP() { return currentLCP; }
    };
    
    template<class PreSearchStrategyWithSATwistClass, class BinarySearchStrategyClass>
    class SATwoStageSearchWithSATwistStrategy: public SASearchStrategyInterface<SATwoStageSearchWithSATwistStrategy<PreSearchStrategyWithSATwistClass, BinarySearchStrategyClass>> {

    private:
        typedef SATwoStageSearchWithSATwistStrategy<PreSearchStrategyWithSATwistClass, BinarySearchStrategyClass> ThisType;

        typedef SASearchStrategyInterface<PreSearchStrategyWithSATwistClass> PreSearchWithSATwistStrategy;
        typedef SASearchStrategyInterface<BinarySearchStrategyClass> BinarySearchStrategy;

        PreSearchWithSATwistStrategy* preSearch;
        BinarySearchStrategy* binSearch;

    public:
        SATwoStageSearchWithSATwistStrategy(PreSearchStrategyWithSATwistClass* preSearch, BinarySearchStrategyClass* binSearch)
                : SASearchStrategyInterface<ThisType>(binSearch->getStrategyName() + " with " + preSearch->getStrategyName(), preSearch->getSizeInBytes() + binSearch->getSizeInBytes()),
        preSearch((PreSearchWithSATwistStrategy*) preSearch),
        binSearch((BinarySearchStrategy*) binSearch)
        {
        }

        string getParamsString() { 
            return this->preSearch->getParamsString() + " " + this->binSearch->getParamsString();
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return preSearch->isValidFor(pattern, patternLength);
        }
        
        inline void searchImpl(unsigned char* &pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            preSearch->search(pattern, patternLength, beg, end);
            ((BinarySearchStrategyClass*) binSearch)->setStartLCP(((PreSearchStrategyWithSATwistClass*) preSearch)->getCurrentLCP());
            binSearch->search(pattern, patternLength, beg, end);
        }
        
        inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
            preSearch->search(pattern, patternLength, beg, end);
            ((BinarySearchStrategyClass*) binSearch)->setStartLCP(((PreSearchStrategyWithSATwistClass*) preSearch)->getCurrentLCP());
            return binSearch->queryCount(pattern, patternLength, beg, end);
        }
        
        inline void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
            preSearch->search(pattern, patternLength, beg, end);
            ((BinarySearchStrategyClass*) binSearch)->setStartLCP(((PreSearchStrategyWithSATwistClass*) preSearch)->getCurrentLCP());
            binSearch->queryLocate(pattern, patternLength, beg, end, res);
        }
        
        inline int resultCountImpl(unsigned int beg, unsigned int end) {
            return binSearch->resultCount(beg, end);
        }
        
        inline void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
            binSearch->resultLocate(beg, end, res);
        }
        
        inline void useAddress4DataImpl(unsigned int* addr) {
            preSearch->useAddress4Data(addr);
            binSearch->useAddress4Data(addr);
        }  
        
        unsigned int getSuffixOffset(unsigned int index) { return binSearch->getSuffixOffset(index); }
        
    };

    template<class PreSearchStrategyWithSATwistClass, class BinarySearchStrategyClass>
    class SATwistStrategy: public SASearchStrategyInterface<SATwistStrategy<PreSearchStrategyWithSATwistClass, BinarySearchStrategyClass>> {

    private:
        typedef SATwistStrategy<PreSearchStrategyWithSATwistClass, BinarySearchStrategyClass> ThisType;

        typedef SASearchStrategyInterface<PreSearchStrategyWithSATwistClass> PreSearchWithSATwistStrategy;
        typedef SASearchStrategyInterface<BinarySearchStrategyClass> BinarySearchStrategy;

        PreSearchWithSATwistStrategy* preSearch;
        BinarySearchStrategy* binSearch;
        
        unsigned int *sa;

    public:
        SATwistStrategy(unsigned int *sa, PreSearchStrategyWithSATwistClass* preSearch, BinarySearchStrategyClass* binSearch)
                : SASearchStrategyInterface<ThisType>(binSearch->getStrategyName() + " with " + preSearch->getStrategyName(), preSearch->getSizeInBytes() + binSearch->getSizeInBytes()),
        preSearch((PreSearchWithSATwistStrategy*) preSearch),
        binSearch((BinarySearchStrategy*) binSearch),
        sa(sa)
        {
        }

        string getParamsString() { 
            return this->preSearch->getParamsString() + " " + this->binSearch->getParamsString();
        }
              
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return preSearch->isValidFor(pattern, patternLength);
        }
        
        inline void searchImpl(unsigned char* &pattern, int patternLength, unsigned int &beg, unsigned int &end) {
        }
        
        inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
            preSearch->search(pattern, patternLength, beg, end);
            int count = 0;
            for(int i = beg; i < end; i++)
                count += this->sa[i];
            return count;
//            ((BinarySearchStrategyClass*) binSearch)->setStartLCP(((PreSearchStrategyWithSATwistClass*) preSearch)->getCurrentLCP());
//            return binSearch->queryCount(pattern, patternLength, beg, end);
        }
        
        inline void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
        }
        
        inline int resultCountImpl(unsigned int beg, unsigned int end) {
            return binSearch->resultCount(beg, end);
        }
        
        inline void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
            binSearch->resultLocate(beg, end, res);
        }
        
        inline void useAddress4DataImpl(unsigned int* addr) {
            preSearch->useAddress4Data(addr);
            binSearch->useAddress4Data(addr);
        }  
        
        unsigned int getSuffixOffset(unsigned int index) { return binSearch->getSuffixOffset(index); }
        
    };

    
}

#endif	/* SAHUFFLOOKUPSTRATEGIES_H */

