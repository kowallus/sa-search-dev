/* 
 * File:   testingBench.h
 * Author: coach
 *
 * Created on 13 luty 2015, 12:38
 */

#ifndef TESTINGBENCH_H
#define	TESTINGBENCH_H

#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <cstring>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include "asmlib.h"
#include "interface.h"
#include <byteswap.h>

#include "SASearchStrategyBase.h"
#include "StrCmpStrategies.h"
#include "SALookupStrategies.h"
#include "SAHashStrategies.h"

#include "createSAkary.h"
#include "SAkaryHelpers.h"


#define REPEATS_DEFAULT 11
#define MAX_PATTERN_LENGTH 1024

//#define NEGATIVES
//#define VERIFY_LOCATIONS

using namespace std;
using namespace SASearch;
using namespace kary;

extern unsigned int *addr4Data, *saArray;
extern int txtSize;
extern unsigned char* txt;

extern long long int refCount;
extern double refTime;
extern double refSize;

extern vector<double> times;
extern vector<string> timesDesc;
extern vector<unsigned int> timesMem;

extern fstream resultFile;
extern vector<string> patternsVector;
extern string* patterns;

extern int patternsNumber, patternLength;

extern unsigned int *begSA, *endSA;

extern SALookupOn1Symbol* saLUT1;
extern SALookupOn2Symbols* saLUT2;
extern SALookupOn3Symbols* saLUT3; 

void initTestingEnvironment(char* filePrefix, char* patternsCat, char* patternsLength);
void disposeTestingEnvironment();

void bestVariantsRanking();

void readPatternsToArray();

template<class SASearchStrategyClass>
double testSASearchStrategy(SASearchStrategyClass* saSearchStrategy, int repeats = REPEATS_DEFAULT);

#ifndef uchar
#define uchar unsigned char
#endif
#ifndef ulong
#define ulong unsigned long
#endif

template<class SASearchStrategyClass>
double testSASearchStrategy(SASearchStrategyClass* saSearchStrategy, int repeats = REPEATS_DEFAULT) {
    SASearchStrategyInterface<SASearchStrategyClass>* searchStrategy = saSearchStrategy;
    searchStrategy->useAddress4Data(addr4Data);
    
    double minT = 99999999;
    struct timeval tim;
    double t1, t2;
    double countTimes[99];
    double locateTimes[99];
    
    long long count, locateCount;
    
    int locRepeats = repeats;
    
    for (int i = 0; i < repeats; i++) {
        readPatternsToArray();

        //COUNT QUERY
        count = 0;
        
        gettimeofday(&tim, NULL);  
        t1 = tim.tv_sec + (tim.tv_usec / 1000000.0);  

        for (int i = 0; i < patternsNumber; i++) {
            count += searchStrategy->queryCount((unsigned char*) patterns[i].c_str(), patternLength, begSA[i] = 0 , endSA[i] = txtSize);
//            count += searchStrategy->resultCount(begSA[i], endSA[i]);
        }

        gettimeofday(&tim, NULL);  
        t2 = tim.tv_sec + (tim.tv_usec / 1000000.0);

        countTimes[i] = t2 - t1;
        if (t2 - t1 < minT)
            minT = t2 - t1;
        
        if (i < locRepeats) {
            //LOCATE QUERY
            locateCount = 0; 
            readPatternsToArray();
            vector<unsigned int> locations;
            locations.reserve(100000);
            gettimeofday(&tim, NULL);  
            t1 = tim.tv_sec + (tim.tv_usec / 1000000.0);  

            for (int i = 0; i < patternsNumber; i++) {
                searchStrategy->queryLocate((unsigned char*) patterns[i].c_str(), patternLength, begSA[i] = 0 , endSA[i] = txtSize, locations);
                //searchStrategy->resultLocate(begSA[i], endSA[i], locations);
                locateCount += locations.size();
                locations.clear();
            }

            gettimeofday(&tim, NULL);  
            t2 = tim.tv_sec + (tim.tv_usec / 1000000.0);

            locateTimes[i] = t2 - t1;
        
            if ((t2 - t1) / minT > 3) {
                locRepeats = 2 * repeats / ((t2 - t1) / minT);
                if (locRepeats < 3)
                    locRepeats = 1;
            }
                        
        }
        
    }
    std::sort(countTimes, countTimes + repeats);
    std::sort(locateTimes, locateTimes + locRepeats);
    double medT = countTimes[repeats / 2];
    double locMedT = locateTimes[locRepeats / 2];

    
    int negative = 0;
/*    for (int i = 0; i < patternsNumber; i++) {
        if (searchStrategy->isNegative(begSA[i], endSA[i])) {
#ifdef NEGATIVES
            if (i < patternsNumber - 2)
#endif
                negative++;	
        }
    }
*/
    std::ostringstream res;
    
    res << (medT/patternsNumber*1000000000) << "\t" << (locMedT/patternsNumber*1000000000) << "\t";
    if (refSize != -1) 
        res << ((searchStrategy->getSizeInBytes())/refSize) << "\t";
    else 
        res << "\t\t";
    res << searchStrategy->getStrategyName() << "\t\t";
    
    if (refTime != -1) 
        res << "(x" << (refTime/medT) << "\tx" << (refTime/locMedT) << ")\t";
    else {
        res << "\t" << "(loc:\tx" << (medT/locMedT) << ", count:\t" << count << ")\t";
        refTime = medT;
        refCount = count;
    }
    
    res << (searchStrategy->getSizeInBytes() / 1000000.0) << "[MB]\t";
    if (refCount != count || locateCount!=count)
        res << "\tCNTERROR (" << count << (negative?"N":"") << (locateCount!=count?("!=" + toString(locateCount) + "L"):"") << ")";
    res << "\tmin: " << (minT/patternsNumber*1000000000)  << " [ns]\t\t";
    
#ifdef VERIFY_LOCATIONS
    if (locateCount == count) {
        readPatternsToArray();
        vector<unsigned int> locations;
        for (int i = 0; i < patternsNumber; i++) {
            searchStrategy->resultLocate(begSA[i], endSA[i], locations);
            for (int j = 0; j < locations.size(); j++) {                 
                if (locations[j] >= txtSize) {
                    res << endl << "ERRORNEOUS LOCATION: " << locations[j] << " (j=" << j << " |" << locations.size() << "|, i=" << i << ", " << patterns[i] << ")";
                    locations.clear();
                    searchStrategy->resultLocate(begSA[i], endSA[i], locations);
                    break;   
                }
                if (strncmp(patterns[i].c_str(), (char*) txt + locations[j], patternLength) != 0) {
                    string suf((char*) txt + locations[j], patternLength);
                    res << endl << "ERRORNEOUS LOCATION: " << locations[j] << " (j=" << j << " |" << locations.size() << "|, i=" << i << ", "  << patterns[i] << " != " << suf << ")";
                    locations.clear();
                    searchStrategy->resultLocate(begSA[i], endSA[i], locations);
                    break;
                }                    
            }
            
            locations.clear();
        }
    }
#endif
    
    cout << res.str() << endl;
    resultFile << res.str() << endl;

    times.push_back(medT);
    timesDesc.push_back(res.str());
    timesMem.push_back(searchStrategy->getSizeInBytes());
//    cout << (count / patternsNumber) << endl;
    return medT;
}


template<class SASearchStrategyClass, class ReferenceSASearchStrategyClass>
int debugSASearchStrategy(SASearchStrategyClass* saSearchStrategy, ReferenceSASearchStrategyClass* referenceSASearchStrategy) {
    cout << "Verification... " << endl;

    SASearchStrategyInterface<SASearchStrategyClass>* searchStrategy = saSearchStrategy;
    SASearchStrategyInterface<ReferenceSASearchStrategyClass>* referenceStrategy = referenceSASearchStrategy;
    
    readPatternsToArray();

    unsigned int beg, end, refBeg, refEnd;
    
    for (int i = 0; i < patternsNumber; i++) {
        string ptrn = patterns[i];
        unsigned int crc = searchStrategy->queryCount((unsigned char*) ptrn.c_str(), patternLength, beg = 0 , end = txtSize);
        ptrn = patterns[i];
        unsigned int refCRC = referenceStrategy->queryCount((unsigned char*) ptrn.c_str(), patternLength, refBeg = 0 , refEnd = txtSize);

        ptrn = patterns[i];        
        searchStrategy->search((unsigned char*) ptrn.c_str(), patternLength, beg = 0 , end = txtSize);
        ptrn = patterns[i];
        referenceStrategy->search((unsigned char*) ptrn.c_str(), patternLength, refBeg = 0 , refEnd = txtSize);
    
        if (crc != refCRC || searchStrategy->isNegative(beg, end)) {
#ifdef NEGATIVES
            if ((i >= patternsNumber - 2) && (crc == refCRC) && searchStrategy->isNegative(beg, end))
                continue;
#endif
            cout << i << ". error: crc is " << crc << ", should be " << refCRC << "; " << (searchStrategy->isNegative(beg, end)?"NEGATIVE":"") << endl;
            return i;
        }  
    }

    cout << "OK!" << endl;
    
    return -1;
}

#endif	/* TESTINGBENCH_H */
