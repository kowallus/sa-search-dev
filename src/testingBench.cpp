#include "testingBench.h"

#include "SAHashStrategies.h"

unsigned int *addr4Data, *saArray, mid;

long long int refCount = -1;
double refTime = -1;
double refSize = -1;

vector<double> times;
vector<string> timesDesc;
vector<unsigned int> timesMem;

unsigned int *karySA;

int saFileSize, t, txtSize, saSize;
vector<string> patternsVector;
string* patterns;

unsigned char* txt;

string resultFileName = "searchSA_res.txt";
fstream resultFile(resultFileName.c_str(), ios::out | ios::binary | ios::app);

int patternsNumber, patternLength;

unsigned int *begSA, *endSA;

SALookupOn1Symbol* saLUT1;
SALookupOn2Symbols* saLUT2;
SALookupOn3Symbols* saLUT3; 

void debugKarySA();

template<class SASearchStrategyClass, class ReferenceSASearchStrategyClass>
int debugSASearchStrategy(SASearchStrategyClass* saSearchStrategy, ReferenceSASearchStrategyClass* referenceSASearchStrategy);

void loadData(string filePrefix, string patternsCat, string patternsLength);

void initTestingEnvironment(char* filePrefix, char* patternsCat, char* patternsLength) {
     
	patternLength = atoi(patternsLength);

	loadData(string(filePrefix), string(patternsCat), string(patternsLength));        
        
        resultFile << string(filePrefix) << ", " << patternsNumber << " patterns, m = " << string(patternsLength) << endl;

        #ifdef ALIGN_MEMORY
        cout << "K-ary variants memory blocks aligned to 128 bytes." << endl;
        resultFile << "K-ary variants memory blocks aligned to 128 bytes." << endl;
        #endif

//        debugKarySA();
/**/        
	begSA = new unsigned int[patternsNumber];
	endSA = new unsigned int[patternsNumber];
            
        cout << "Building lookups ..." << endl;  
        
        saLUT1 = new SALookupOn1Symbol(saArray, txt, txtSize);
        saLUT2 = new SALookupOn2Symbols(saArray, txt, txtSize);
        saLUT3 = new SALookupOn3Symbols(saArray, txt, txtSize); 
        
        cout << "Searching ..." << endl;
        
        cout << endl << "Reference implementation." << endl;
        refSize = txtSize;
        
        SASimpleBinarySearch<AsmLibStrCmp> saSearchStrategy(saArray, txt, txtSize);
        refTime = testSASearchStrategy(&saSearchStrategy, REPEATS_DEFAULT);
        
        SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING> saSearchStrategyPlusPower(saArray, txt, txtSize);
        testSASearchStrategy(&saSearchStrategyPlusPower, REPEATS_DEFAULT);
         
}
        
void disposeTestingEnvironment() {
        
	resultFile.close();

	delete[] begSA;
	delete[] endSA;
	delete[] patterns;
	
	delete[] txt;
	#ifdef ALIGN_MEMORY
        free2n(addr4Data);
        free2n(saArray);
        #else   
        delete[] addr4Data;
        delete[] saArray;
        #endif

        exit(EXIT_SUCCESS);
}

void loadData(string filePrefix, string patternsCat, string patternsLength) {
    string saFileName = filePrefix + ".sa";
    string txtFileName = filePrefix + ".txt";
    string patternsFileName = patternsCat + filePrefix + "_" + patternsLength + "_patterns.txt";
    string patternsLenFileName = patternsCat + filePrefix + "_" + patternsLength + "_patternsLen.txt";

    fstream saFile(saFileName.c_str(), ios::in | ios::binary | ios::ate);
    if (!saFile) {
            cout << "There was a problem opening file " << saFileName << " for reading." << endl;
            exit(0);
    }
    cout << "Opened " << saFileName << " for reading." << endl;
    saFileSize = int(saFile.tellg());
    saSize = saFileSize / 4;
    saFile.seekg(0, ios::beg);
    #ifdef ALIGN_MEMORY
    addr4Data = (unsigned int*) malloc2n(2 * saFileSize / 4 * sizeof(unsigned int));
    saArray = (unsigned int*) malloc2n(saFileSize / 4 * sizeof(unsigned int));
    #else
    addr4Data = new unsigned int[2* saFileSize / 4];
    saArray = new unsigned int[saFileSize / 4];
    #endif
    
    cout << "Address for data: " << addr4Data << endl;
    
    saFile.read((char *)saArray, saFileSize);
    saFile.close();
    
    fstream patternsFile(patternsFileName.c_str(), ios::in | ios::binary | ios::ate);
    fstream patternsLenFile(patternsLenFileName.c_str());
    if (!patternsFile) {
            cout << "There was a problem opening file " << patternsFileName << " for reading." << endl;
            exit(0);
    }
    if (!patternsLenFile) {
            cout << "There was a problem opening file " << patternsLenFileName << " for reading." << endl;
            exit(0);
    }
    cout << "Opened " << patternsFileName << " for reading." << endl;
    cout << "Opened " << patternsLenFileName << " for reading." << endl;
    string patternLen, patternsString;
    int nextPattern = 0;

    patternsString.resize(patternsFile.tellg());
    patternsFile.seekg(0, ios::beg);
    patternsFile.read(&patternsString[0], patternsString.size());
    patternsFile.close();

    while (patternsLenFile.good()) {
            getline(patternsLenFile, patternLen);
            patternsVector.push_back(patternsString.substr(nextPattern, atoi(patternLen.c_str())));
            nextPattern += atoi(patternLen.c_str());
    }
    patternsLenFile.close();

    patternsNumber = patternsVector.size();
    patterns = new string[patternsNumber];

    fstream txtFile(txtFileName.c_str(), ios::in | ios::binary | ios::ate);
    txtSize = txtFile.tellg();
    txt = (unsigned char*) malloc(txtSize + 1 + MAX_PATTERN_LENGTH);
    txtFile.seekg(0, ios::beg);
    txtFile.read((char*) txt, txtSize);
    txtFile.close();
    
    //add guard
    memset(txt + txtSize + 1, 255, MAX_PATTERN_LENGTH);
}


void readPatternsToArray() {
	for (int i = 0; i < patternsVector.size(); i++) {
		patterns[i] = patternsVector[i];
                patterns[i][0] = patternsVector[i][0]; 
	}
#ifdef NEGATIVES
        patterns[patternsVector.size() - 2][0]++;
        patterns[patternsVector.size() - 1][patternLength - 1]++;
#endif        
}

bool timesRankingComp (int i, int j) { 
    return (times[i] < times[j]); 
}

void bestVariantsRanking() {

    cout << endl << "* * * * R A N K I N G * * * *" << endl;
    vector<int> timesRanking;
    for (int i = 0; i < times.size(); i++)
        timesRanking.push_back(i);
    
    std::sort(timesRanking.begin(), timesRanking.end(), timesRankingComp);
    
    int ranking_eq_mem_count = 3;
    
    unsigned int cMem = UINT32_MAX;
    int counter = 0;
    
    for (int i = 0; i < times.size(); i++) {
        int j = timesRanking[i];
        if (cMem > timesMem[j]) {
            cMem = timesMem[j];
            counter = 0;
            ranking_eq_mem_count = 2;
        }
        
        if (cMem == timesMem[j] && counter++ < ranking_eq_mem_count)
            cout << (i+1) << ". " << timesDesc[j] << endl;
    }
}

void debugKarySA() {
//    SALookupOn3Symbols* debSALUT3 = new SALookupOn3Symbols(sa, txt, txtSize);
    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* debSAHash50p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, 8, 50);
//    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* debSAHash90p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(sa, txt, txtSize, 8, 90);

    SASimpleBinarySearch<AsmLibStrCmp> refBS(saArray, txt, txtSize);
//    KarySASearch<3, 1, AsmLibStrCmp> refBS(sa, txt, txtSize);
//       KaryLCPjoinSASearch<5, 1, 7, 1, BSwap32StrNECmp, AsmLibStrCmp> refBS(sa, txt, txtSize);
//    KaryLUTSASimpleSearch<13, BSwap32StrNECmp, SALookupOn3Symbols, 3> refBS(sa, txt, txtSize, debSALUT3);
//    MLKarySASearch<4, 7, 1, BSwap32StrNECmp> refBS(sa, txt, txtSize);

    SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING> testSAsearch(saArray, txt, txtSize, debSAHash50p->getLCP());
    SATwoStageSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>> testBS(debSAHash50p, &testSAsearch);
//    SASmartLCPBinarySearch<LCPStrNCmp32, -2> testBS(sa, txt, txtSize);
 
//    KarySASearch<6, 2, AsmLibStrCmp, 0> testBS(sa, txt, txtSize);
//    KaryLCPjoinSASearch<5, 3, 7, 1, BSwap32StrNECmp, AsmLibStrCmp> testBS(sa, txt, txtSize);
//     KarySASearch<21, 3, AsmLibStrCmp, 1> testBS(sa, txt, txtSize);
//    KaryLCPSASearch<3, 1, 9, 2, BSwap32StrNECmp, AsmLibStrCmp, 0> testBS(sa, txt, txtSize);
//    KaryLCPmixSASearch<4, 1, 12, 1, BSwap32StrNECmp, AsmLibStrCmp, 2> testBS(sa, txt, txtSize);
//    KaryLCPjoinSASearch<4, 1, 12, 1, BSwap32StrNECmp, AsmLibStrCmp> testBS(sa, txt, txtSize);

       
//    MLKarySASearch<2, 4, 1, BSwap32StrNECmp, 3> testBS(sa, txt, txtSize);
//    MLKarySASearch<5, 8, 3, BSwap32StrNECmp, 3> testBS(sa, txt, txtSize);
//     MLKaryLCPmixSASearch<4, 7, 3, 2, 2, BSwap32StrNECmp, AsmLibStrCmp> testBS(sa, txt, txtSize);
       
//      MLKaryLCPSASearch<5, 5, 1, 2, 2, BSwap32StrNECmp, AsmLibStrCmp> testBS(sa, txt, txtSize);
//    KaryLCPSASearch<4, 3, 7, 1, BSwap32StrSECmp, AsmLibStrCmp> testBS(sa, txt, txtSize);
//    MLKaryLCPjoinSASearch<5, 8, 3, 2, 2, BSwap32StrNECmp, AsmLibStrCmp> testBS(sa, txt, txtSize);
    
//      KaryLUTSASearch<13, 3, BSwap32StrNECmp, SALookupOn3Symbols, 3> testBS(sa, txt, txtSize, debSALUT3);
//    KaryLUTSASearch<3, 1, BSwap32StrNECmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, 0> testBS(sa, txt, txtSize, debSAHash50p);
  
//    KaryLUTLCPSASearch<5, 1, BSwap32StrNECmp, SALookupOn3Symbols, KaryLCPSASearch<5, 1, 5, 2, BSwap32StrNECmp, AsmLibStrCmp, 3>, 2> testBS(sa, txt, txtSize, 10000);
 //   KaryLUTLCPSASimpleSearch<5, BSwap32StrNECmp, SALookupOn3Symbols, MLKarySASimpleSearch<4, 5, BSwap32StrNECmp, 3>> testBS(sa, txt, txtSize, 10000);
      
 //   KaryLUTSASimpleSearch<5, BSwap32StrNECmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, 2>testBS(sa, txt, txtSize, debSAHash90p);
  
//    KaryLUTLCPSASearch<5, 1, BSwap32StrNECmp, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 8, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0> testBS(sa, txt, txtSize, 6000000);
    
    int t = debugSASearchStrategy(&testBS, &refBS);
    if (t != -1) {
        unsigned int beg, end;
        cout << "TEXT SIZE:\t"<< txtSize << endl;
        readPatternsToArray();
        string ptrn = patterns[t];
        refBS.search((unsigned char*) ptrn.c_str(), patternLength, beg = 0 , end = txtSize);
        cout << "***REFERENCE: " << refBS.getStrategyName() << endl;
        cout << "RAW RESULT:\t" << beg << "\t" << end << endl;
        cout << "SUFFIXES OFFSETS:\t" << refBS.getSuffixOffset(beg) << "\t" << refBS.getSuffixOffset(end) << endl;
        refBS.getIndexRanks(beg, end);
        cout << "SUFFIXES RANKS:\t"  << beg << "\t" << end << endl;
        cout << "***ERRORNEOUS: " << testBS.getStrategyName() << endl;
        ptrn = patterns[t];        
        testBS.search((unsigned char*) ptrn.c_str(), patternLength, beg = 0 , end = txtSize);
        cout << "RAW RESULT:\t" << beg << "\t" << end << endl;
        cout << "SUFFIXES OFFSETS:\t" << testBS.getSuffixOffset(beg) << "\t" << testBS.getSuffixOffset(end) << endl;
        ptrn = patterns[t];
        testBS.queryCount((unsigned char*) ptrn.c_str(), patternLength, beg = 0 , end = txtSize);
        testBS.getIndexRanks(beg, end);
        cout << "SUFFIXES RANKS:\t"  << beg << "\t" << end << endl;
    }
    
//    delete debSALUT3;
    delete debSAHash50p;
//    delete debSAHash90p;
#ifdef LCP_STATS_ON
    testBS.printStats();
#endif
}

