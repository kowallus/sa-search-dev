/* 
 * File:   mlKarySASearchStrategies.h
 * Author: coach
 *
 * Created on 12 czerwca 2015, 13:20
 */

#ifndef MLKARYSASEARCHSTRATEGIES_H
#define	MLKARYSASEARCHSTRATEGIES_H

#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"
#include "karySASearchStrategies.h"

using namespace std;

namespace SASearch {

    namespace kary {    
        
        template<int localLevelsCount, int k, int step, class StringComparisonStrategyClass, int plusK, class KarySearchStrategyClass>
        class MLKarySASearchTemplate: public KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KarySearchStrategyClass> {
        protected:
            unsigned int* mlKarySA;
            int mlLevelsCount;
            int nonLeafMLNodes;
            int bigLeafMLnodeLevels;
            int bigLeafMLnodeElements;
            int smallLeafMLnodeElements;
            int firstBigLeafNodeIndex;
            int lastBigLeafMlNode;
            int firstSmallLeafNodeIndex;

        private:
            unsigned int determineLastBigLeafMlNode() {
                unsigned int lastLevelElements = this->txtSize - pow(k, (this->levelsCount - 1));
                unsigned int lastLevelNodes = ((lastLevelElements - 1) / (k - 1)) + 1;
                unsigned int bigLeafMlNodeLastLevelNodes = pow(k, bigLeafMLnodeLevels - 1);
                unsigned int bigLeafMlNodes = ((lastLevelNodes - 1)/ bigLeafMlNodeLastLevelNodes) + 1;
                return nonLeafMLNodes + bigLeafMlNodes - 1;
            } 
            
            unsigned int determineFirstBigLeafNodeIndex() {
                unsigned int lastLevelElements = this->txtSize - pow(k, (this->levelsCount - 1));
                unsigned int lastLevelNodes = ((lastLevelElements - 1) / (k - 1)) + 1;
                unsigned int bigLeafMlNodes = lastBigLeafMlNode - nonLeafMLNodes + 1;
                return firstBigLeafNodeIndex + lastLevelNodes * (k - 1) + smallLeafMLnodeElements * bigLeafMlNodes;
            }
            
        public:
            MLKarySASearchTemplate(unsigned int *sa, unsigned char* txt, unsigned int txtSize, string name = "unnamed")
            : KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KarySearchStrategyClass>(sa, txt, txtSize, toString(localLevelsCount) + "-ML " + name),
                    mlKarySA(createMLkAry<localLevelsCount, k>(this->karySA, txtSize)),
                    mlLevelsCount(((this->levelsCount - 1) / localLevelsCount) + 1),
                    nonLeafMLNodes((pow(MLARYNESS, mlLevelsCount - 1) - 1) / (MLARYNESS - 1)),
                    bigLeafMLnodeLevels(((this->levelsCount - 1) % localLevelsCount) + 1),
                    bigLeafMLnodeElements(pow(k, bigLeafMLnodeLevels) - 1),
                    smallLeafMLnodeElements(pow(k, bigLeafMLnodeLevels - 1) - 1),
                    firstBigLeafNodeIndex(pow(k, (mlLevelsCount - 1) * localLevelsCount) - 1),
                    lastBigLeafMlNode(determineLastBigLeafMlNode()),
                    firstSmallLeafNodeIndex(determineFirstBigLeafNodeIndex())
            {
            }

            MLKarySASearchTemplate(unsigned char* txt, string name = "unnamed")
            : KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KarySearchStrategyClass>(txt, toString(localLevelsCount) + "-ML " + name)
            {
            }
            
            inline void setKaryConfiguration(unsigned int* karyLCPSA, unsigned int size) {
                this->txtSize = size;
                this->mlKarySA = karyLCPSA;
                this->lastNode = (this->txtSize - 1) / (k-1);
                this->levelsCount = karyLevels<k>(size);
                mlLevelsCount = ((this->levelsCount - 1) / localLevelsCount) + 1;
                nonLeafMLNodes = (pow(MLARYNESS, mlLevelsCount - 1) - 1) / (MLARYNESS - 1);
                bigLeafMLnodeLevels = ((this->levelsCount - 1) % localLevelsCount) + 1;
                bigLeafMLnodeElements = pow(k, bigLeafMLnodeLevels) - 1;
                smallLeafMLnodeElements = pow(k, bigLeafMLnodeLevels - 1) - 1;
                firstBigLeafNodeIndex = pow(k, (mlLevelsCount - 1) * localLevelsCount) - 1;
                lastBigLeafMlNode = determineLastBigLeafMlNode();
                firstSmallLeafNodeIndex = determineFirstBigLeafNodeIndex();
            }
            
            inline void index2suffixIndex(unsigned int &index) {}
            
            inline void suffixIndex2index(unsigned int &suffixIndex) {}
            
            void indexRankingCorrection() {
                int i = 1;
                while (this->mlKarySA[this->firstSmallLeafNodeIndex - i] == this->mlKarySA[this->firstSmallLeafNodeIndex - (i + 1)]) {
                    this->txtSize--;
                    i++;
                }
            }
            
            void nullKaryConfiguration() {
                this->mlKarySA = NULL;
            }
            
            unsigned int* createLCPSAkary(unsigned int *sa, unsigned int saSize, unsigned int* dest) { 
                unsigned int* karySA = createSAkarySimple<k>(sa, saSize);
                int size = (((saSize - 1) / (k - 1)) + 1) * (k - 1);
                unsigned int* mlKarySA = createMLkAry<localLevelsCount, k>(karySA, size);
                
                std::copy(mlKarySA, mlKarySA + size, dest);

                #ifdef ALIGN_MEMORY
                free2n(karySA);
                free2n(mlKarySA);
                #else
                delete[] karySA;
                delete[] mlKarySA;
                #endif
            }
            
            unsigned int getPrefixesSizeInBytes() { return 0;}
            
            static const unsigned int plusKvalue = plusK;
            
            ~MLKarySASearchTemplate() {
                #ifdef ALIGN_MEMORY
                free2n(this->mlKarySA);
                #else
                delete[] this->mlKarySA;
                #endif
            }
            
            inline bool isPatternLowerThanSuffix(const unsigned char* patternPtr, int patternLength, unsigned int suffixIndex) {
                return this->strcmpStrategy(patternPtr, this->txt + this->mlKarySA[suffixIndex], patternLength + 1) <= 0;
            }
            
            inline void indexMLCoordinates(unsigned int i, int& mlLevel, unsigned int& mlNodeIndex, unsigned int& mlNodeElements, unsigned int& mlNodeInMLLevel) {
                mlLevel = 0;
                int nextMLLevelNodes = (MLARYNESS - 1);
                unsigned int mlLevelIndex = 0;
                while (i >= mlLevelIndex + nextMLLevelNodes) {
                    mlLevel++;
                    mlLevelIndex += nextMLLevelNodes;
                    nextMLLevelNodes *= MLARYNESS;
                }
                mlNodeElements = (MLARYNESS - 1);
                unsigned int referenceNodeIndex = mlLevelIndex;
                if (i >= firstSmallLeafNodeIndex) {
                    mlNodeElements = smallLeafMLnodeElements;
                    referenceNodeIndex = firstSmallLeafNodeIndex;
                } else if (i >= firstBigLeafNodeIndex)
                        mlNodeElements = bigLeafMLnodeElements;
                
                unsigned int relativeMLNode = (i - referenceNodeIndex) / mlNodeElements;
                mlNodeIndex = referenceNodeIndex + relativeMLNode * mlNodeElements;
                
                mlNodeInMLLevel = relativeMLNode;
                if (i >= firstSmallLeafNodeIndex)
                    mlNodeInMLLevel += lastBigLeafMlNode - nonLeafMLNodes + 1;
                else if (mlNodeIndex + bigLeafMLnodeElements > firstSmallLeafNodeIndex)
                    mlNodeElements = firstSmallLeafNodeIndex - mlNodeIndex;
            }
            
            unsigned int getIndexRankImpl(unsigned int i) { 
                if (i >= (((this->txtSize - 1) / (k - 1)) + 1) * (k - 1))
                    return this->txtSize;
                
                int mlLevel;
                unsigned int mlNodeIndex;
                unsigned int nodeElements;
                unsigned int mlNodeInMLLevel;
                
                indexMLCoordinates(i, mlLevel, mlNodeIndex, nodeElements, mlNodeInMLLevel);

                unsigned int localIndex = i - mlNodeIndex;
                unsigned int localLevel = 0;
                unsigned int localLevelIndex = 0;
                unsigned int localElementsAtLevel = k - 1;
                
                while (localLevelIndex + localElementsAtLevel <= localIndex) {
                    localLevel++;
                    localLevelIndex += localElementsAtLevel;
                    localElementsAtLevel *= k;
                }
                                
                unsigned int karyIndex = pow(k, mlLevel * localLevelsCount + localLevel) - 1;
                karyIndex += mlNodeInMLLevel * pow(k, localLevel) * (k - 1);
                karyIndex += localIndex - localLevelIndex;
                
                return getKaryIndexRank<k>(karyIndex, this->txtSize, this->lastNode); 
            }

            inline void mlNodeCoordinates(unsigned int mlNode, int mlLevel, unsigned int& firstElementIndex, int& lastLocalNode) {
                if (mlLevel < this->mlLevelsCount - 1) {
                    firstElementIndex = node2index<MLARYNESS>(mlNode);
                    lastLocalNode = (MLARYNESS - 1 - 1) / (k - 1);
                } else {
                    if (mlNode <= this->lastBigLeafMlNode) {
                        firstElementIndex = this->firstBigLeafNodeIndex + (mlNode - this->nonLeafMLNodes) * this->bigLeafMLnodeElements;
                        if (mlNode == this->lastBigLeafMlNode)
                            lastLocalNode = (this->firstSmallLeafNodeIndex - firstElementIndex - 1) / (k - 1);
                        else
                            lastLocalNode = (this->bigLeafMLnodeElements - 1) / (k - 1);
                    } else {
                        firstElementIndex = this->firstSmallLeafNodeIndex + (mlNode - this->lastBigLeafMlNode - 1) * this->smallLeafMLnodeElements;
                        lastLocalNode = this->smallLeafMLnodeElements > 0?(this->smallLeafMLnodeElements - 1) / (k - 1):-1;
                    } 
                }
            }
            
            inline bool incrementMLKaryIndex(unsigned int& i, int& mlLevel, unsigned int& mlNode, unsigned int& mlNodeIndex, unsigned int& mlNodeElements) {
                unsigned int localIndex = i - mlNodeIndex;
                
                int c = numOfElementInNode<k>(localIndex);
                int node = index2node<k>(localIndex);

                unsigned int cMLNodeIndex = mlNodeIndex;
                unsigned int cMLLevel = mlLevel;
                int cNode = childNode<k>(node, c + 1);
                unsigned int j = node2index<k>(cNode);
                
                int lastLocalNode = (mlNodeElements - 1) / (k - 1);
                
                if ((mlNodeIndex < firstBigLeafNodeIndex) || (j < mlNodeElements)) {
                    if (j >= mlNodeElements) {
                        int mlC = cNode - lastLocalNode - 1;
                        unsigned int cMLNode = childNode<MLARYNESS>(index2node<MLARYNESS>(cMLNodeIndex), mlC);
                        mlNodeCoordinates(cMLNode, ++cMLLevel, cMLNodeIndex, lastLocalNode);
                        j = 0;
                        cNode = 0;
                    } 
                    if ((cMLNodeIndex < firstSmallLeafNodeIndex) || (smallLeafMLnodeElements > 0)) {
                        mlLevel = cMLLevel;
                        mlNodeIndex = cMLNodeIndex;
                        do {
                            i = mlNodeIndex + j;
                            cNode = childNode<k>(cNode, 0);
                            j = node2index<k>(cNode);
                            if (j >= mlNodeElements) {
                                if (mlLevel + 1 == mlLevelsCount)
                                    break;
                                int mlC = cNode - lastLocalNode - 1;
                                unsigned int cMLNode = childNode<MLARYNESS>(index2node<MLARYNESS>(mlNodeIndex), mlC);
                                int cLastLocalNode = lastLocalNode;
                                mlNodeCoordinates(cMLNode, ++cMLLevel, cMLNodeIndex, cLastLocalNode);
                                if (cLastLocalNode < 0) 
                                    break;
                                mlNode = cMLNode;
                                mlLevel = cMLLevel;
                                mlNodeIndex = cMLNodeIndex;
                                lastLocalNode = cLastLocalNode;
                                j = 0;
                                cNode = 0;
                            } 
                        } while (cNode <= lastLocalNode);
                        mlNodeElements = (lastLocalNode + 1) * (k - 1);
                        return true;
                    }
                }
                
                if (c < k - 2) {
                    i++;
                    return true; 
                }

                while (true) {
                    if (node == 0) {
                        if (mlLevel == 0) {
                            i = this->txtSize;
                            break;
                        }
                        int mlC = parentsChildNum<MLARYNESS>(mlNode);
                        mlNode = parentNode<MLARYNESS>(mlNode);
                        mlNodeIndex = node2index<MLARYNESS>(mlNode);
                        c = mlC % k;
                        node = (POW(k, localLevelsCount - 1) - 1) / (k - 1) + mlC / k;
                        mlLevel--;
                        mlNodeElements = (MLARYNESS - 1);
                    } else {
                        c = parentsChildNum<k>(node);
                        node = parentNode<k>(node);
                    }
                    i = mlNodeIndex + node2index<k>(node) + c;
                    if (c < k - 1)
                        return true;
                };            

                return false;
            }
            
            inline bool findPlusKOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, int counter) {
                if (isPatternLowerThanSuffix(pattern, patternLength, i))
                    return true;
                                
                int mlLevel;
                unsigned int mlNodeIndex;
                unsigned int mlNodeElements;
                unsigned int mlNodeInMLLevel;
                unsigned int mlNode;
                
                this->indexMLCoordinates(i, mlLevel, mlNodeIndex, mlNodeElements, mlNodeInMLLevel);
                if (mlLevel == this->mlLevelsCount - 1)     
                    mlNode = this->nonLeafMLNodes + mlNodeInMLLevel;
                else
                    mlNode = index2node<MLARYNESS>(mlNodeIndex);
                
                while (counter--) {
                    if (!this->incrementMLKaryIndex(i, mlLevel, mlNode, mlNodeIndex, mlNodeElements))
                        return true;
                    if (isPatternLowerThanSuffix(pattern, patternLength, i))
                        return true;
                }
                
                return false;
            }

            inline unsigned int determineMLNodeChild(const unsigned char* pattern, int patternLength, unsigned int firstElementIndex, unsigned int &i, int lastLocalNode) {
                return static_cast<KarySearchStrategyClass*>(this)->determineMLNodeChildImpl(pattern, patternLength, firstElementIndex, i, lastLocalNode);
            }
            
            inline int searchNode(const unsigned char* pattern, int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node = 0, int startElement = 0) { return static_cast<KarySearchStrategyClass*>(this)->searchNodeImpl(pattern, patternLength, firstElementIndex, i, node, startElement); }
            
            inline unsigned int determineMLNodeChildImpl(const unsigned char* pattern, int patternLength, unsigned int firstElementIndex, unsigned int &i, int lastLocalNode) {
                int node = 0;                
                
                while (node <= lastLocalNode)
                    node = childNode<k>(node, searchNode(pattern, patternLength, firstElementIndex, i, node));
                
                return node - lastLocalNode - 1;
            }
            
            inline void findFirstOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, unsigned int mlNode = 0) {
                i = this->txtSize;
                
                int mlLevel = 0;
                unsigned int firstElementIndex;
                int lastLocalNode;
                
                while (mlLevel < this->mlLevelsCount) {
                    
                    this->mlNodeCoordinates(mlNode, mlLevel++, firstElementIndex, lastLocalNode);
                    
                    int c = determineMLNodeChild(pattern, patternLength, firstElementIndex, i, lastLocalNode);
                    
                    mlNode = childNode<MLARYNESS>(mlNode, c);
                }
            }            
            
            void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) { };
            
            int resultCountImpl(unsigned int beg, unsigned int end) {
                return getIndexRankImpl(end) - getIndexRankImpl(beg);
            }
            
            virtual unsigned int getSuffixOffset(unsigned int index) { return this->mlKarySA[index]; };
            unsigned int* getMLKarySA() { return this->mlKarySA; }
        };
       
        template<int localLevelsCount, int k, int step, class StringComparisonStrategyClass, int plusK = 0>
        class MLKarySASearch: public MLKarySASearchTemplate<localLevelsCount, k, step, StringComparisonStrategyClass, plusK, MLKarySASearch<localLevelsCount, k, step, StringComparisonStrategyClass, plusK>> {
        private:
            typedef MLKarySASearch<localLevelsCount, k, step, StringComparisonStrategyClass, plusK> ThisType;

        public:
            MLKarySASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : MLKarySASearchTemplate<localLevelsCount, k, step, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "")
            {}

            ~MLKarySASearch() { }

            inline int searchNodeImpl(const unsigned char* pattern, int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node = 0, int startElement = 0) {
                unsigned int j = node2index<k>(node) + firstElementIndex + startElement + (step - 1);
                int c;
                    
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(pattern, patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }
                
                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(pattern, patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                
                return c;
            }
        };
    }
}

#endif	/* MLKARYSASEARCHSTRATEGIES_H */

