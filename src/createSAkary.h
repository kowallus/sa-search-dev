/* 
 * File:   createSAkary.h
 * Author: coach
 *
 * Created on 29 maja 2015, 10:05
 */

#ifndef CREATESAKARY_H
#define	CREATESAKARY_H

#include "SAkaryHelpers.h"
#include <cstring>
#include <iostream>

//#define KARY_STATS_ON

using namespace std;

namespace SASearch {    
    namespace kary {
     
        template<int k>
        unsigned int* createSAkarySimple(unsigned int *sa, int saSize) {
        
            #ifdef ALIGN_MEMORY
            unsigned int* karySA = (unsigned int*) malloc2n((saSize + k) * sizeof(unsigned int));
            #else
            unsigned int* karySA = new unsigned int[saSize + k];
            #endif
            
            unsigned int currentElementAtLevel[MAXKARYLEVEL + 1] = { };
            unsigned int currentIndexAtLevel[MAXKARYLEVEL + 1];
            
            int levelsCount = karyLevels<k>(saSize);
            calculateLevelsIndexes<k>(currentIndexAtLevel, saSize);

            unsigned int i = 0;
            int level = levelsCount - 1;
            
            while (i < saSize) {
                while (currentElementAtLevel[level] == (k - 1))
                    currentElementAtLevel[level--] = 0;
                
                karySA[currentIndexAtLevel[level]++] = sa[i++];
                currentElementAtLevel[level]++;
                
                if(currentIndexAtLevel[level] == saSize)
                    level--;
                else 
                    while (currentIndexAtLevel[level + 1] < saSize)
                        level++;
            }
            
            // last node filled up with the last value
            for(i = 0; i < k; i++)
                karySA[saSize + i] = karySA[saSize - 1];
            
            return karySA;
        }
        
        template<int k, class PreSearchStrategyClass>
        unsigned int* createSAkaryForLUT(unsigned int *sa, unsigned char* txt, int size, PreSearchStrategyClass* preSearch) {
        
            #ifdef ALIGN_MEMORY
            unsigned int* karySA = (unsigned int*) malloc2n((size + k) * sizeof(unsigned int));
            #else
            unsigned int* karySA = new unsigned int[size + k];
            #endif
            karySA[0] = sa[0];
            
            int lcp = preSearch->getLCP();

#ifdef KARY_STATS_ON
            const int thresholdsCount = 8;
            unsigned int thresholds[] = {k - 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000};
            unsigned int rangeCount[thresholdsCount] = { };
            unsigned int equalPrefixes[thresholdsCount] = { };
            unsigned int aboveElementsCount[thresholdsCount] = { };
#endif
            
            unsigned int beg = 1, end;
            while(beg < size) {
                unsigned char* prefix = txt + sa[beg];
                preSearch->search(prefix, lcp, beg, end);
                
                // TODO: hack concerning LUT inconsistencies with \0 symbol.
                if (beg == end) 
                    end++;

#ifdef KARY_STATS_ON
                for(int t = 0; t < thresholdsCount; t++) 
                    if ((end - beg) <= thresholds[t]) {
                        rangeCount[t]++;
                        int sCount = (end - beg < k - 1)?(end - beg):(k - 1);
                        for(int s = 1; s < sCount; s++) {
                            string prefixA((char*) txt + sa[beg + s - 1] + lcp,  4);
                            string prefixB((char*) txt + sa[beg + s] + lcp,  4);
                            if (prefixA == prefixB)
                                equalPrefixes[t]++;
                        }
                        for (int u = 0; u <= t; u++)
                            aboveElementsCount[u] += (end - beg);
                        
                        break;
                    }
#endif
                
                if ((end - beg < k) || (end - beg < MIN_SIZE_FOR_KARY_LAYOUT))
                    std::copy(sa + beg, sa + end, karySA + beg);
                else {
                    unsigned int* karyPart = createSAkarySimple<k>(sa + beg, end - beg);
                    std::copy(karyPart, karyPart + end - beg, karySA + beg);
                    #ifdef ALIGN_MEMORY
                    free2n(karyPart);
                    #else
                    delete[] (karyPart);
                    #endif
                }
                
                beg = end;
            }

#ifdef KARY_STATS_ON
            cout << "* * * K-ARY with LUT STATS for lcp=" << lcp << " k=" << k << "." << endl;
            for(int t = 0; t < thresholdsCount; t++) 
                    cout << "Blocks to " << thresholds[t] << " elements count: \t" << rangeCount[t] << "; equal prefixes ratio " << ((double) equalPrefixes[t] / rangeCount[t]) << endl;

            for(int t = 0; t < thresholdsCount; t++) 
                    cout << "Elements in blocks above " << (t==0?0:thresholds[t-1]) << ": \t" << aboveElementsCount[t] << endl;
#endif
            
            return karySA;
        }
        
        template<int k, int prefixLengthInUints>
        prefixarray<prefixLengthInUints>* createPrefixesCache(unsigned int *karySA, unsigned char* txt, int elementsCount, int prefixesLevelsCount) {
            
            unsigned int currentIndexAtLevel[MAXKARYLEVEL];
            
            calculateLevelsIndexes<k>(currentIndexAtLevel, elementsCount);
            unsigned int currentElementAtLevel[MAXKARYLEVEL + 1] = { };
            
            int levelsCount = karyLevels<k>(elementsCount);
            unsigned int prefixesCount;
            if (prefixesLevelsCount < levelsCount) 
                prefixesCount = currentIndexAtLevel[prefixesLevelsCount];
            else {
                prefixesCount = elementsCount;
                prefixesLevelsCount = levelsCount;
            }
            
            #ifdef ALIGN_MEMORY
            prefixarray<prefixLengthInUints>* prefixes = (prefixarray<prefixLengthInUints>*) malloc2n((prefixesCount + k) * sizeof(prefixarray<prefixLengthInUints>));
            #else
            prefixarray<prefixLengthInUints>* prefixes = new prefixarray<prefixLengthInUints>[prefixesCount + k];
            #endif

            prefixarray<prefixLengthInUints> minPrefix;
            prefixarray<prefixLengthInUints> maxPrefix;
            for(int i = 0; i < prefixLengthInUints * sizeof(unsigned int); i++) {
                    minPrefix[i] = 0;
                    maxPrefix[i] = 255;
                }
            
            unsigned char* lcp = new unsigned char[prefixesLevelsCount]();
            
            int level = 0;
            
            unsigned char* lPrefix;
            unsigned char* rPrefix;
            
            while (true) {
                
                if (currentElementAtLevel[level] < (k - 1))
                    strncpy((char*) prefixes[currentIndexAtLevel[level]], 
                            (char*) txt + karySA[currentIndexAtLevel[level]] + lcp[level], 
                            prefixLengthInUints * sizeof(unsigned int));

                if (currentElementAtLevel[level] == k) {
                    if (level == 0)
                        break;
                    currentElementAtLevel[level] = 0;
                    if (currentElementAtLevel[--level]++ < k - 1)
                        currentIndexAtLevel[level]++;
                    continue;
                }
                
                if ((level == prefixesLevelsCount - 1) || currentIndexAtLevel[level + 1] >= elementsCount) {
                    if (currentElementAtLevel[level]++ < k - 1)
                        currentIndexAtLevel[level]++;
                    continue;     
                }
                
                int lLCPShift = 0;
                int lLevel = level;
                while(true) {
                    if (currentElementAtLevel[lLevel] > 0) {
                        lPrefix = prefixes[currentIndexAtLevel[lLevel] - 1] + lLCPShift;
                        break;
                    }
                    lLCPShift += lcp[lLevel] - lcp[lLevel - 1];
                    if (--lLevel < 0) {
                        lPrefix = minPrefix;
                        break;
                    }
                }
                if (lLCPShift >= prefixLengthInUints * sizeof(unsigned int)) {
                    lcp[++level] = lcp[level - 1];
                    continue;
                }
                
                int lcpShift = 0;
                int rLevel = level;
                while(true) {
                    if (currentElementAtLevel[rLevel] < (k - 1)) {
                        rPrefix = prefixes[currentIndexAtLevel[rLevel]] + lcpShift;
                        break;
                    }
                    lcpShift += lcp[rLevel] - lcp[rLevel - 1];
                    if (--rLevel < 0) {
                        rPrefix = maxPrefix;
                        break;
                    }
                }
                if (lcpShift >= prefixLengthInUints * sizeof(unsigned int)) {
                    lcp[++level] = lcp[level - 1];
                    continue;
                }
                if (lLCPShift > lcpShift)
                    lcpShift = lLCPShift;
                
                lcp[++level] = lcp[level - 1] + strlcp(lPrefix, rPrefix, prefixLengthInUints * sizeof(unsigned int) - lcpShift);
                    
            }
                    
            delete[] lcp;
            
            // last node filled up with the last value
            if (prefixesCount == elementsCount)
                for(unsigned int i = 0; i < k; i++)
                    strncpy((char*) prefixes[elementsCount + i], 
                                (char*) prefixes[elementsCount - 1], 
                                prefixLengthInUints * sizeof(unsigned int));
            
            return prefixes;
        }
        
        template<int k, int prefixesLengthInUints>
        unsigned int* mixKaryWithPrefixes(unsigned int *karySA, prefixarray<prefixesLengthInUints>* prefixes, int elementsCount, int prefixesLevelsCount) {
                        
            unsigned int levelsIndexes[MAXKARYLEVEL];
            
            calculateLevelsIndexes<k>(levelsIndexes, elementsCount);
           
            int levelsCount = karyLevels<k>(elementsCount);
            unsigned int prefixesCount;
            if (prefixesLevelsCount < levelsCount) 
                prefixesCount = levelsIndexes[prefixesLevelsCount];
            else {
                prefixesCount = elementsCount;
                prefixesLevelsCount = levelsCount;
            }
            
            unsigned int mixSAsize = (elementsCount + k) + (prefixesCount + k) * sizeof(prefixarray<prefixesLengthInUints>) / sizeof(unsigned int);
            
            #ifdef ALIGN_MEMORY
            unsigned int* mixSA = (unsigned int*) malloc2n(mixSAsize * sizeof(unsigned int));
            #else
            unsigned int* mixSA = new unsigned int[mixSAsize];
            #endif
            
            unsigned int* ptr = mixSA;
            
            // last node filled up with the last value?
            if (prefixesCount == elementsCount)
                prefixesCount += k;
            else 
                elementsCount += k;
            
            unsigned int i;
            for(i = 0; i < prefixesCount; i++) {
                *ptr++ = karySA[i];
                strncpy((char*) ptr, (char*) prefixes[i], prefixesLengthInUints * sizeof(unsigned int));
                ptr += prefixesLengthInUints;
            }
            
            for(; i < elementsCount; i++)
                *ptr++ = karySA[i];
            
            return mixSA;
        }
        
        template<int k, int prefixesLengthInUints>
        unsigned int* joinKaryWithPrefixes(unsigned int *karySA, prefixarray<prefixesLengthInUints>* prefixes, int elementsCount, int prefixesLevelsCount) {
                        
            unsigned int levelsIndexes[MAXKARYLEVEL];
            
            calculateLevelsIndexes<k>(levelsIndexes, elementsCount);
           
            int levelsCount = karyLevels<k>(elementsCount);
            unsigned int prefixesCount;
            if (prefixesLevelsCount < levelsCount) 
                prefixesCount = levelsIndexes[prefixesLevelsCount];
            else {
                prefixesCount = elementsCount;
                prefixesLevelsCount = levelsCount;
            }
            
            unsigned int joinSAsize = (elementsCount + k) + (prefixesCount + k) * prefixesLengthInUints;
            
            #ifdef ALIGN_MEMORY
            unsigned int* joinSA = (unsigned int*) malloc2n(joinSAsize * sizeof(unsigned int));
            #else
            unsigned int* joinSA = new unsigned int[joinSAsize];
            #endif
            
            unsigned int* ptr = joinSA;
            
            // last node filled up with the last value?
            if (prefixesCount == elementsCount)
                prefixesCount += k;
            else 
                elementsCount += k;
            
            unsigned int i;
            for(i = 0; i < prefixesCount; i += k - 1) {
                for (int j = 0; j < k - 1; j++) 
                    *ptr++ = karySA[i + j];
                
                for (int j = 0; j < k - 1; j++) {
                    strncpy((char*) ptr, (char*) prefixes[i + j], prefixesLengthInUints * sizeof(unsigned int));
                    ptr += prefixesLengthInUints;
                }
            }
            
            for(; i < elementsCount; i++) 
                *ptr++ = karySA[i];
            
            return joinSA;
        }
        
        template<int localLevelsCount, int k, int baseElemLengthInInts = 1>
        unsigned int* createMLkAry(unsigned int *anyKarySA, int elementsCount) {
            
            unsigned int levelsIndexes[MAXKARYLEVEL];
            
            calculateLevelsIndexes<k>(levelsIndexes, elementsCount);
           
            int levelsCount = karyLevels<k>(elementsCount);
            
            unsigned int currentOffsetAtLevel[MAXKARYLEVEL] = { };
            for(int i = 1; i <= levelsCount; i++) {
                int elemsAtPreviousLevel = levelsIndexes[i] - levelsIndexes[i - 1];
                currentOffsetAtLevel[i] = currentOffsetAtLevel[i - 1] + elemsAtPreviousLevel * baseElemLengthInInts;
                elemsAtPreviousLevel *= k; 
            }
            unsigned int offsetsCount = elementsCount * baseElemLengthInInts;
            
            unsigned int mlSAsize = (elementsCount + k) * baseElemLengthInInts;
            
            #ifdef ALIGN_MEMORY
            unsigned int* mlSA = (unsigned int*) malloc2n(mlSAsize * sizeof(unsigned int));
            #else
            unsigned int* mlSA = new unsigned int[mlSAsize];
            #endif
            
            unsigned int nodesAtMLLevel = 1;
            unsigned int currentNodeAtMLLevel = 0;
            
            unsigned int* ptr = mlSA;
            
            int globalLevel = 0;
            int localLevel = 0;
            int mlLevel = 0;
            int nodesInLocalLevel = 1;
            unsigned int offsetWithoutLastLevelCheck = offsetsCount;
            
            do {
                int cpyLength = nodesInLocalLevel * (k - 1) * baseElemLengthInInts;
                if (currentOffsetAtLevel[globalLevel] + cpyLength >= offsetsCount) {
                    int lastLevelNodesInLastBigLeaf = ((((offsetsCount - currentOffsetAtLevel[globalLevel]) / baseElemLengthInInts) - 1) / (k - 1)) + 1;
                    cpyLength = lastLevelNodesInLastBigLeaf * (k - 1) * baseElemLengthInInts;
                    offsetWithoutLastLevelCheck = ptr + cpyLength - mlSA;
                }
                memcpy(ptr, anyKarySA + currentOffsetAtLevel[globalLevel], cpyLength * sizeof(unsigned int));
                currentOffsetAtLevel[globalLevel] += cpyLength;
                ptr += cpyLength;
                
                ++globalLevel;
                
                if ((currentOffsetAtLevel[globalLevel] >= offsetsCount) || (++localLevel == localLevelsCount)) {
                    currentNodeAtMLLevel++;
                    localLevel = 0;
                    nodesInLocalLevel = 1;
                    if (currentNodeAtMLLevel < nodesAtMLLevel) {
                        globalLevel = mlLevel * localLevelsCount;
                    } else {
                        mlLevel++;
                        currentNodeAtMLLevel = 0;
                        nodesAtMLLevel *= POW(k, localLevelsCount);
                    }
                } else 
                    nodesInLocalLevel *= k;
                
            } while (ptr < mlSA + offsetsCount);
            
            return mlSA;
        }

        template<class PreSearchStrategyClass, class KaryLCPSAStrategyClass>
        unsigned int minRangeSizeForLUTWithPrefixes(unsigned int *sa, unsigned char* txt, int size, PreSearchStrategyClass* preSearch, KaryLCPSAStrategyClass* karyLCPSASearch, unsigned int maxAdditonalSizeInBytes) {
            
            unsigned int* rangeCount = new unsigned int[size]();

            int lcp = preSearch->getLCP();
            
            unsigned int beg = 1, end;
            while(beg < size) {
                unsigned char* prefix = txt + sa[beg];
                preSearch->search(prefix, lcp, beg, end);                
                // TODO: hack concerning LUT inconsistencies with \0 symbol.
                if (beg == end) 
                    end++;
                rangeCount[end - beg]++;
                beg = end;
            }
            
            unsigned int additionSize = 0;
            
            for(int i = size - 1; i >= karyLCPSASearch->getPrefixesCount(); i--) {
                additionSize += rangeCount[i] * karyLCPSASearch->getPrefixesSizeInBytes();
                additionSize += rangeCount[i] * sizeof(unsigned int) * ((KaryLCPSAStrategyClass::kValue - 2) - ((end - beg - 1) % (KaryLCPSAStrategyClass::kValue - 1)));
                if (additionSize > maxAdditonalSizeInBytes) {
                    delete[] rangeCount;
                    return i + 1;
                }
            }
            
            delete[] rangeCount;
            
            return karyLCPSASearch->getPrefixesCount();
        }
        
        template<class PreSearchStrategyClass>
        unsigned int noOfElementsInLCPSARanges(unsigned int *sa, unsigned char* txt, int size, PreSearchStrategyClass* preSearch, unsigned int minSizeForLCPSA) {
            unsigned int elementsCount = 0;

            int lcp = preSearch->getLCP();
            
            unsigned int beg = 1, end;
            while(beg < size) {
                unsigned char* prefix = txt + sa[beg];
                preSearch->search(prefix, lcp, beg, end);                
                // TODO: hack concerning LUT inconsistencies with \0 symbol.
                if (beg == end) 
                    end++;
                if (end - beg >= minSizeForLCPSA)
                    elementsCount += end - beg;
                beg = end;
            }
            
            return elementsCount;
        }
            
        template<int k, class PreSearchStrategyClass, class KaryLCPSAStrategyClass>
        unsigned int* createSAkaryForLUTWithPrefixes(unsigned int *sa, unsigned char* txt, int size, PreSearchStrategyClass* preSearch, KaryLCPSAStrategyClass* karyLCPSASearch, unsigned int minSizeForLCPSA, unsigned int &additonalSizeInBytes) {
            int lcp = preSearch->getLCP();            
            unsigned int beg = 1, end;
            additonalSizeInBytes = 0;
            while(beg < size) {
                unsigned char* prefix = txt + sa[beg];
                preSearch->search(prefix, lcp, beg, end);
                
                // TODO: hack concerning LUT inconsistencies with \0 symbol.
                if (beg == end) 
                    end++;
                
                if (end - beg >= minSizeForLCPSA) {
                    additonalSizeInBytes += karyLCPSASearch->getPrefixesSizeInBytes();
                    additonalSizeInBytes += ((KaryLCPSAStrategyClass::kValue - 2) - ((end - beg - 1) % (KaryLCPSAStrategyClass::kValue - 1))) * sizeof(unsigned int);
                }
                beg = end;
            }
            
            #ifdef ALIGN_MEMORY
            unsigned int* karyLUTLCPSA = (unsigned int*) malloc2n((size + KaryLCPSAStrategyClass::kValue) * sizeof(unsigned int) + additonalSizeInBytes);
            #else
            unsigned int* karyLUTLCPSA = new unsigned int[size + KaryLCPSAStrategyClass::kValue];
            #endif
            karyLUTLCPSA[0] = sa[0];
           
            
            beg = 1;
            unsigned int pos = beg, newEnd;
            while(beg < size) {
                unsigned char* prefix = txt + sa[beg];
                preSearch->search(prefix, lcp, beg, end);
                
                // TODO: hack concerning LUT inconsistencies with \0 symbol.
                if (beg == end) 
                    end++;
                
                newEnd = pos + end - beg;
                
                if (end - beg >= minSizeForLCPSA) {
                    karyLCPSASearch->createLCPSAkary(sa + beg, end - beg, karyLUTLCPSA + pos);
                    newEnd += karyLCPSASearch->getPrefixesSizeInBytes() / sizeof(unsigned int);
                    newEnd += (KaryLCPSAStrategyClass::kValue - 2) - ((end - beg - 1) % (KaryLCPSAStrategyClass::kValue - 1));
                } else if (end - beg >= MIN_SIZE_FOR_KARY_LAYOUT) {
                    unsigned int* karyPart = createSAkarySimple<k>(sa + beg, end - beg);
                    std::copy(karyPart, karyPart + end - beg, karyLUTLCPSA + pos);
                    #ifdef ALIGN_MEMORY
                    free2n(karyPart);
                    #else
                    delete[] (karyPart);
                    #endif
                } else 
                    std::copy(sa + beg, sa + end, karyLUTLCPSA + pos);
                
                preSearch->update(prefix, pos, newEnd);                
                pos = newEnd;
                beg = end;
            }
            preSearch->finalizeUpdate(karyLUTLCPSA);
            
            return karyLUTLCPSA;            
        }
        
        template<int k, class PreSearchStrategyWithSATwistClass, class KaryLCPSAStrategyClass>
        unsigned int* createSAkaryForLUTWithSATwistWithPrefixes(unsigned int *sa, unsigned char* txt, int size, PreSearchStrategyWithSATwistClass* preSearch, KaryLCPSAStrategyClass* karyLCPSASearch, unsigned int minSizeForLCPSA, unsigned int &additonalSizeInBytes) {
            int lcp = preSearch->getLCP();            
            unsigned int beg = 1, end;
            additonalSizeInBytes = 0;
            while(beg < size) {
                unsigned char* prefix = txt + sa[beg];
                preSearch->search(prefix, lcp, beg, end);
                
                // TODO: hack concerning LUT inconsistencies with \0 symbol.
                if (beg == end) 
                    end++;
                
                if (end - beg >= minSizeForLCPSA) {
                    additonalSizeInBytes += karyLCPSASearch->getPrefixesSizeInBytes();
                    additonalSizeInBytes += ((KaryLCPSAStrategyClass::kValue - 2) - ((end - beg - 1) % (KaryLCPSAStrategyClass::kValue - 1))) * sizeof(unsigned int);
                }
                beg = end;
            }
            
            #ifdef ALIGN_MEMORY
            unsigned int* karyLUTLCPSA = (unsigned int*) malloc2n((size + KaryLCPSAStrategyClass::kValue) * sizeof(unsigned int) + additonalSizeInBytes);
            #else
            unsigned int* karyLUTLCPSA = new unsigned int[size + KaryLCPSAStrategyClass::kValue];
            #endif
            karyLUTLCPSA[0] = sa[0];
           
            
            beg = 1;
            unsigned int pos = beg, newEnd;
            while(beg < size) {
                unsigned char* prefix = txt + sa[beg];
                preSearch->search(prefix, lcp, beg, end);
                
                if (prefix - txt + preSearch->getCurrentLCP() >= size) {
                    beg++;
                    continue;
                }
                
                // TODO: hack concerning LUT inconsistencies with \0 symbol.
                if (beg == end) 
                    end++;
                
                newEnd = pos + end - beg;
                
                if (end - beg >= minSizeForLCPSA) {
                    karyLCPSASearch->createLCPSAkary(sa + beg, end - beg, karyLUTLCPSA + pos, preSearch->getCurrentLCP());
                    newEnd += karyLCPSASearch->getPrefixesSizeInBytes() / sizeof(unsigned int);
                    newEnd += (KaryLCPSAStrategyClass::kValue - 2) - ((end - beg - 1) % (KaryLCPSAStrategyClass::kValue - 1));
                } else if (end - beg >= MIN_SIZE_FOR_KARY_LAYOUT) {
                    unsigned int* karyPart = createSAkarySimple<k>(sa + beg, end - beg);
                    std::copy(karyPart, karyPart + end - beg, karyLUTLCPSA + pos);
                    #ifdef ALIGN_MEMORY
                    free2n(karyPart);
                    #else
                    delete[] (karyPart);
                    #endif
                } else 
                    std::copy(sa + beg, sa + end, karyLUTLCPSA + pos);
                
                preSearch->update(prefix, pos, newEnd);                
                pos = newEnd;
                beg = end;
            }
            preSearch->finalizeUpdate(karyLUTLCPSA);
            
            return karyLUTLCPSA;            
        }
        
        template<int k>
        void karytest(unsigned int *sa, int saSize) {
            for (int i = 0; i < saSize; i++)
                cout << sa[i] << "\t";
            cout << endl << endl;
            
            unsigned int* karySA = createSAkarySimple<k>(sa, saSize);
            
            for (int i = 0; i < saSize; i++)
                cout << karySA[i] << "\t";
            
            cout << endl << endl;
        }
        
/*        void karytest1() {
            unsigned int sa[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            
            karytest<2>(sa, 10);
            karytest<3>(sa, 10);
            karytest<4>(sa, 10);
            karytest<5>(sa, 10);
            karytest<6>(sa, 10);
            
        }*/
        
    }
}


#endif	/* CREATESAKARY_H */

