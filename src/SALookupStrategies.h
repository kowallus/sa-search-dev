/* 
 * File:   SALookupStrategies.h
 * Author: coach
 *
 * Created on 25 maja 2015, 12:53
 */

#ifndef SALOOKUPSTRATEGIES_H
#define	SALOOKUPSTRATEGIES_H

#define MAX_LOOKUP_LCP 32

#include <string>
#include "SABinarySearchStrategies.h"
#include "StrCmpStrategies.h"
#include "SASearchStrategyBase.h"

using namespace std;

namespace SASearch {
 
    class SALookupOn1Symbol: public SASearchStrategyInterface<SALookupOn1Symbol> {
    private:
        unsigned int lut1[257]; 
        unsigned int* lutEndPtr = &lut1[256];
        
        void fillLut1(unsigned int *sa, unsigned char* txt, unsigned int txtSize) {
            
            SASimpleBinarySearch<AsmLibStrCmp> simpleSASearch(sa, txt, txtSize);
            
            unsigned char lutPattern[] = "a";
            unsigned int beg, end;
            lut1[256] = txtSize;
            for (int i = 255; i >= 0; i--) {
                lutPattern[0] = i;
                simpleSASearch.search(lutPattern, 1, beg = 0, end = txtSize);
                if (beg == 0) {
                        lut1[i] = lut1[i + 1];
                } else {
                        lut1[i] = beg;
                }
            }
        }
        
    public:
        SALookupOn1Symbol(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
        : SASearchStrategyInterface<SALookupOn1Symbol>("LUT1", 257 * sizeof(unsigned int)) {
            fillLut1(sa, txt, txtSize);
        }
        
        string getParamsString() { 
            return "1 0";
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return 1 <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            beg = lut1[pattern[0]];
            end = lut1[pattern[0] + 1];
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
            unsigned int* lutPtr = &lut1[pattern[0]];
            *lutPtr++ = beg;
            unsigned int oldEnd = *lutPtr;
            do {
                if (lutPtr == lutEndPtr) {
                    *lutPtr = end;
                    return;
                }
                if (*(lutPtr + 1) == oldEnd)
                    *lutPtr++ = end;
                else 
                    return;
            } while (true);
        }
        
        inline void finalizeUpdate(unsigned int *sa) {}
        
        inline int getLCP() { return 1; }
    };
    
    class SALookupOn2Symbols: public SASearchStrategyInterface<SALookupOn2Symbols> {
    private:
        unsigned int lut2[256][257];
        unsigned int* lutEndPtr = &lut2[255][256];
        
        void fillLut2(unsigned int *sa, unsigned char* txt, unsigned int txtSize) {
            
            SASimpleBinarySearch<AsmLibStrCmp> simpleSASearch(sa, txt, txtSize);
            
            unsigned char lutPattern[] = "aa";
            unsigned int beg, end;
            lut2[255][256] = txtSize;
            for (int i = 255; i >= 0; i--) {
                    if (i < 255) {
                            lut2[i][256] = lut2[i + 1][0];
                    }
                    lutPattern[0] = i;
                    for (int j = 255; j >= 0; j--) {
                            lutPattern[1] = j;
                            simpleSASearch.search(lutPattern, 2, beg = 0, end = txtSize);
                            if (beg == 0) {
                                    lut2[i][j] = lut2[i][j + 1];
                            } else {
                                    lut2[i][j] = beg;
                            }
                    }
            }
        }
        
    public:
        SALookupOn2Symbols(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
        : SASearchStrategyInterface<SALookupOn2Symbols>("LUT2", 256 * 257 * sizeof(unsigned int)) {
            fillLut2(sa, txt, txtSize);
        }
        
        string getParamsString() { 
            return "2 0";
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return 2 <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            beg = lut2[pattern[0]][pattern[1]];
            end = *(&(lut2[pattern[0]][pattern[1]]) + 1);
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
            unsigned int* lutPtr = &lut2[pattern[0]][pattern[1]];
            *lutPtr++ = beg;
            unsigned int oldEnd = *lutPtr;
            do {
                if (lutPtr == lutEndPtr) {
                    *lutPtr = end;
                    return;
                }
                if (*(lutPtr + 1) == oldEnd)
                    *lutPtr++ = end;
                else 
                    return;
            } while (true);
        }
        
        inline void finalizeUpdate(unsigned int *sa) {}
        
        inline int getLCP() { return 2; }
    };
    
    class SALookupOn3Symbols: public SASearchStrategyInterface<SALookupOn3Symbols> {
    private:
        unsigned int lut3[256][256][257];
        unsigned int* lutEndPtr = &lut3[255][255][256];
        
        void fillLut3(unsigned int *sa, unsigned char* txt, unsigned int txtSize) {
            
            SASimpleBinarySearch<AsmLibStrCmp> simpleSASearch(sa, txt, txtSize);
            
            unsigned char lutPattern[] = "aaa";
            unsigned int beg, end;
            lut3[255][255][256] = txtSize;
            for (int i = 255; i >= 0; i--) {
                    if (i < 255) {
                            lut3[i][255][256] = lut3[i + 1][255][0];
                    }
                    lutPattern[0] = i;
                    for (int j = 255; j >= 0; j--) {
                            if (j < 255) {
                                    lut3[i][j][256] = lut3[i][j + 1][0];
                            }
                            lutPattern[1] = j;
                            for (int k = 255; k >= 0; k--) {
                                    lutPattern[2] = k;
                                    simpleSASearch.search(lutPattern, 3, beg = 0, end = txtSize);
                                    if (beg == 0) {
                                            lut3[i][j][k] = lut3[i][j][k + 1];
                                    } else {
                                            lut3[i][j][k] = beg;
                                    }
                            }
                    }
            }
        }
        
    public:
        SALookupOn3Symbols(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
        : SASearchStrategyInterface<SALookupOn3Symbols>("LUT3", 256 * 256 * 257 * sizeof(unsigned int)) {
            fillLut3(sa, txt, txtSize);
        }
        
        string getParamsString() { 
            return "3 0";
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return 3 <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            beg = lut3[(unsigned char)pattern[0]][(unsigned char)pattern[1]][(unsigned char)pattern[2]];
            end = *(&(lut3[(unsigned char)pattern[0]][(unsigned char)pattern[1]][(unsigned char)pattern[2]]) + 1);
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
            unsigned int* lutPtr = &lut3[pattern[0]][pattern[1]][pattern[2]];
            *lutPtr++ = beg;
            unsigned int oldEnd = *lutPtr;
            do {
                if (lutPtr == lutEndPtr) {
                    *lutPtr = end;
                    return;
                }
                if (*(lutPtr + 1) == oldEnd)
                    *lutPtr++ = end;
                else 
                    return;
            } while (true);
        }
        
        inline void finalizeUpdate(unsigned int *sa) {}
        
        inline int getLCP() { return 3; }
    };
    
}

#endif	/* SALOOKUPSTRATEGIES_H */

