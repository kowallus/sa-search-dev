/* 
 * File:   karySASearchStrategies.h
 * Author: coach
 *
 * Created on 29 maja 2015, 14:04
 */

#ifndef KARYSASEARCHSTRATEGIES_H
#define	KARYSASEARCHSTRATEGIES_H

#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"

#define ELEMENTSINNODE k - 1

using namespace std;

namespace SASearch {

    namespace kary {    
        
        string stepString(int step);
    
        template<int k, int step, class StringComparisonStrategyClass, int plusK, class KarySearchStrategyClass>
        class KarySASearchTemplate: public SASearchStrategyTemplate<KarySearchStrategyClass, StringComparisonStrategyClass> {

        protected:
            int levelsCount;
            int lastNode;
            unsigned int* karySA;
            bool externalAddress4Data = false;
            
        public:
            KarySASearchTemplate(unsigned int *sa, unsigned char* txt, unsigned int txtSize, string name = "unnamed")
            : SASearchStrategyTemplate<KarySearchStrategyClass, StringComparisonStrategyClass>(sa, txt, txtSize, toString(k) + "-ary " + stepString(step) + name + plusKString(plusK)),
                    levelsCount(karyLevels<k>(txtSize)),
                    lastNode((txtSize-1) / (k-1))
            {
//                cout << "Generating simple " << toString(k) << "-arySA ..." << endl;
                this->karySA = createSAkarySimple<k>(sa, txtSize);
            }
            
            inline void useAddress4DataImpl(unsigned int* addr) {
                std::copy(this->karySA, this->karySA + this->txtSize, addr);
                #ifdef ALIGN_MEMORY
                free2n(this->karySA);
                #else
                delete[] this->karySA;
                #endif
                this->karySA = addr;
                externalAddress4Data = true;
            }
            
            // for use with karyKULLCPSA strategies
            KarySASearchTemplate(unsigned char* txt, string name = "unnamed")
            : SASearchStrategyTemplate<KarySearchStrategyClass, StringComparisonStrategyClass>(NULL, txt, 0, toString(k) + "-ary " + stepString(step) + name + plusKString(plusK)),
                    levelsCount(0),
                    lastNode(0)
            {
                this->karySA = NULL;
            }
            
            void nullKaryConfiguration() {
                this->karySA = NULL;
            }
            
            ~KarySASearchTemplate() {
                if (!externalAddress4Data) {
                    #ifdef ALIGN_MEMORY
                    free2n(this->karySA);
                    #else
                    delete[] this->karySA;
                    #endif
                }
            }

            string getParamsString() { 
                return toString(k) + " " + toString(step) + " " + toString(plusK);
            }
            
            inline bool incrementKaryIndex(unsigned int& i) {
                int c = numOfElementInNode<k>(i);
                unsigned int node = index2node<k>(i);

                unsigned int cNode = childNode<k>(node, c + 1);
                unsigned int j = node2index<k>(cNode); // j might overflow uint32 so cNode <= this->lastNode condition added
                if (cNode <= this->lastNode && j < this->txtSize) {
                    do {
                        i = j;
                        cNode = childNode<k>(cNode, 0);
                        j = node2index<k>(cNode);
                    } while (cNode <= this->lastNode && j < this->txtSize);
                    return true;
                }

                if (c < k - 2) {
                    i++;
                    if (i < this->txtSize)
                        return true;
                }

                while (node > 0) {
                    c = parentsChildNum<k>(node);
                    node = parentNode<k>(node);
                    if (c < k - 1) {
                        i = node2index<k>(node) + c;
                        return true;
                    }
                };            

                i = this->txtSize;
                return false;
            }

            inline bool levelUpKaryNode(unsigned int& node, int& c) {

                while (node > 0) {
                    c = parentsChildNum<k>(node);
                    node = parentNode<k>(node);
                    if (c < k - 1)
                        return true;
                };            

                return false;
            }

            inline bool isPatternLowerThanSuffix(const unsigned char* patternPtr, int patternLength, unsigned int suffixIndex) {
                // patternLength + 1 is a tweak to use with strncmp strategies
                return this->strcmpStrategy(patternPtr, this->txt + this->karySA[suffixIndex], patternLength + 1) <= 0;
            }
            
            
            inline int searchNode(const unsigned char* pattern, int patternLength, unsigned int &i, unsigned int node, int startElement = 0) { return static_cast<KarySearchStrategyClass*>(this)->searchNodeImpl(pattern, patternLength, i, node, startElement); }
            
            inline bool findPlusKOccurrence(const unsigned char* pattern, int patternLength, unsigned int &i, int counter = plusK) { return static_cast<KarySearchStrategyClass*>(this)->findPlusKOccurrenceImpl(pattern, patternLength, i, counter); }
                        
            inline bool findPlusKOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, int counter = plusK) {
                if (isPatternLowerThanSuffix(pattern, patternLength, i))
                    return true;

                while (counter--) {
                    if (!this->incrementKaryIndex(i))
                        return true;
                    if (isPatternLowerThanSuffix(pattern, patternLength, i))
                        return true;
                }
                
                return false;
            }
            
            inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                unsigned int node = 0;
                beg = this->txtSize;

                while (node <= this->lastNode)
                    node = childNode<k>(node, searchNode(pattern, patternLength, beg, node, 0));

                if (!incrementPattern(pattern, patternLength)) {
                    decrementPattern(pattern, patternLength);
                    return;
                };
                int c;
                end = beg;
                unsigned int i = end;

                if (plusK == 0) {
                    unsigned int endNode = index2node<k>(end);

                    while (true) {
                        if (isPatternLowerThanSuffix(pattern, patternLength, end))
                            break;
                        i = end + 1;
                        node = endNode;
                        if (!this->levelUpKaryNode(endNode, c)) {
                            end = this->txtSize;
                            break;
                        }
                        end = node2index<k>(endNode) + c;
                    }

                    if (end == beg) {
                        decrementPattern(pattern, patternLength);
                        return;
                    }

                } else {
                    if (findPlusKOccurrence(pattern, patternLength, i, plusK)) {
                        end = i;
                        decrementPattern(pattern, patternLength);
                        return;
                    }

                    unsigned int endNode = index2node<k>(end);
                    while (true) {
                        i = end + 1;
                        node = endNode;
                        if (!this->levelUpKaryNode(endNode, c)) {
                            end = this->txtSize;
                            break;
                        }
                        end = node2index<k>(endNode) + c;
                        if (isPatternLowerThanSuffix(pattern, patternLength, end))
                            break;
                    }

                }

                c = numOfElementInNode<k>(i);

                node = childNode<k>(node, searchNode(pattern, patternLength, end, node, c));
                while (node <= this->lastNode)
                    node = childNode<k>(node, searchNode(pattern, patternLength, end, node, 0));
                decrementPattern(pattern, patternLength);
            }
            
            virtual unsigned int getSuffixOffset(unsigned int index) { return this->karySA[index]; };
            unsigned int getIndexRankImpl(unsigned int i) { return getKaryIndexRank<k>(i, this->txtSize, this->lastNode); }

            inline void copyLocationsImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                res.insert(res.end(), karySA + beg, karySA + end);
            }
            
            inline void copyLocations(unsigned int beg, unsigned int end, vector<unsigned int>& res) { static_cast<KarySearchStrategyClass*>(this)->copyLocationsImpl(beg, end, res); }
            
            void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                unsigned int begAtLevelOffset[MAXKARYLEVEL];
                unsigned int begNode, endNode, cBeg, cEnd;
                unsigned int endAtLevel;
                int levelsDelta = 0;
                if (beg < end) {
                    if (end > this->txtSize) 
                        end = this->txtSize;
                    
                    begAtLevelOffset[levelsDelta++] = beg;
                    begNode = childNodePrecedingIndex<k>(beg);
                    while ((begNode <= this->lastNode) && (beg = node2index<k>(begNode) + k - 1) <= end) {
                        begAtLevelOffset[levelsDelta++] = beg;
                        begNode = childNode<k>(begNode, k - 1);
                    }
                    
                    endNode = childNodePrecedingIndex<k>(end);
                    while ((endNode <= this->lastNode) && (endAtLevel = node2index<k>(endNode) + k - 1) <= this->txtSize) {
                        this->copyLocations(beg, endAtLevel, res);
                        endNode = childNode<k>(endNode, k - 1);
                        begNode = childNode<k>(begNode, k - 1);
                        beg = node2index<k>(begNode) + k - 1;
                    }
                    
                    if (begNode <= this->lastNode && beg <= this->txtSize)
                        this->copyLocations(beg, this->txtSize, res);
                    
                    bool allToEnd = end >= this->txtSize;
                    if (allToEnd) {
                        end = 1;
                        while ((end * k - 1) < this->txtSize)
                            end *= k;
                        end -= 1;
                        endNode = index2node<k>(end) - 1;
                    } else
                        endNode = index2node<k>(end);
                    
                    while (levelsDelta-- > 0) {
                        if (allToEnd && end <= begAtLevelOffset[levelsDelta]) {
                            this->copyLocations(begAtLevelOffset[levelsDelta], 
                                this->txtSize, res);
                            cEnd = k - 1;
                        } else {
                            this->copyLocations(begAtLevelOffset[levelsDelta], 
                                    end, res);
                            cEnd = parentsChildNum<k>(endNode);
                            endNode = parentNode<k>(endNode);
                            end = node2index<k>(endNode) + cEnd;
                        }
                    }
                    begNode = index2node<k>(begAtLevelOffset[0]);
                    if (begNode == 0)
                        return;
                    do {
                        cBeg = parentsChildNum<k>(begNode);
                        begNode = parentNode<k>(begNode);
                        this->copyLocations(node2index<k>(begNode) + cBeg, 
                                node2index<k>(endNode) + cEnd, res);
                        if (begNode == endNode)
                            break;
                        cEnd = parentsChildNum<k>(endNode);
                        endNode = parentNode<k>(endNode);
                    } while (true);
                    return;
                }
                
                if (beg > end) {
                    begAtLevelOffset[levelsDelta++] = beg;
                    begNode = index2node<k>(beg);
                    cBeg = parentsChildNum<k>(begNode);
                    begNode = parentNode<k>(begNode);
                    while ((beg = node2index<k>(begNode) + cBeg) > end) {
                        begAtLevelOffset[levelsDelta++] = beg;
                        cBeg = parentsChildNum<k>(begNode);
                        begNode = parentNode<k>(begNode);
                    }
                    
                    endAtLevel = end;
                    endNode = index2node<k>(endAtLevel);
                    
                    do {
                        this->copyLocations(beg, endAtLevel, res);
                        if (begNode == endNode) 
                            break;
                        cBeg = parentsChildNum<k>(begNode);
                        cEnd = parentsChildNum<k>(endNode);
                        begNode = parentNode<k>(begNode);
                        endNode = parentNode<k>(endNode);
                        beg = node2index<k>(begNode) + cBeg;
                        endAtLevel = node2index<k>(endNode) + cEnd;
                    } while (true);

                    endNode = childNodePrecedingIndex<k>(end);
                    while (levelsDelta-- > 0) {
                        endAtLevel = node2index<k>(endNode) + k - 1;
                        if ((endNode > this->lastNode) || (endAtLevel > this->txtSize))
                            endAtLevel = this->txtSize;
                        copyLocations(begAtLevelOffset[levelsDelta],
                                    endAtLevel, res);
                        endNode = childNode<k>(endNode, k - 1);
                    }

                    begNode = childNodePrecedingIndex<k>(begAtLevelOffset[0]);
                    while ((endNode <= this->lastNode) && (endAtLevel = node2index<k>(endNode) + k - 1) <= this->txtSize) {;
                        copyLocations(node2index<k>(begNode) + k - 1, endAtLevel, res);
                        endNode = childNode<k>(endNode, k - 1);
                        begNode = childNode<k>(begNode, k - 1);
                    }
                    
                    if ((begNode <= this->lastNode) && (beg = node2index<k>(begNode) + k - 1) <= this->txtSize)
                        copyLocations(beg, this->txtSize, res);                
                }          
            }
            
            int resultCountImpl(unsigned int beg, unsigned int end) {
                unsigned int count = 0;
                unsigned int begAtLevelOffset[MAXKARYLEVEL];
                unsigned int begNode, endNode, cBeg, cEnd;
                unsigned int endAtLevel;
                int levelsDelta = 0;
                if (beg < end) {
                    if (end > this->txtSize) 
                        end = this->txtSize;
                    
                    begAtLevelOffset[levelsDelta++] = beg;
                    begNode = childNodePrecedingIndex<k>(beg);
                    while ((begNode <= this->lastNode) && (beg = node2index<k>(begNode) + k - 1) <= end) {
                        begAtLevelOffset[levelsDelta++] = beg;
                        begNode = childNode<k>(begNode, k - 1);
                    }
                    
                    endNode = childNodePrecedingIndex<k>(end);
                    while ((endNode <= this->lastNode) && (endAtLevel = node2index<k>(endNode) + k - 1) <= this->txtSize) {
                        count += endAtLevel - beg;
                        endNode = childNode<k>(endNode, k - 1);
                        begNode = childNode<k>(begNode, k - 1);
                        beg = node2index<k>(begNode) + k - 1;
                    }
                    
                    if (begNode <= this->lastNode && beg <= this->txtSize)
                        count += this->txtSize - beg;

                    bool allToEnd = end >= this->txtSize;
                    if (allToEnd) {
                        end = 1;
                        while ((end * k - 1) < this->txtSize)
                            end *= k;
                        end -= 1;
                        endNode = index2node<k>(end) - 1;
                    } else
                        endNode = index2node<k>(end);
                    
                    while (levelsDelta-- > 0) {
                        if (allToEnd && end <= begAtLevelOffset[levelsDelta]) {
                            count += this->txtSize - begAtLevelOffset[levelsDelta];
                            cEnd = k - 1;
                        } else {
                            count += end - begAtLevelOffset[levelsDelta];
                            cEnd = parentsChildNum<k>(endNode);
                            endNode = parentNode<k>(endNode);
                            end = node2index<k>(endNode) + cEnd;
                        }
                    }
                    begNode = index2node<k>(begAtLevelOffset[0]);
                    if (begNode == 0)
                        return count;
                    do {
                        cBeg = parentsChildNum<k>(begNode);
                        begNode = parentNode<k>(begNode);
                        count += node2index<k>(endNode) + cEnd - (node2index<k>(begNode) + cBeg);
                        if (begNode == endNode)
                            break;
                        cEnd = parentsChildNum<k>(endNode);
                        endNode = parentNode<k>(endNode);
                    } while (true);
                    return count;
                }
                
                if (beg > end) {
                    begAtLevelOffset[levelsDelta++] = beg;
                    begNode = index2node<k>(beg);
                    cBeg = parentsChildNum<k>(begNode);
                    begNode = parentNode<k>(begNode);
                    while ((beg = node2index<k>(begNode) + cBeg) > end) {
                        begAtLevelOffset[levelsDelta++] = beg;
                        cBeg = parentsChildNum<k>(begNode);
                        begNode = parentNode<k>(begNode);
                    }
                    
                    endAtLevel = end;
                    endNode = index2node<k>(endAtLevel);
                    
                    do {
                        count += endAtLevel - beg;
                        if (begNode == endNode) 
                            break;
                        cBeg = parentsChildNum<k>(begNode);
                        cEnd = parentsChildNum<k>(endNode);
                        begNode = parentNode<k>(begNode);
                        endNode = parentNode<k>(endNode);
                        beg = node2index<k>(begNode) + cBeg;
                        endAtLevel = node2index<k>(endNode) + cEnd;
                    } while (true);

                    endNode = childNodePrecedingIndex<k>(end);
                    while (levelsDelta-- > 0) {
                        endAtLevel = node2index<k>(endNode) + k - 1;
                        if ((endNode > this->lastNode) || (endAtLevel > this->txtSize))
                            endAtLevel = this->txtSize;
                        count += endAtLevel - begAtLevelOffset[levelsDelta];
                        endNode = childNode<k>(endNode, k - 1);
                    }

                    begNode = childNodePrecedingIndex<k>(begAtLevelOffset[0]);
                    while ((endNode <= this->lastNode) && (endAtLevel = node2index<k>(endNode) + k - 1) <= this->txtSize) {;
                        count += endAtLevel - (node2index<k>(begNode) + k - 1);
                        endNode = childNode<k>(endNode, k - 1);
                        begNode = childNode<k>(begNode, k - 1);
                    }
                    
                    if ((begNode <= this->lastNode) && (beg = node2index<k>(begNode) + k - 1) <= this->txtSize)
                        count += this->txtSize - beg;
                }          
                
                return count;
            }
            
            unsigned int* getKarySA() { return this->karySA; }
        };
        
        template<int k, int step, class StringComparisonStrategyClass, int plusK = 0>
        class KarySASearch: public KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, KarySASearch<k, step, StringComparisonStrategyClass, plusK>> {
        private:
            typedef KarySASearch<k, step, StringComparisonStrategyClass, plusK> ThisType;

            
        public:
            KarySASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : KarySASearchTemplate<k, step, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "SA")
            {}

            ~KarySASearch() { }
            
            inline int searchNodeImpl(const unsigned char* pattern, int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + startElement + (step - 1);

                int c;
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(pattern, patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }
                
                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(pattern, patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                
                return c;
            }
               
        };

        template<class StringComparisonStrategyClass, int plusK = 0>
        class EytzingerAdvancedSASearch: public KarySASearchTemplate<2, 1, StringComparisonStrategyClass, plusK, EytzingerAdvancedSASearch<StringComparisonStrategyClass, plusK>> {
        private:
            typedef EytzingerAdvancedSASearch<StringComparisonStrategyClass, plusK> ThisType;
            
        public:
            EytzingerAdvancedSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : KarySASearchTemplate<2, 1, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "Eytzinger non-branched, aligned with prefeching (and ffs) SA")
            {
                for(int i = this->txtSize + 1; i > 0; i--)
                    this->karySA[i] = this->karySA[i-1];
                
                  this->karySA++; 
            }

            ~EytzingerAdvancedSASearch() { 
                this->karySA--;
            }
            
            inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                unsigned int node = 0;
                beg = this->txtSize;
                
//                cout << endl;
                while (node <= this->lastNode) {
//                    __builtin_prefetch(this->karySA+(64*node + 95));
                    __builtin_prefetch(this->karySA + (16*node + 23));
//                    cout << "a:" << (this->karySA + node) << " p:" << this->karySA + (16*node + 23) << " ";
                    node = this->isPatternLowerThanSuffix(pattern, patternLength, node)?node*2 + 1:node*2 + 2;
                }
                beg = (node+1) >> __builtin_ffs(~(node+1));
                beg = (beg == 0) ? this->lastNode+1 : beg - 1;

                if (!incrementPattern(pattern, patternLength)) {
                    decrementPattern(pattern, patternLength);
                    return;
                };
                int c;
                end = beg;
                unsigned int i = end;

                if (plusK == 0) {
                    unsigned int endNode = index2node<2>(end);

                    while (true) {
                        if (this->isPatternLowerThanSuffix(pattern, patternLength, end))
                            break;
                        i = end + 1;
                        node = endNode;
                        if (!this->levelUpKaryNode(endNode, c)) {
                            end = this->txtSize;
                            break;
                        }
                        end = node2index<2>(endNode) + c;
                    }

                    if (end == beg) {
                        decrementPattern(pattern, patternLength);
                        return;
                    }

                } else {
                    if (this->findPlusKOccurrence(pattern, patternLength, i, plusK)) {
                        end = i;
                        decrementPattern(pattern, patternLength);
                        return;
                    }

                    unsigned int endNode = index2node<2>(end);
                    while (true) {
                        i = end + 1;
                        node = endNode;
                        if (!this->levelUpKaryNode(endNode, c)) {
                            end = this->txtSize;
                            break;
                        }
                        end = node2index<2>(endNode) + c;
                        if (this->isPatternLowerThanSuffix(pattern, patternLength, end))
                            break;
                    }

                }

                c = numOfElementInNode<2>(i);

                node = this->isPatternLowerThanSuffix(pattern, patternLength, node)?node*2 + 1:node*2 + 2;
                while (node <= this->lastNode) {
                    //node = childNode<2>(node, this->searchNode(pattern, patternLength, end, node, 0));
                    __builtin_prefetch(this->karySA+(16*node + 23));
                    node = this->isPatternLowerThanSuffix(pattern, patternLength, node)?node*2 + 1:node*2 + 2;
                }
                end = (node+1) >> __builtin_ffs(~(node+1));
                end = (end == 0) ? this->lastNode+1 : end - 1;
                decrementPattern(pattern, patternLength);
            }
               
        };
        
// UNFINISHED - works only halfway
        template<int k, class StringComparisonStrategyClass>
        class KarySASmartLCPSearch: public SASearchStrategyTemplate<KarySASmartLCPSearch<k, StringComparisonStrategyClass>, StringComparisonStrategyClass> {
        private:
            typedef KarySASmartLCPSearch<k, StringComparisonStrategyClass> ThisType;

            int levelsCount;
            int lastNode;
            
        public:
            KarySASmartLCPSearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : SASearchStrategyTemplate<ThisType, StringComparisonStrategyClass>(sa, txt, txtSize, toString(k) + "-arySASmartLCP(UNFINISHED)"),
                    levelsCount(karyLevels<k>(txtSize)),
                    lastNode((txtSize-1) / (k-1))
            {
                cout << "Generating simple " << toString(k) << "-arySA ..." << endl;
                this->sa = createSAkarySimple<k>(sa, txtSize);
            }

            ~KarySASmartLCPSearch() {
                delete[] this->sa;
            }
            
            inline void findFirstOccurrence(unsigned char* pattern, int patternLength, unsigned int &i) {
                // patternLength + 1 is a tweak to use with strncmp strategies
                unsigned int node = 0;                
                i = this->txtSize;
                
                while (node <= lastNode) {
                    unsigned int j = node2index<k>(node);
                    
                    unsigned int elems = k - 1;
                    if (node == lastNode)
                        elems = this->txtSize - j;
                    
                    int c;
                    for(c = 0; c < elems; c++) {
                        if (this->strcmpStrategy(pattern, this->txt + this->sa[j], patternLength + 1) <= 0) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                    
                    node = childNode<k>(node, c);
                }
            }
            
            inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                unsigned int l = 0;

                int lcp_l = 0;
                int lcp_r = 0;
                int lcp_beg = 0;
                int lcp_end = 0;
                
                const unsigned char* ptrn = pattern;
                unsigned char* stxt = this->txt;
                int lcp = 0;
            
                int cmpRes;
                unsigned int node = 0;                
                
                bool equalFound = false;
                
                while (node <= lastNode) {
                    unsigned int mid = node2index<k>(node);
                    
                    unsigned int elems = k - 1;
                    if (node == lastNode)
                        elems = this->txtSize - mid;
                    
                    int c;
                    for(c = 0; c < elems; c++) {
                        cmpRes = this->strcmpStrategy(ptrn, stxt + this->sa[mid], patternLength-lcp);
                        if (cmpRes > 0) {
                            beg = mid;
                            if ((lcp_beg = lcp_l = lcp + cmpRes - 1) <= lcp_r) {
                                lcp = lcp_l;
                                ptrn = pattern + lcp; stxt = this->txt + lcp;
                            } else if (lcp_r > lcp) {
                                lcp = lcp_r;
                                ptrn = pattern + lcp; stxt = this->txt + lcp;
                            }
                        } else if (cmpRes < 0) {
                            end = mid;
                            if ((lcp_end = lcp_r = lcp - cmpRes - 1) <= lcp_l) {
                                lcp = lcp_r;
                                ptrn = pattern + lcp; stxt = this->txt + lcp;
                            } else if (lcp_l > lcp) {
                                lcp = lcp_l;
                                ptrn = pattern + lcp; stxt = this->txt + lcp;
                            }
                            break;
                        } else if (!equalFound) {
                            beg = mid;
                            lcp_beg = lcp_r = patternLength;
                            equalFound = true;
                        }
                        
                        if (cmpRes <= 0) {
                            l = mid;
                            break;
                        }
                        mid++;
                    }
                    
                    node = childNode<k>(node, c);
                }
                
                unsigned int start = l;
                
                l = beg;
                lcp_l = lcp_beg;
                lcp_r = lcp_end;

                lcp = lcp_l < lcp_r ? lcp_l : lcp_r;
                ptrn = pattern + lcp; stxt = this->txt + lcp;
                
                beg = start;
            }

            unsigned int getIndexRankImpl(unsigned int i) { return getKaryIndexRank<k>(i, this->txtSize, this->lastNode); } 
            
            unsigned int* getKarySA() { return this->sa; }            
        };
    }
}

#endif	/* KARYSASEARCHSTRATEGIES_H */

