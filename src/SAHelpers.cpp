/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "SAHelpers.h"

string toString(unsigned long long value) {
        std::ostringstream oss;
        oss << value;
        return oss.str();
};

string toStringDouble(double value) {
        std::ostringstream oss;
        oss << value;
        return oss.str();
};

void *malloc2n(size_t s) {
    if (ALIGN_MEM2N < 4)
        return NULL;
    
    unsigned int align = POW(2, ALIGN_MEM2N);
    unsigned char *p;
    unsigned char *porig = (unsigned char*) malloc (s + align);
    if (porig == NULL) return NULL;             
    p = porig + (align - ((size_t) porig % align));
    *(((unsigned int*)p)-1) = p - porig;                         
    return p;
}

void free2n(void *p) {
    if ((p == NULL) || (ALIGN_MEM2N < 4))
        return;
    
    unsigned int align = POW(2, ALIGN_MEM2N);
    
    unsigned char *porig = (unsigned char*) p;
    porig = porig - *(((unsigned int*)porig)-1);
    free (porig);                  
}

bool incrementPattern(unsigned char* pattern, int patternLength) {
    do {
        if (++pattern[--patternLength])
            return true;
    } while(patternLength);
    return false;
}

void decrementPattern(unsigned char* pattern, int patternLength) {
    do {
        if (pattern[--patternLength]--)
            return;
    } while (patternLength);
}

unsigned long djb2(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}


