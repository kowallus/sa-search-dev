/* 
 * File:   karyLCPmixSASearchStrategies.h
 * Author: coach
 *
 * Created on 9 czerwca 2015, 12:06
 */

#ifndef KARYLCPMIXSASEARCHSTRATEGIES_H
#define	KARYLCPMIXSASEARCHSTRATEGIES_H

#include "karyLCPSASearchStrategies.h"

using namespace std;

namespace SASearch {

    namespace kary {    
 
        template<int k, int step, int lcpLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK = 0>
        class KaryLCPmixSASearch: public KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, KaryLCPmixSASearch<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK>> {
        private:
            typedef KaryLCPmixSASearch<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK> ThisType;
            typedef StringComparisonStrategyInterface<StrNCmp32bitsComparisonStrategyClass> StrNCmpComparisionStrategy;
        
        protected:
            
            unsigned int* mixSA; 
            
            unsigned int firstNoPrefixNode;
            unsigned int firstNoPrefixIndex;
            unsigned int* firstNoPrefixElem;
            
            inline unsigned int* prefixElemIndex2Ptr(unsigned int index) {
                return mixSA + index * (1 + prefixLengthInUints);
            }
            
            inline unsigned int* noPrefixElemIndex2Ptr(unsigned int index) {
                return firstNoPrefixElem + (index - firstNoPrefixIndex);
            }

            inline unsigned char* prefixElemPtr2Prefix(unsigned int* ptr) {
                return (unsigned char*) (ptr + 1);
            }
            
            inline unsigned int* elemPtr2SuffixPtr(unsigned int* ptr) {
                return ptr;
            }
            
        public:
            KaryLCPmixSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "LCPmixSA"),
                    mixSA(mixKaryWithPrefixes<k, prefixLengthInUints>(this->karySA, this->prefixes, txtSize, lcpLevels)),
                    firstNoPrefixNode((lcpLevels < this->levelsCount)?karyNodes(k, lcpLevels):(this->lastNode + 1)),
                    firstNoPrefixIndex(node2index<k>(firstNoPrefixNode)),
                    firstNoPrefixElem(prefixElemIndex2Ptr(firstNoPrefixIndex))
            {
            }

            KaryLCPmixSASearch(unsigned char* txt)
            : KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(txt, "LCPmixSA"),
                    firstNoPrefixNode(karyNodes(k, lcpLevels)),
                    firstNoPrefixIndex(node2index<k>(firstNoPrefixNode))
            {
            }
            
            inline void setKaryConfiguration(unsigned int* karyLCPSA, unsigned int size) {
                this->txtSize = size - this->prefixesSizeInUints;
                this->mixSA = karyLCPSA;
                this->firstNoPrefixElem = prefixElemIndex2Ptr(firstNoPrefixIndex);
                this->lastNode = (this->txtSize - 1) / (k-1);
            }
            
            inline void index2suffixIndex(unsigned int &index) {
                if (index >= firstNoPrefixIndex)
                    index += this->prefixesSizeInUints;
                else 
                    index += index * prefixLengthInUints;
            }
            
            inline void suffixIndex2index(unsigned int &suffixIndex) {
                if (suffixIndex > firstNoPrefixIndex + this->prefixesSizeInUints)
                    suffixIndex -= this->prefixesSizeInUints;
                else 
                    suffixIndex -= suffixIndex / (1 + prefixLengthInUints) * prefixLengthInUints;
            }
            
            void indexRankingCorrection() {
                unsigned int* suffixPtr = noPrefixElemIndex2Ptr(this->txtSize - 1);
                while (*suffixPtr == *(suffixPtr - 1)) {
                    this->txtSize--;
                    suffixPtr--;
                }
            }
            
            void nullKaryConfiguration() {
                this->mixSA = NULL;
            }
            
            unsigned int* createLCPSAkary(unsigned int *sa, unsigned int saSize, unsigned int* dest) { 
                unsigned int* karySA = createSAkarySimple<k>(sa, saSize);
                int size = (((saSize - 1) / (k - 1)) + 1) * (k - 1);
                prefixarray<prefixLengthInUints>* prefixes = createPrefixesCache<k, prefixLengthInUints>(karySA, this->txt, saSize, lcpLevels);

                unsigned int* mixSA = mixKaryWithPrefixes<k, prefixLengthInUints>(karySA, prefixes, saSize, lcpLevels);
                
                std::copy(mixSA, mixSA + size + this->prefixesSizeInUints, dest);

                #ifdef ALIGN_MEMORY
                free2n(karySA);
                free2n(prefixes);
                free2n(mixSA);
                #else
                delete[] karySA;
                delete[] prefixes;
                delete[] mixSA;
                #endif
            }
            
            ~KaryLCPmixSASearch() {
                #ifdef ALIGN_MEMORY
                free2n(mixSA);
                #else
                delete[] mixSA;
                #endif
            } 
            
            inline bool findPlusKOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, int counter) {
                unsigned int* ptr;
                if (this->firstNoPrefixIndex < i)
                    ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                else
                    ptr = this->mixSA + i * (1 + prefixLengthInUints);

                if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                    return true;
                
                while (counter--) {
                    if (!this->incrementKaryIndex(i))
                        return true;
                    
                    if (this->firstNoPrefixIndex < i)
                        ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                    else
                        ptr = this->mixSA + i * (1 + prefixLengthInUints);
                        
                    if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                        return true;
                }
                
                return false;
            }   
            
            virtual unsigned int getSuffixOffset(unsigned int index) { 
                return *((firstNoPrefixIndex < index)?noPrefixElemIndex2Ptr(index):prefixElemIndex2Ptr(index));
            };
            
            unsigned int* getKaryLCPmixSA() { return this->mixSA; }
            
            void copyLocationsImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                res.reserve(res.size() + end - beg);
                if (beg < this->firstNoPrefixIndex) {
                    unsigned int* ptr = this->prefixElemIndex2Ptr(beg);
                    while (beg < end && ptr != this->firstNoPrefixElem) {
                        res.push_back(*ptr);
                        beg++;
                        ptr += (1 + prefixLengthInUints);
                    }
                }

                if (beg < end)
                    res.insert(res.end(), this->noPrefixElemIndex2Ptr(beg), this->noPrefixElemIndex2Ptr(end));
            }
            
            
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node);
                int c;
                unsigned int* ptr = this->prefixElemIndex2Ptr(j + startElement + step - 1);
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->prefixElemPtr2Prefix(ptr), this->elemPtr2SuffixPtr(ptr))) {
                        i = j + c;
                        break;
                    }
                    ptr += step * (1 + prefixLengthInUints);
                }
                
                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    ptr -= (step - 1) * (1 + prefixLengthInUints);
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->prefixElemPtr2Prefix(ptr), this->elemPtr2SuffixPtr(ptr))) {
                            i = j + c;
                            break;
                        }
                        ptr += (1 + prefixLengthInUints);
                    }
                }
                this->lcpUpdate(c, this->prefixElemPtr2Prefix(ptr - (1 + prefixLengthInUints)), this->prefixElemPtr2Prefix(ptr));
                
                return c;
            }

            inline int searchNodeImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node);
                int c; 
                if (node <= this->lastNodeWithPrefixes) {
                    unsigned int* ptr = this->prefixElemIndex2Ptr(j + startElement + step - 1);
                    for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                        if (this->isPatternLowerThanSuffix(patternLength, this->elemPtr2SuffixPtr(ptr))) {
                            i = j + c;
                            break;
                        }
                        ptr += step * (1 + prefixLengthInUints);
                    }

                    if (step > 1) {
                        int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                        ptr -= (step - 1) * (1 + prefixLengthInUints);
                        for(c -= step - 1; c < guard; c++) {
                            if (this->isPatternLowerThanSuffix(patternLength, this->elemPtr2SuffixPtr(ptr))) {
                                i = j + c;
                                break;
                            }
                            ptr += (1 + prefixLengthInUints);
                        }
                    }
                } else {
                    unsigned int* ptr = this->noPrefixElemIndex2Ptr(j + startElement + step - 1);

                    for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                        if (this->isPatternLowerThanSuffix(patternLength, this->elemPtr2SuffixPtr(ptr))) {
                            i = j + c;
                            break;
                        }
                        ptr += step;
                    }

                    if (step > 1) {
                        int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                        ptr -= step - 1;
                        for(c -= step - 1; c < guard; c++) {
                            if (this->isPatternLowerThanSuffix(patternLength, this->elemPtr2SuffixPtr(ptr))) {
                                i = j + c;
                                break;
                            }
                            ptr++;
                        }
                    }
                }
                return c;
            }         
        };
    }
}

#endif	/* KARYLCPMIXSASEARCHSTRATEGIES_H */

