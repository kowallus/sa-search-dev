/* 
 * File:   SABinarySearchStrategies.h
 * Author: coach
 *
 * Created on 25 maja 2015, 13:04
 */

#ifndef SABINARYSEARCHSTRATEGIES_H
#define	SABINARYSEARCHSTRATEGIES_H

#include <string>
#include "SAkaryHelpers.h"

using namespace std;

namespace SASearch {
    
    template<class LCPStrNCmpComparisonStrategyClass, int plusK = 0>
    class SASmartLCPBinarySearch: public SASearchStrategyTemplate<SASmartLCPBinarySearch<LCPStrNCmpComparisonStrategyClass, plusK>, LCPStrNCmpComparisonStrategyClass> {
    private:
        typedef SASmartLCPBinarySearch<LCPStrNCmpComparisonStrategyClass, plusK> ThisType;

        int startLCP = 0;
        
    public:
        SASmartLCPBinarySearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int startLCP = 0)
        : SASearchStrategyTemplate<ThisType, LCPStrNCmpComparisonStrategyClass>(sa, txt, txtSize, "SmartLCPBinary" + plusKString(plusK)) {
            setStartLCP(startLCP);
        }
        
        inline void setStartLCP(int preSearchLCP) { 
            startLCP = preSearchLCP; 
        }
        
        inline void searchImpl(unsigned char* &pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int mid, l = beg;
            unsigned int r = end;
            
            int lcp_l = startLCP;
            int lcp_r = startLCP;
            int lcp_beg = startLCP;
            int lcp_end = startLCP;
        /**/
            int lcp = startLCP;
            const unsigned char* ptrn = pattern + lcp; 
            unsigned char* stxt = this->txt + lcp;

            int cmpRes;

            while (l < r) {
                mid = (l + r) / 2;

                cmpRes = this->strcmpStrategy(ptrn, stxt + this->sa[mid], patternLength-lcp);
                if (cmpRes > 0) {
                    beg = mid + 1;
                    l = mid + 1;
                    if ((lcp_beg = lcp_l = lcp + cmpRes - 1) <= lcp_r) {
                        lcp = lcp_l;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    } else if (lcp_r > lcp) {
                        lcp = lcp_r;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    }
                } else if (cmpRes < 0) {
                    end = mid;
                    r = mid - 1;
                    if ((lcp_end = lcp_r = lcp - cmpRes - 1) <= lcp_l) {
                        lcp = lcp_r;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    } else if (lcp_l > lcp) {
                        lcp = lcp_l;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    }
                } else {
                    if (beg < mid)
                        beg = mid + 1;
                    r = mid;
                    
                    lcp_beg = lcp_r = patternLength;
                }
            }

            if (l == r && l == beg) {
                // beg might be smaller then pattern
                cmpRes = this->strcmpStrategy(ptrn, stxt + this->sa[l], patternLength-lcp);
                if (cmpRes > 0) {
                    beg = ++l;
                    lcp_beg = lcp_l;
                } else if (cmpRes < 0) {
                    end = l;
                    lcp_end = lcp_l;
                } else 
                    lcp_beg = lcp_l;
            }

            unsigned int start = l;

            l = beg;
            r = end;
            lcp_l = lcp_beg;
            lcp_r = lcp_end;
            
            lcp = lcp_l < lcp_r ? lcp_l : lcp_r;
            ptrn = pattern + lcp; stxt = this->txt + lcp;
            
            if (plusK == -2) {
                if (this->strcmpStrategy(ptrn, stxt + this->sa[l], patternLength-lcp) < 0) {
                    beg = start;
                    end = l;
                    return;
                }

                r = l + 1;
                for (unsigned int i = 1; ;i <<= 1) {
                    if (r >= end) {
                        r = end;
                        break;
                    }
                    
                    if (this->strcmpStrategy(ptrn, stxt + this->sa[r], patternLength-lcp) < 0) {
                        if (i < 4) {
                            beg = start;
                            end = r;
                            return;
                        }
                        break;
                    }

                    l = r + 1;
                    r += i;
                }
            }
            
            if (plusK > 0)
                if (r > l + plusK) 
                    if (this->strcmpStrategy(ptrn, stxt + this->sa[l + plusK], patternLength-lcp) < 0)
                        r = l + plusK;
                    else
                        l = l + plusK + 1;
            
            while (l < r) {
                mid = (l + r) / 2;
                cmpRes = this->strcmpStrategy(ptrn, stxt + this->sa[mid], patternLength-lcp);
                if (cmpRes > 0) {
                    l = mid + 1;
                    if ((lcp_l = lcp + cmpRes - 1) <= lcp_r) {
                        lcp = lcp_l;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    } else if (lcp_r > lcp) {
                        lcp = lcp_r;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    }
                }
                else if (cmpRes < 0) {
                    r = mid;
                    if ((lcp_r = lcp - cmpRes - 1) <= lcp_l) {
                        lcp = lcp_r;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    } else if (lcp_l > lcp) {
                        lcp = lcp_l;
                        ptrn = pattern + lcp; stxt = this->txt + lcp;
                    }
                } else {
                    l = mid + 1;
                    lcp_l = patternLength;
                }
            }

            beg = start;
            end = l;
        }
    };
    
    template<class StrNCmpComparisonStrategyClass, int plusK = 0>
    class SASmartBinarySearchNCmp: public SASearchStrategyTemplate<SASmartBinarySearchNCmp<StrNCmpComparisonStrategyClass, plusK>, StrNCmpComparisonStrategyClass> {
    private:
        typedef SASmartBinarySearchNCmp<StrNCmpComparisonStrategyClass, plusK> ThisType;
        
        int startLCP = 0;
        
    public:
        SASmartBinarySearchNCmp(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int startLCP = 0)
        : SASearchStrategyTemplate<ThisType, StrNCmpComparisonStrategyClass>(sa, txt, txtSize, "SmartBinary" + plusKString(plusK)) {
            setStartLCP(startLCP);
        }
        
        inline void setStartLCP(int preSearchLCP) { 
            this->txt += preSearchLCP - startLCP;
            startLCP = preSearchLCP; 
        }
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int mid, l = beg;
            unsigned int r = end;
            int cmpRes;

            const unsigned char* ptrn = pattern + startLCP;
            patternLength -= startLCP;
            
            while (l < r) {
                mid = (l + r) / 2;

                cmpRes = this->strcmpStrategy(ptrn, this->txt + this->sa[mid], patternLength);
                if (cmpRes > 0) {
                    beg = mid + 1;
                    l = mid + 1;
                } else if (cmpRes < 0) {
                    end = mid;
                    r = mid - 1;
                } else {
                    if (beg < mid)
                        beg = mid + 1;
                    r = mid;
                }
            }

            if (l == r && l == beg) {
                // beg might be smaller then pattern
                cmpRes = this->strcmpStrategy(ptrn, this->txt + this->sa[l], patternLength);
                if (cmpRes > 0)
                    beg = ++l;
                else if (cmpRes < 0)
                    end = l;
                else if (beg < l)
                        beg = l + 1;
            }

            unsigned int start = l;

            l = beg;
            r = end;

            if (plusK == -2) {
                if (this->strcmpStrategy(ptrn, this->txt + this->sa[l], patternLength) < 0) {
                    beg = start;
                    end = l;
                    return;
                }
                
                r = l + 1;
                for (unsigned int i = 1; ; i <<= 1) {
                    if (r >= end) {
                        r = end;
                        break;
                    }
                    
                    if (this->strcmpStrategy(ptrn, this->txt + this->sa[r], patternLength) < 0) {
                        if (i < 4) {
                            beg = start;
                            end = r;
                            return;
                        }
                        break;
                    } 

                    l = r + 1;
                    r += i;
                }
            }
            
            if (plusK > 0)
                if (r > l + plusK) 
                    if (this->strcmpStrategy(ptrn, this->txt + this->sa[l + plusK], patternLength) < 0)
                        r = l + plusK;
                    else
                        l = l + plusK + 1;
            
            while (l < r) {
                mid = (l + r) / 2;
                cmpRes = this->strcmpStrategy(ptrn, this->txt + this->sa[mid], patternLength);
                if (cmpRes > 0) 
                    l = mid + 1;
                else if (cmpRes < 0)
                    r = mid;
                else
                    l = mid + 1;
            }

            beg = start;
            end = l;
        }
    };
    
    template<class StringComparisonStrategyClass, int plusK = 0>
    class SASimpleBinarySearch: public SASearchStrategyTemplate<SASimpleBinarySearch<StringComparisonStrategyClass, plusK>, StringComparisonStrategyClass> {
    private:
        typedef SASimpleBinarySearch<StringComparisonStrategyClass, plusK> ThisType;
        
        int startLCP = 0;
        
    public:
        SASimpleBinarySearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int startLCP = 0)
        : SASearchStrategyTemplate<ThisType, StringComparisonStrategyClass>(sa, txt, txtSize, "SimpleBinary" + plusKString(plusK)) {
            setStartLCP(startLCP);
//            this->sa = (unsigned int*) malloc128((txtSize + 1) * sizeof(unsigned int));
//            std::copy(sa, sa + txtSize, this->sa);
        }
       
        inline void setStartLCP(int preSearchLCP) { 
            this->txt += preSearchLCP - startLCP;
            startLCP = preSearchLCP; 
        }
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            // patternLength + 1 is a tweak to use with strncmp strategies
            const unsigned char* ptrn = pattern + startLCP;
            patternLength -= startLCP;
                        
            unsigned int mid, l = beg;
            
/*            // FIXME: buggy :(
            if (plusK == -4) {
                if (!this->strcmpStrategy(ptrn, this->txt + this->sa[l], patternLength + 1) <= 0) {
                    unsigned int maxGap = end - l;
                    for (unsigned int i = 1; ; i <<= 1) {

                        if (i >= maxGap) {
                            l = end;
                            break;
                        }
                        l = beg + i;
                        if (this->strcmpStrategy(ptrn, this->txt + this->sa[l], patternLength + 1) <= 0) {
                            if (i < 4)
                                beg = l;
                            break;
                        } 
                    }
                }
            } */
            unsigned int r = end;
            while (l < r) {
                mid = (l + r) / 2;
                if (this->strcmpStrategy(ptrn, this->txt + this->sa[mid], patternLength + 1) > 0) {
                    l = mid + 1;
                } else {
                    r = mid;
                }
            }
            beg = l;
            r = end;
            if (!incrementPattern(pattern + startLCP, patternLength)) {
                decrementPattern(pattern + startLCP, patternLength);
                return;
            };
            
            if (plusK == -1) {
                unsigned int maxGap = r - l;
                
                for (unsigned int i = 1; ; i <<= 1) {

                    if (i >= maxGap) {
                        r = end;
                        break;
                    }
                    r = l + i;
                    if (this->strcmpStrategy(ptrn, this->txt + this->sa[r], patternLength + 1) <= 0) {
                        if (i < 4) {
                            end = r;
                            decrementPattern(pattern + startLCP, patternLength);
                            return;
                        }
                        break;
                    } 
                }
            }
            
            if (plusK == -2) {
                if (this->strcmpStrategy(ptrn, this->txt + this->sa[l], patternLength + 1) <= 0) {
                    beg = 0; end = 0;
                    decrementPattern(pattern + startLCP, patternLength);
                    return;
                }
                
                r = l + 1;
                for (unsigned int i = 1; ; i <<= 1) {

                    if (r >= end) {
                        l = r - (i >> 2);
                        r = end;
                        break;
                    }
                    
                    if (this->strcmpStrategy(ptrn, this->txt + this->sa[r], patternLength + 1) <= 0) {
                        if (i < 4) {
                            l = r - (i >> 1);
                            end = r;
                            decrementPattern(pattern + startLCP, patternLength);
                            return;
                        }
                        break;
                    } 

                    r += i;
                }
            }
            
            if (plusK <= PLUSK_DOUBLING) {
                 if (this->strcmpStrategy(ptrn, this->txt + this->sa[l], patternLength + 1) <= 0) {
                    beg = 0; end = 0;
                    decrementPattern(pattern + startLCP, patternLength);
                    return;
                }
                
                unsigned int maxGap = r - l;
                for (unsigned int i = 1; ; i <<= 1) {

                    if (i >= maxGap) {
//                        l += (i >> 1);
                        r = end;
                        break;
                    }
                    r = l + i;
                    if (this->strcmpStrategy(ptrn, this->txt + this->sa[r], patternLength + 1) <= 0) {
                        if (i < 4) {
                            end = r;
                            decrementPattern(pattern + startLCP, patternLength);
                            return;
                        }
//                        l += (i >> 1);
                        break;
                    } 
                }
            }
            
            if (plusK > 0)
                if (r > l + plusK) 
                    if (this->strcmpStrategy(ptrn, this->txt + this->sa[l + plusK], patternLength + 1) < 0)
                        r = l + plusK;
                    else
                        l = l + plusK + 1;
                
            while (l < r) {
                mid = (l + r) / 2;
                if (this->strcmpStrategy(ptrn, this->txt + this->sa[mid], patternLength + 1) <= 0) {
                    r = mid;
                } else {
                    l = mid + 1;
                }
            }
            end = r;
            decrementPattern(pattern + startLCP, patternLength);
        }
        
    };
        
    template<class StringComparisonStrategyClass>
    class SASimpleBinarySearchWithBrutePlusPlus: public SASearchStrategyTemplate<SASimpleBinarySearchWithBrutePlusPlus<StringComparisonStrategyClass>, StringComparisonStrategyClass> {
    private:
        typedef SASimpleBinarySearchWithBrutePlusPlus<StringComparisonStrategyClass> ThisType;
        
    public:
        SASimpleBinarySearchWithBrutePlusPlus(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
        : SASearchStrategyTemplate<ThisType, StringComparisonStrategyClass>(sa, txt, txtSize, "SimpleBinary&Brute++") {
        }
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int mid, l = beg;
            unsigned int r = end;
            while (l < r) {
                mid = (l + r) / 2;
                if (this->strcmpStrategy(pattern, this->txt + this->sa[mid], patternLength + 1) > 0) {
                    l = mid + 1;
                } else {
                    r = mid;
                }
            }
            beg = l;
            r = l;
            if (!incrementPattern(pattern, patternLength)) {
                decrementPattern(pattern, patternLength);
                return;
            };
            while (this->strcmpStrategy(pattern, this->txt + this->sa[r], patternLength + 1) >= 0) {
                ++r;
            }
            end = r;
            decrementPattern(pattern, patternLength);
        }
    };
}

#endif	/* SABINARYSEARCHSTRATEGIES_H */

