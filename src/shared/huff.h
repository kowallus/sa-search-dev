#include <map>

#define ANY_SYMBOL 256

using namespace std;

struct pnode
{
  char ch; // char
  float p; // probability
};

struct treenode : public pnode
{
  char lcode;
  char rcode;
  treenode *left; // left child
  treenode *right; // right child
};

// returns codeword for each char
// ptable - table size (number of chars)
// tsize - table of probabilities)
map<char, string> EncHuffman(pnode *ptable, int tsize);

class CoderOrd0
{
private:
  unsigned int code[256] = {0};
  char codeBits[256] = {0};
  
public:
  CoderOrd0( const char *txt, int txtLen, int precSym = ANY_SYMBOL, int sndPrecSym = ANY_SYMBOL);
  unsigned int encode(const char *txt, int bits);
  unsigned int encode(const char *txt, int bits, int &codedBitsCount);
  virtual ~CoderOrd0();
  static string getName();
  static string getParamsString();
  static unsigned int getSizeInBytes();
  
  void copyCodes(unsigned int* code, char* codeBits);
};

class CoderOrd1
{
private:
  unsigned int codeT[257][256] = {{0}};
  char codeBitsT[257][256] = {{0}};
     
public:
  CoderOrd1( const char *txt, int txtLen);
  unsigned int encode(const char *txt, int bits);
  unsigned int encode(const char *txt, int bits, int &codedBitsCount);
  virtual ~CoderOrd1();
  static string getName();
  static string getParamsString();
  static unsigned int getSizeInBytes();
};

class CoderOrd2
{
private:
  unsigned int codeTT[257][257][256] = {{{0}}};
  char codeBitsTT[257][257][256] = {{{0}}};
  
public:
  CoderOrd2( const char *txt, int txtLen);
  unsigned int encode(const char *txt, int bits);
  unsigned int encode(const char *txt, int bits, int &codedBitsCount);
  virtual ~CoderOrd2();
  static string getName();
  static string getParamsString();
  static unsigned int getSizeInBytes();
};