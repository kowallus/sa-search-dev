/*
 *  Huffman coding algorithm
 *  by Sergey Tikhonov (st@haqu.net)
 * 
 *  Usage: huffman [OPTIONS] input [output]
 *    The default action is to encode input file.
 *    -d  Decode file.
 * 
 *  Examples:
 *    huffman input.txt
 *    huffman input.txt encoded.txt
 *    huffman -d encoded.txt
 */

#ifdef _WIN32
#define _CRT_SECURE_NO_DEPRECATE
#define NL "\r\n"
#else
#define NL "\n"
#endif

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <cstdio>
#include "huff.h"

using namespace std;

static int pnode_compare( const void *elem1, const void *elem2 )
{
  const pnode a = *(pnode*)elem1;
  const pnode b = *(pnode*)elem2;
  if( a.p < b.p ) return 1; // 1 - less (reverse for decreasing sort)
  else if( a.p > b.p ) return -1;
  return 0;
}

CoderOrd0::CoderOrd0( const char *txt, int txtLen, int precSym, int sndPrecSym)
{
    map<char, int> freqs; // frequency for each char from input text
    int i;

    //  Counting chars
    //
    unsigned total = 0;
    for(int i = 0; i < txtLen; i++)
        if ((precSym == ANY_SYMBOL || (i > 0 && txt[i - 1] == (char) precSym)) &&
            (sndPrecSym == ANY_SYMBOL || (i > 1 && txt[i - 2] == (char) sndPrecSym))) {
            freqs[txt[i]]++;
            total++;
        }
    int tsize = (int)freqs.size();

    //  Building decreasing freqs table
    //
    pnode* ptable = new pnode[tsize];
    assert( ptable );
    float ftot = float(total);
    map<char, int>::iterator fi;
    for( fi=freqs.begin(),i=0; fi!=freqs.end(); ++fi,++i )
    {
      ptable[i].ch = (*fi).first;
      ptable[i].p = float((*fi).second) / ftot;
    }
    qsort( ptable, tsize, sizeof(pnode), pnode_compare );

    //  Encoding
    //
    map<char, string> codes = EncHuffman(ptable, tsize);
    
    for( i=0; i<tsize; i++ )
    {
        codeBits[(unsigned char) ptable[i].ch] = codes[ptable[i].ch].length();
        code[(unsigned char) ptable[i].ch] =  strtol(codes[ptable[i].ch].data(), NULL, 2);
        if (codeBits[(unsigned char) ptable[i].ch] > 32) {
            cout << "UNSUPPORTED huff length";
            exit(0);
        }
//      printf("%d\t%s"NL, (int)(ptable[i].ch), codes[ptable[i].ch].c_str() );
    }
    
    delete[] ptable;
}

CoderOrd0::~CoderOrd0()
{
}

string CoderOrd0::getName() { return "huffOrd0"; }
string CoderOrd0::getParamsString() { return "0"; }

unsigned int CoderOrd0::getSizeInBytes() { return 256 * (sizeof(unsigned int) + sizeof(char));}

unsigned int CoderOrd0::encode(const char *txt, int bits) {
      int currLen = 0;
      unsigned long long res = 0;
      while (currLen < bits) {
        unsigned char ch = *(txt++);
        res = (res << codeBits[ch]) + code[ch];
        currLen += codeBits[ch];
      }
      return (unsigned int) (res >> (currLen - bits));
}

unsigned int CoderOrd0::encode(const char *txt, int bits, int &codedSymbolsCount) {
      int currLen = 0;
      unsigned long long res = 0;
      codedSymbolsCount = 0;
      while (currLen < bits) {
        unsigned char ch = *(txt++);
        codedSymbolsCount++;
        res = (res << codeBits[ch]) + code[ch];
        currLen += codeBits[ch];
      }
      if (currLen > bits)
          codedSymbolsCount--;
      return (unsigned int) (res >> (currLen - bits));
}

void CoderOrd0::copyCodes(unsigned int* code, char* codeBits) {
    std::copy(this->code, this->code + 256, code);
    std::copy(this->codeBits, this->codeBits + 256, codeBits);
}

/**/
/* v0
unsigned int Coder::encode(const char *txt, int bits) {
      int currLen = 0;
      unsigned long long res = 0;
      while (currLen < bits) {
        unsigned char ch = *(txt++);
        res |= code[ch] << currLen;
        currLen += codeBits[ch];        
      }
      return ((unsigned int) res) << (32 - bits) >> (32 - bits);
}
/**/

CoderOrd1::CoderOrd1( const char *txt, int txtLen)
{
    CoderOrd0 fstCoder(txt, txtLen);
    fstCoder.copyCodes(codeT[ANY_SYMBOL], codeBitsT[ANY_SYMBOL]);
    
    for (int i = 0; i < 256; i++) {
        if (codeBitsT[ANY_SYMBOL][i] > 0) {
            CoderOrd0 sndCoder(txt, txtLen, (char) ((unsigned char) i));
            sndCoder.copyCodes(codeT[i], codeBitsT[i]);
        }
    }   
}

CoderOrd1::~CoderOrd1()
{
}

string CoderOrd1::getName() { return "huffOrd1"; }
string CoderOrd1::getParamsString() { return "1"; }

unsigned int CoderOrd1::getSizeInBytes() { return 256 * 257 * (sizeof(unsigned int) + sizeof(char));}

unsigned int CoderOrd1::encode(const char *txt, int bits) {
      int precSym = ANY_SYMBOL;
      int currLen = 0;
      unsigned long long res = 0;
      while (currLen < bits) {
        unsigned char ch = *(txt++);
        res = (res << codeBitsT[precSym][ch]) + codeT[precSym][ch];
        currLen += codeBitsT[precSym][ch];
        precSym = ch;
      }
      return (unsigned int) (res >> (currLen - bits));
}

unsigned int CoderOrd1::encode(const char *txt, int bits, int &codedSymbolsCount) {
      int precSym = ANY_SYMBOL;
      int currLen = 0;
      unsigned long long res = 0;
      codedSymbolsCount = 0;
      while (currLen < bits) {
        unsigned char ch = *(txt++);
        codedSymbolsCount++;
        res = (res << codeBitsT[precSym][ch]) + codeT[precSym][ch];
        currLen += codeBitsT[precSym][ch];
        precSym = ch;
      }
      if (currLen > bits)
          codedSymbolsCount--;
      return (unsigned int) (res >> (currLen - bits));
}

CoderOrd2::CoderOrd2(const char *txt, int txtLen)
{
    CoderOrd0 fstCoder(txt, txtLen);
    fstCoder.copyCodes(codeTT[ANY_SYMBOL][ANY_SYMBOL], codeBitsTT[ANY_SYMBOL][ANY_SYMBOL]);
    
    for (int i = 0; i < 256; i++)
        if (codeBitsTT[ANY_SYMBOL][ANY_SYMBOL][i] > 0) {
            CoderOrd0 sndCoder(txt, txtLen, (char) ((unsigned char) i));
            sndCoder.copyCodes(codeTT[ANY_SYMBOL][i], codeBitsTT[ANY_SYMBOL][i]);
            for (int j = 0; j < 256; j++)
                if (codeBitsTT[ANY_SYMBOL][i][j] > 0) {
                    CoderOrd0 trdCoder(txt, txtLen, (char) ((unsigned char) j), (char) ((unsigned char) i));
                    trdCoder.copyCodes(codeTT[i][j], codeBitsTT[i][j]);
                }
        }
}

CoderOrd2::~CoderOrd2()
{
}

string CoderOrd2::getName() { return "huffOrd2"; }
string CoderOrd2::getParamsString() { return "2"; }

unsigned int CoderOrd2::getSizeInBytes() { return 256 * 257 * 257 * (sizeof(unsigned int) + sizeof(char));}

unsigned int CoderOrd2::encode(const char *txt, int bits) {
      int precSym = ANY_SYMBOL;
      int sndPrecSym = ANY_SYMBOL;
      int currLen = 0;
      unsigned long long res = 0;
      while (currLen < bits) {
        unsigned char ch = *(txt++);
        res = (res << codeBitsTT[sndPrecSym][precSym][ch]) + codeTT[sndPrecSym][precSym][ch];
        currLen += codeBitsTT[sndPrecSym][precSym][ch];
        sndPrecSym = precSym;
        precSym = ch;
      }
      return (unsigned int) (res >> (currLen - bits));
}

unsigned int CoderOrd2::encode(const char *txt, int bits, int &codedSymbolsCount) {
      int precSym = ANY_SYMBOL;
      int sndPrecSym = ANY_SYMBOL;
      int currLen = 0;
      unsigned long long res = 0;
      codedSymbolsCount = 0;
      while (currLen < bits) {
        unsigned char ch = *(txt++);
        codedSymbolsCount++;
        res = (res << codeBitsTT[sndPrecSym][precSym][ch]) + codeTT[sndPrecSym][precSym][ch];
        currLen += codeBitsTT[sndPrecSym][precSym][ch];
        sndPrecSym = precSym;
        precSym = ch;
      }
      if (currLen > bits)
          codedSymbolsCount--;
      return (unsigned int) (res >> (currLen - bits));
}

void GenerateCode(treenode *node, map<char, string> &codes) // for outside call: node is root
{
    static string sequence = "";
    if( node->left )
    {
      sequence += node->lcode;
      GenerateCode( node->left, codes );
    }

    if( node->right )
    {
      sequence += node->rcode;
      GenerateCode( node->right, codes );
    }

    if( !node->left && !node->right )
      codes[node->ch] = sequence;

    int l = (int)sequence.length();
    if( l > 1 ) sequence = sequence.substr( 0, l-1 );
    else sequence = "";
}

void DestroyNode( treenode *node ) // for outside call: node is root
{
    if( node->left )
    {
      DestroyNode( node->left );
      delete node->left;
      node->left = NULL;
    }

    if( node->right )
    {
      DestroyNode( node->right );
      delete node->right;
      node->right = NULL;
    }
}

map<char, string> EncHuffman(pnode *ptable, int tsize)
{
    //  Creating leaves (initial top-nodes)
    //
    treenode *n;
    vector<treenode*> tops; // top-nodes
    int i, numtop=tsize;
    for( i=0; i<numtop; i++ )
    {
      n = new treenode;
      assert( n );
      n->ch = ptable[i].ch;
      n->p = ptable[i].p;
      n->left = NULL;
      n->right = NULL;
      tops.push_back( n );
    }

    //  Building binary tree.
    //  Combining last two nodes, replacing them by new node
    //  without invalidating sort
    //
    while( numtop > 1 )
    {
      n = new treenode;
      assert( n );
      n->p = tops[numtop-2]->p + tops[numtop-1]->p;
      n->left = tops[numtop-2];
      n->right = tops[numtop-1];
      if( n->left->p < n->right->p )
      {
        n->lcode = '0';
        n->rcode = '1';
      }
      else
      {
        n->lcode = '1';
        n->rcode = '0';
      }
      tops.pop_back();
      tops.pop_back();
      bool isins = false;
      std::vector<treenode*>::iterator ti;
      for( ti=tops.begin(); ti!=tops.end(); ++ti )
        if( (*ti)->p < n->p )
        {
          tops.insert( ti, n );
          isins = true;
          break;
        }
      if( !isins ) tops.push_back( n );
      numtop--;
    }
    
    map<char, string> codes;
    //  Building codes
    //
    treenode *root = tops[0];
    GenerateCode( root, codes );

    //  Cleaning
    //
    DestroyNode( root );
    tops.clear();
    
    return codes;
}
  
