/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "bench/testingBench.h"
#include "testPlainKarySuite.h"
#include "testKaryLCPSASuite.h"
#include "testKaryLUTSASuite.h"

//#define TEST_SA
//#define TEST_LUTS
#define TEST_LUTS_HT
//#define TEST_LUTS_HTEX
#define TEST_LUTS_HT_VAR
//#define TEST_LUTS_HUFF
//#define TEST_KARY
//#define TEST_KARY_LCPSA_8
//#define TEST_KARY_LCPSA_PREFIX
//#define TEST_KARY_LCPSA_PREFIX_S
//#define TEST_KARY_SSELCPSA
//#define TEST_KARY_PLUSK
//#define TEST_KARY_LUTSA
//#define TEST_KARY_HT
//#define TEST_KARY_LUTS_HUFF_ORD0
//#define TEST_KARY_LUTS_HUFF_ORD0_26b
//#define TEST_KARY_LUTLCPSA_PREFIX_S
//#define TEST_KARY_LUTLCPSA_K_8_S_PART1
//#define TEST_KARY_LUTLCPSA_K_8_S_PART2
//#define TEST_KARY_LUTLCPSA_K_8_S_PART3
//#define TEST_KARY_LUTLCPSA_K_8_S_OK
//#define TEST_KARY_LUTLCPSA_FINAL
//#define TEST_KARY_LUTLCPSA_FINAL_HUFF

using namespace std;

void getUsage(char **argv) {
	cout << "Select what you want to test:" << endl;
	cout << "SA: ./" << argv[0] << " filePrefix patternNum patternLen [patternLenMax]" << endl;
        cout << "where:" << endl;
	cout << "fileName - name of text file" << endl;
	cout << "patternNum - number of patterns (queries)" << endl;
	cout << "patternLen - pattern length" << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    if (argc < 4) {
        getUsage(argv);
        exit(1);
    }
    
    const char *filePrefix = argv[1];
    unsigned int queriesNum = atoi(argv[2]);
    unsigned int m = atoi(argv[3]);
    string textFileName = string(filePrefix) + ".txt";
    
    if (argc == 5)
        initTestingBench(textFileName.c_str(), filePrefix, queriesNum, m, atoi(argv[4]));
    else 
        initTestingBench(textFileName.c_str(), filePrefix, queriesNum, m);
    
//    testSASearchStrategy<SASimpleBinarySearch<AsmLibStrCmp>>();

//    SAHuffLookupWithSATwist<CoderOrd2, 29>* lut29bOrd2 = new SAHuffLookupWithSATwist<CoderOrd2, 29>(saArray, txt, txtSize);
//    testSATwistStrategy<SAHuffLookupWithSATwist<CoderOrd2, 29>, SASimpleBinarySearch<AsmLibStrCmp, 0>>(lut29bOrd2);

/*  THE BEST VARIANT FOR PROTEINS200  
    CoderOrd0* coder = new CoderOrd0((const char*) txt, txtSize);
    SAHuffLookupWithSATwist<CoderOrd0, 27>* karyLUThuffTwisted27 = new SAHuffLookupWithSATwist<CoderOrd0, 27>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<CoderOrd0, 27>, KaryLUTWithSATwistSearch<5, 1, AsmLibStrCmp, SAHuffLookupWithSATwist<CoderOrd0, 27>, 0>>(karyLUThuffTwisted27);
    delete karyLUThuffTwisted27;
    delete coder;
 */
    
#ifdef TEST_SA
    setResultFile(string("results/kary/")+string(filePrefix)+".txt");
    testSASearchStrategy<SASimpleBinarySearch<BSwap32StrNECmp>>();
    testSASearchStrategy<SASimpleBinarySearch<BSwap32StrNECmp, PLUSK_DOUBLING>>();
    testSASearchStrategy<SASimpleBinarySearch<AsmLibStrCmp>>();
    testSASearchStrategy<SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>();
#endif

int hashK = 9;
    
#ifdef TEST_LUTS
    setResultFile(string("results/kary/")+string(filePrefix)+"_saluts.txt");
//    testSASearchStrategy<SASimpleBinarySearch<AsmLibStrCmp>>();

    saLUTHTVariantsTestsSet<AsmLibStrCmp, 0>(hashK);
    saLUTHTVariantsTestsSet<AsmLibStrCmp, PLUSK_DOUBLING>(hashK);
#endif 

#ifdef TEST_LUTS_HT
    setResultFile(string("results/kary/")+string(filePrefix)+"_saluts.txt");

    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash90p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, hashK<patternLength?hashK:patternLength-1, 90);
//    testSATwoStageSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, 0>>(saHash90p);
    testSATwoStageSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saHash90p);
    delete saHash90p;
#endif

#ifdef TEST_LUTS_HTEX
    setResultFile(string("results/kary/")+string(filePrefix)+"_saluts.txt");

    SAHashExLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHashEx90p = new SAHashExLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, hashK<patternLength?hashK:patternLength-1, 90);
    testSATwoStageSearchStrategy<SAHashExLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, 0>>(saHashEx90p);
    testSATwoStageSearchStrategy<SAHashExLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saHashEx90p);
    delete saHashEx90p;
#endif

#ifdef TEST_LUTS_HT_VAR
    setResultFile(string("results/kary/")+string(filePrefix)+"_saluts_var.txt");

    int minHashK = 4;
    int maxHashK = 10;
    int expectedMaxRange = 250;
    SAAutoKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saAutoKHash = new SAAutoKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, 
            3, maxHashK<patternLength?maxHashK:patternLength-1, expectedMaxRange, 10, 90);
    testSATwoStageSearchStrategy<SAAutoKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saAutoKHash);
    SATwoKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saTwoKHash = new SATwoKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, 
            minHashK<patternLength?minHashK:patternLength-1, maxHashK<patternLength?maxHashK:patternLength-1, expectedMaxRange, 13, 90);
    testSATwoStageSearchStrategy<SATwoKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saTwoKHash);
    SARangeKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saVarKHash90p = new SARangeKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, 
            minHashK<patternLength?minHashK:patternLength-1, maxHashK<patternLength?maxHashK:patternLength-1, expectedMaxRange, 20, 90);
//    testSATwoStageSearchStrategy<SARangeKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, 0>>(saVarKHash90p);
    testSATwoStageSearchStrategy<SARangeKHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saVarKHash90p);
    delete saVarKHash90p;
#endif
    
#ifdef TEST_LUTS_HUFF
    setResultFile(string("results/kary/")+string(filePrefix)+"_saluts_huff.txt");
    
    saLUTHuffVariantsTestsSet<AsmLibStrCmp, PLUSK_DOUBLING>();
#endif 
    
#ifdef TEST_KARY
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary"+".txt");
    karyVariantsPlannedTestsSet<AsmLibStrCmp, 0>();
#endif

#ifdef TEST_KARY_LCPSA_8
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LCPSA_8-chars"+".txt");
    karyLCPSAVariantsPlannedTestsSet<AsmLibStrCmp, 0>();
#endif

#ifdef TEST_KARY_SSELCPSA    
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_SSELCPSA"+".txt");

    testSASearchStrategy<K9arySSELCPSASearch<5, AsmLibStrCmp, 0>>();
    testSASearchStrategy<KaryLCPSASearch<9, 2, 5, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>>();
    testSASearchStrategy<K17arySSELCPSASearch<4, AsmLibStrCmp, 0>>();
    testSASearchStrategy<KaryLCPSASearch<17, 3, 4, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>>();    
#endif
    
#ifdef TEST_KARY_LCPSA_PREFIX
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LCPSA_prefixes"+".txt");
    karyLCPSAVariantsDiffPrefixPlannedTestsSet<AsmLibStrCmp, 0>();
#endif

#ifdef TEST_KARY_LCPSA_PREFIX_S
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LCPSA_prefixes_size"+".txt");
    karyLCPSAVariantsSameSizePlannedTestsSet<AsmLibStrCmp, 0>();
#endif

#ifdef TEST_KARY_PLUSK
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_plusK"+".txt");
    karyVariantsPlusKPlannedTestsSet<AsmLibStrCmp>(); 
    karyLCPSAVariantsPlusKPlannedTestsSet<AsmLibStrCmp>();
#endif

/*    CoderOrd0* coder = new CoderOrd0((const char*) txt, txtSize);
    SAHuffLookupWithSATwist<CoderOrd0, 25>* karyLUThuffTwisted = new SAHuffLookupWithSATwist<CoderOrd0, 25>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<CoderOrd0, 25>, KaryLUTWithSATwistSearch<3, 1, AsmLibStrCmp, SAHuffLookupWithSATwist<CoderOrd0, 25>, 0>>(karyLUThuffTwisted);
    cout << "DONE!";
/**/    
#ifdef TEST_KARY_LUTSA
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTSA"+".txt");
    karyLUTSAVariantsPlannedTestsSet<AsmLibStrCmp, 0>(hashK);
    karyLUTHuffSelectedVariantsTestsSet<CoderOrd1, AsmLibStrCmp>(false);
#endif

#ifdef TEST_KARY_HT
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_HT"+".txt");
    for(int hash_k = 8; hash_k < 12; hash_k++) {
        SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash90p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, hash_k<patternLength?hash_k:patternLength-1, 90);
        testSATwoStageSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp>>(saHash90p);
        testSATwoStageSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saHash90p);
        testSATwoStageKaryLUTSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTSASearch<5, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, 0>>(saHash90p);
        delete saHash90p;
    }
#endif
    
#ifdef TEST_KARY_LUTS_HUFF_ORD0
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTSHUFF_ORD0"+".txt");        
    karyLUTHuffSelectedVariantsTestsSet<CoderOrd0, AsmLibStrCmp>(true);
#endif

#ifdef TEST_KARY_LUTS_HUFF_ORD0_26b
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTSHUFF_ORD0"+".txt");        
    CoderOrd0* coder = new CoderOrd0((const char*) txt, txtSize);
    SAHuffLookupWithSATwist<CoderOrd0, 26>* karyLUThuffTwisted26 = new SAHuffLookupWithSATwist<CoderOrd0, 26>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<CoderOrd0, 26>, KaryLUTWithSATwistSearch<5, 1, AsmLibStrCmp, SAHuffLookupWithSATwist<CoderOrd0, 26>, 0>>(karyLUThuffTwisted26);
    delete karyLUThuffTwisted26;
    delete coder;
#endif
    
#ifdef TEST_KARY_LUTLCPSA_PREFIX_S
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTLCPSA_prefixes_size"+".txt");
    karyLCPLUTSAVariantsDiffPrefixPlannedTestsSet<5, 1, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsDiffPrefixPlannedTestsSet<33, 5, AsmLibStrCmp, 0>();
#endif

#ifdef TEST_KARY_LUTLCPSA_K_8_S_PART1
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTLCPSA_k_8_size"+".txt");
    karyLCPLUTSAVariantsDiffKPlannedTestsSet<3, 1, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsDiffKPlannedTestsSet<5, 1, AsmLibStrCmp, 0>();
//    karyLCPLUTSAVariantsDiffKPlannedTestsSet<9, 2, AsmLibStrCmp, 0>();
//    karyLCPLUTSAVariantsDiffKPlannedTestsSet<17, 3, AsmLibStrCmp, 0>();
//    karyLCPLUTSAVariantsDiffKPlannedTestsSet<25, 4, AsmLibStrCmp, 0>();
#endif
    
#ifdef TEST_KARY_LUTLCPSA_K_8_S_PART2
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTLCPSA_k_8_size"+".txt");
    karyLCPLUTSAVariantsDiffKPlannedTestsSet<33, 5, AsmLibStrCmp, 0>();
/*    karyLCPLUTSAVariantsDiffKPlannedTestsSet<41, 6, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsDiffKPlannedTestsSet<49, 7, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsDiffKPlannedTestsSet<57, 8, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsDiffKPlannedTestsSet<65, 9, AsmLibStrCmp, 0>();*/
#endif
    
#ifdef TEST_KARY_LUTLCPSA_K_8_S_PART3
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTLCPSA_k_8_size"+".txt");
//    karyLCPLUTSAVariantsDiffKLargeKPlannedTestsSet<3, 1, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsDiffKLargeKPlannedTestsSet<5, 1, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsDiffKLargeKPlannedTestsSet<33, 5, AsmLibStrCmp, 0>();
//    karyLCPLUTSAVariantsDiffKLargeKPlannedTestsSet<65, 9, AsmLibStrCmp, 0>();
#endif

#ifdef TEST_KARY_LUTLCPSA_K_8_S_OK
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTLCPSA_k_8_size_OK"+".txt");
    karyLCPLUTSAVariantsBestPlannedTestsSet<5, 1, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsBestPlannedTestsSet<33, 5, AsmLibStrCmp, 0>();
    karyLCPLUTSAVariantsBestPlannedTestsSet<65, 9, AsmLibStrCmp, 0>();
#endif

#ifdef TEST_KARY_LUTLCPSA_FINAL
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTLCPSA_FINAL"+".txt");
    karyLCPLUTSAVariantsFinalTestsSet<AsmLibStrCmp>(8);
#endif
    
#ifdef TEST_KARY_LUTLCPSA_FINAL_HUFF
    setResultFile(string("results/kary/")+string(filePrefix)+"_k-ary_LUTLCPSA_FINAL_HUFF"+".txt");
    karyLCPLUTSHuffVariantsFinalTestsSet<AsmLibStrCmp, CoderOrd0>(false);
#endif
    
    bestVariantsRanking();
    
    disposeTestingBench();

    return 0;
}