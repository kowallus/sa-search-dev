/* 
 * File:   karyLUTLCPSASearchStrategies.h
 * Author: coach
 *
 * Created on 29 maja 2015, 14:04
 */

#ifndef KARYLUTLCPSASEARCHSTRATEGIES_H
#define	KARYLUTLCPSASEARCHSTRATEGIES_H

#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"
#include "karyLUTSASearchStrategies.h"

#define ELEMENTSINNODE k - 1

using namespace std;

namespace SASearch {

    namespace kary {    
        
        template<int k, int step, class StringComparisonStrategyClass, class PreSearchStrategyClass, class KaryLCPSAStrategyClass, int plusK = 0>
        class KaryLUTLCPSASearch: public KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, plusK, KaryLUTLCPSASearch<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, KaryLCPSAStrategyClass, plusK>> {
        private:
            typedef KaryLUTLCPSASearch<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, KaryLCPSAStrategyClass, plusK> ThisType;
            
            typedef SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING> StandardSASearchStrategy;
            
            PreSearchStrategyClass* preSearchImpl;
            KaryLCPSAStrategyClass* karyLCPSASearch;
            
        protected:
            unsigned int minSizeForLCPSA;
            unsigned int additionalSizeInBytes = 0;
            
        public:
            KaryLUTLCPSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize, unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor)
            : KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, plusK, ThisType>(new PreSearchStrategyClass(sa, txt, txtSize, hashK, loadFactor), txt, txtSize, "SA"),
                    preSearchImpl((PreSearchStrategyClass*) (this->preSearch)),
                    karyLCPSASearch(new KaryLCPSAStrategyClass(txt + preSearchImpl->getLCP())),
                    minSizeForLCPSA(minRangeSizeForLUTWithPrefixes(sa, txt, txtSize, preSearchImpl, karyLCPSASearch, maxAdditionalSizeInBytes))
            {    
                this->name += " (>=" + toString(minSizeForLCPSA) + "; " 
                        + toString(((double) 100 * noOfElementsInLCPSARanges(sa, txt, txtSize, preSearchImpl, minSizeForLCPSA)) / txtSize) + "%: " 
                        + karyLCPSASearch->getStrategyName() + ")";
                this->karyLUTSA = createSAkaryForLUTWithPrefixes<k>(sa, txt, txtSize, preSearchImpl, karyLCPSASearch, minSizeForLCPSA, additionalSizeInBytes);
                this->sizeInBytes += additionalSizeInBytes;
                this->stdSearch = new StandardSASearchStrategy(this->karyLUTSA, txt, txtSize);
                this->stdSearch->setStartLCP(this->preSearchImpl->getLCP());
            }
            
            KaryLUTLCPSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize, unsigned int maxAdditionalSizeInBytes)
            : KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyClass, plusK, ThisType>(new PreSearchStrategyClass(sa, txt, txtSize), txt, txtSize, "SA"),
                    preSearchImpl((PreSearchStrategyClass*) (this->preSearch)),
                    karyLCPSASearch(new KaryLCPSAStrategyClass(txt + preSearchImpl->getLCP())),
                    minSizeForLCPSA(minRangeSizeForLUTWithPrefixes(sa, txt, txtSize, preSearchImpl, karyLCPSASearch, maxAdditionalSizeInBytes))
            {    
                this->name += " (>=" + toString(minSizeForLCPSA) + "; " 
                        + toString(((double) 100 * noOfElementsInLCPSARanges(sa, txt, txtSize, preSearchImpl, minSizeForLCPSA)) / txtSize) + "%: " 
                        + karyLCPSASearch->getStrategyName() + ")";
                this->karyLUTSA = createSAkaryForLUTWithPrefixes<k>(sa, txt, txtSize, preSearchImpl, karyLCPSASearch, minSizeForLCPSA, additionalSizeInBytes);
                this->sizeInBytes += additionalSizeInBytes;
                this->stdSearch = new StandardSASearchStrategy(this->karyLUTSA, txt, txtSize);
                this->stdSearch->setStartLCP(this->preSearchImpl->getLCP());
            }
            
            inline void useAddress4DataImpl(unsigned int* addr) {
                this->stdSearch->useAddress4Data(addr);
                this->preSearch->useAddress4Data(addr);
                std::copy(this->karyLUTSA + this->txtSize, this->karyLUTSA + this->txtSize + additionalSizeInBytes / sizeof(unsigned int), addr + this->txtSize);
                #ifdef ALIGN_MEMORY
                free2n(this->karyLUTSA);
                #else
                delete[] this->karyLUTSA;
                #endif
                this->karyLUTSA = addr;
                this->externalAddress4Data = true;
            }
            
            ~KaryLUTLCPSASearch() {
                karyLCPSASearch->nullKaryConfiguration();
                delete karyLCPSASearch;
            }
            
            string getParamsString() { 
                return toString(this->startLCP) + " " + this->preSearch->getParamsString() + " " + toString(k) + " " + toString(step) + " " + toString(plusK) + " " + this->karyLCPSASearch->getParamsString();
            }
            
            inline void searchKaryWithPrefixesLayout(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + beg, end - beg);
                
                this->cPatternPtr = pattern + this->startLCP;
                this->cPatternLength = patternLength - this->startLCP;
                
                unsigned int partBeg = beg;
                karyLCPSASearch->searchImpl(this->cPatternPtr, this->cPatternLength, beg, end);
                karyLCPSASearch->index2suffixIndex(beg);
                beg += partBeg;
                end += partBeg;
            }
            
            inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                this->preSearch->search(pattern, patternLength, beg, end); 
                
                this->ctxtSize = end - beg;
                
                if (this->ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (this->ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH)
                        this->linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    else
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    return;
                }
                
                if (this->ctxtSize >= minSizeForLCPSA) {
                    searchKaryWithPrefixesLayout(pattern, patternLength, beg, end);
                    return;
                }
                
                this->searchKaryLayout(pattern, patternLength, beg, end);
            } 
            
            inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
                this->preSearch->search(pattern, patternLength, beg, end); 
                this->ctxtSize = end - beg;
                
                if (this->ctxtSize >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize);
                    karyLCPSASearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);
                    karyLCPSASearch->indexRankingCorrection();
                    return karyLCPSASearch->resultCount(beg, end);
                }
                
                return this->localQueryCount(pattern, patternLength, beg, end);
            }

            void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                this->preSearch->search(pattern, patternLength, beg, end); 
                this->ctxtSize = end - beg;
                
                if (this->ctxtSize >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize);
                    karyLCPSASearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);
                    karyLCPSASearch->indexRankingCorrection();
                    karyLCPSASearch->resultLocate(beg, end, res);
                    return;
                }                

                this->localQueryLocate(pattern, patternLength, beg, end, res);
            }
            
            int resultCountImpl(unsigned int beg, unsigned int end) {
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + this->getSuffixOffset(beg), this->startLCP);
                unsigned int begIndex, endIndex;
                this->preSearch->search(prefixPtr, this->startLCP, begIndex, endIndex);
                
                if (endIndex - begIndex >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                    karyLCPSASearch->indexRankingCorrection();
                    beg -= begIndex;
                    karyLCPSASearch->suffixIndex2index(beg);
                    end -= begIndex;
                    return karyLCPSASearch->queryCount(beg, end);
                }
                
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT)
                    return end - beg;
                
                this->karySearch->setKaryConfiguration(0, endIndex - begIndex);
                return this->karySearch->resultCount(beg - begIndex, end - begIndex);
            }
            
            void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + this->getSuffixOffset(beg), this->startLCP);
                unsigned int begIndex, endIndex;
                this->preSearch->search(prefixPtr, this->startLCP, begIndex, endIndex);
                
                if (endIndex - begIndex >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                    karyLCPSASearch->indexRankingCorrection();
                    beg -= begIndex;
                    karyLCPSASearch->suffixIndex2index(beg);
                    end -= begIndex;
                    karyLCPSASearch->queryLocate(beg, end, res);
                    return;
                }
                
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT) {
                    res.insert(res.end(), this->karyLUTSA + beg, this->karyLUTSA + end);
                    return;
                }
                
                this->karySearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                this->karySearch->resultLocate(beg - begIndex, end - begIndex, res);
            }
            
            void getIndexRanksImpl(unsigned int& beg, unsigned int& end) {
                unsigned int begIndex, endIndex;
                this->preSearch->search(this->txt + this->getSuffixOffset(beg), this->startLCP, begIndex, endIndex);
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT)
                    return;
                if (endIndex - begIndex >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                    karyLCPSASearch->indexRankingCorrection();
                    beg -= begIndex;
                    karyLCPSASearch->suffixIndex2index(beg);
                    end -= begIndex;
                    karyLCPSASearch->getIndexRanks(beg, end);
                    beg += begIndex;
                    end += begIndex;
                    return;
                }
                
                beg = begIndex + getKaryIndexRank<k>(beg - begIndex, endIndex - begIndex, index2node<k>(endIndex - begIndex));
                end = begIndex + getKaryIndexRank<k>(end - begIndex, endIndex - begIndex, index2node<k>(endIndex - begIndex));
            }
            
            unsigned int* getKaryLUTLCPSA() { return this->karyLUTSA; }
        };

/* FIXME: Ranges start from different LCP, Currently createSAkaryForLUTWithSATwistWithPrefixes is not considering it properly :(*/
        template<int k, int step, class StringComparisonStrategyClass, class PreSearchStrategyWithSATwistClass, class KaryLCPSAStrategyClass, int plusK = 0>
        class KaryLUTWithSATwistLCPSASearch: public KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, plusK, KaryLUTWithSATwistLCPSASearch<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, KaryLCPSAStrategyClass, plusK>> {
        private:
            typedef KaryLUTWithSATwistLCPSASearch<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, KaryLCPSAStrategyClass, plusK> ThisType;
            
            typedef SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING> StandardSASearchStrategy;
            
            typedef KaryLUTSASearchTemplate<k, step, StringComparisonStrategyClass, PreSearchStrategyWithSATwistClass, plusK, ThisType> BaseClass;
            
            PreSearchStrategyWithSATwistClass* preSearchImpl;
            KaryLCPSAStrategyClass* karyLCPSASearch;
            
        protected:
            unsigned int minSizeForLCPSA;
            unsigned int additionalSizeInBytes = 0;
            
        public:
            
            KaryLUTWithSATwistLCPSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize, unsigned int maxAdditionalSizeInBytes, PreSearchStrategyWithSATwistClass* preSearch)
            : BaseClass(preSearch, txt, txtSize, "SA"),
                    preSearchImpl((PreSearchStrategyWithSATwistClass*) (this->preSearch)),
                    karyLCPSASearch(new KaryLCPSAStrategyClass(txt)),
                    minSizeForLCPSA(minRangeSizeForLUTWithPrefixes(sa, txt, txtSize, preSearchImpl, karyLCPSASearch, maxAdditionalSizeInBytes))
            {    
                this->name += " (>=" + toString(minSizeForLCPSA) + "; " 
                        + toString(((double) 100 * noOfElementsInLCPSARanges(sa, txt, txtSize, preSearchImpl, minSizeForLCPSA)) / txtSize) + "%: " 
                        + karyLCPSASearch->getStrategyName() + ")";
                this->karyLUTSA = createSAkaryForLUTWithSATwistWithPrefixes<k>(sa, txt, txtSize, preSearchImpl, karyLCPSASearch, minSizeForLCPSA, additionalSizeInBytes);
                this->sizeInBytes += additionalSizeInBytes;
                this->stdSearch = new StandardSASearchStrategy(this->karyLUTSA, txt, txtSize);
                this->stdSearch->setStartLCP(this->preSearchImpl->getLCP());
            }
            
            inline void useAddress4DataImpl(unsigned int* addr) {
                this->stdSearch->useAddress4Data(addr);
                this->preSearch->useAddress4Data(addr);
                std::copy(this->karyLUTSA + this->txtSize, this->karyLUTSA + this->txtSize + additionalSizeInBytes / sizeof(unsigned int), addr + this->txtSize);
                #ifdef ALIGN_MEMORY
                free2n(this->karyLUTSA);
                #else
                delete[] this->karyLUTSA;
                #endif
                this->karyLUTSA = addr;
                this->externalAddress4Data = true;
            }
            
            ~KaryLUTWithSATwistLCPSASearch() {
                karyLCPSASearch->nullKaryConfiguration();
                delete karyLCPSASearch;
            }
            
            string getParamsString() { 
                return toString(this->startLCP) + " " + this->preSearch->getParamsString() + " " + toString(k) + " " + toString(step) + " " + toString(plusK) + " " + this->karyLCPSASearch->getParamsString();
            }
            
            inline void searchKaryWithPrefixesLayout(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + beg, end - beg);
                
                this->cPatternPtr = pattern + this->startLCP;
                this->cPatternLength = patternLength - this->startLCP;
                
                unsigned int partBeg = beg;
                karyLCPSASearch->searchImpl(this->cPatternPtr, this->cPatternLength, beg, end);
                karyLCPSASearch->index2suffixIndex(beg);
                beg += partBeg;
                end += partBeg;
            }
            
            inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
                this->preSearch->search(pattern, patternLength, beg, end); 
                
                this->ctxtSize = end - beg;
                
                if (this->ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (this->ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH)
                        this->linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    else
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    return;
                }
                
                if (this->ctxtSize >= minSizeForLCPSA) {
                    searchKaryWithPrefixesLayout(pattern, patternLength, beg, end);
                    return;
                }
                
                this->searchKaryLayout(pattern, patternLength, beg, end);
            } 
            
             inline int localQueryCount(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {     
                this->startLCP = (((PreSearchStrategyWithSATwistClass*) this->preSearch)->getCurrentLCP());
                if (this->ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (this->ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH) {
                        this->ctxt = this->txt + this->startLCP;
                        this->linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    } else {
                        ((typename BaseClass::StandardSASearchStrategy*) (this->stdSearch))->setStartLCP(this->startLCP);
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    }
                    return end - beg;
                }
                
                this->karySearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize, this->txt + this->startLCP);
                this->karySearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);                

                return this->karySearch->resultCount(beg, end);
            }

            void localQueryLocate(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                if (this->ctxtSize < MIN_SIZE_FOR_KARY_LAYOUT) {
                    if (this->ctxtSize <= MAX_SIZE_FOR_LINEAR_SEARCH) {
                        this->ctxt = this->txt + this->startLCP;
                        this->linearSearchOnStandardLayout(pattern, patternLength, beg, end);
                    }else {
                        ((typename BaseClass::StandardSASearchStrategy*) (this->stdSearch))->setStartLCP(this->startLCP);
                        this->stdSearch->search(pattern, patternLength, beg, end);
                    }
                    res.insert(res.end(), this->karyLUTSA + beg, this->karyLUTSA + end);
                    return;
                }
                
                this->karySearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize, this->txt + this->startLCP);
                this->karySearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);
                this->karySearch->resultLocate(beg, end, res);
            }

            inline int queryCountImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end) {
                this->preSearch->search(pattern, patternLength, beg, end); 
                this->startLCP = (((PreSearchStrategyWithSATwistClass*) this->preSearch)->getCurrentLCP());
                this->ctxtSize = end - beg;
                if (this->ctxtSize >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize, this->txt + this->startLCP);
                    karyLCPSASearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);
                    karyLCPSASearch->indexRankingCorrection();
                    return karyLCPSASearch->resultCount(beg, end);
                }
                
                return this->localQueryCount(pattern, patternLength, beg, end);
            }

            void queryLocateImpl(unsigned char* pattern, int patternLength, unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                this->preSearch->search(pattern, patternLength, beg, end);
                this->startLCP = (((PreSearchStrategyWithSATwistClass*) this->preSearch)->getCurrentLCP());
                this->ctxtSize = end - beg;
                
                if (this->ctxtSize >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + beg, this->ctxtSize, this->txt + this->startLCP);
                    karyLCPSASearch->searchImpl(pattern + this->startLCP, patternLength - this->startLCP, beg, end);
                    karyLCPSASearch->indexRankingCorrection();
                    karyLCPSASearch->resultLocate(beg, end, res);
                    return;
                }                

                this->localQueryLocate(pattern, patternLength, beg, end, res);
            }
            
            int resultCountImpl(unsigned int beg, unsigned int end) {
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + this->getSuffixOffset(beg), this->startLCP);
                unsigned int begIndex, endIndex;
                this->preSearch->search(prefixPtr, this->startLCP, begIndex, endIndex);
                
                if (endIndex - begIndex >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                    karyLCPSASearch->indexRankingCorrection();
                    beg -= begIndex;
                    karyLCPSASearch->suffixIndex2index(beg);
                    end -= begIndex;
                    return karyLCPSASearch->queryCount(beg, end);
                }
                
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT)
                    return end - beg;
                
                this->karySearch->setKaryConfiguration(0, endIndex - begIndex);
                return this->karySearch->resultCount(beg - begIndex, end - begIndex);
            }
            
            void resultLocateImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                unsigned char prefixPtr[MAX_LOOKUP_LCP + 1];
                strncpy((char*) prefixPtr, (char*) this->txt + this->getSuffixOffset(beg), this->startLCP);
                unsigned int begIndex, endIndex;
                this->preSearch->search(prefixPtr, this->startLCP, begIndex, endIndex);
                
                if (endIndex - begIndex >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                    karyLCPSASearch->indexRankingCorrection();
                    beg -= begIndex;
                    karyLCPSASearch->suffixIndex2index(beg);
                    end -= begIndex;
                    karyLCPSASearch->queryLocate(beg, end, res);
                    return;
                }
                
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT) {
                    res.insert(res.end(), this->karyLUTSA + beg, this->karyLUTSA + end);
                    return;
                }
                
                this->karySearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                this->karySearch->resultLocate(beg - begIndex, end - begIndex, res);
            }
            
            void getIndexRanksImpl(unsigned int& beg, unsigned int& end) {
                unsigned int begIndex, endIndex;
                this->preSearch->search(this->txt + this->getSuffixOffset(beg), this->startLCP, begIndex, endIndex);
                if (endIndex - begIndex < MIN_SIZE_FOR_KARY_LAYOUT)
                    return;
                if (endIndex - begIndex >= minSizeForLCPSA) {
                    karyLCPSASearch->setKaryConfiguration(this->karyLUTSA + begIndex, endIndex - begIndex);
                    karyLCPSASearch->indexRankingCorrection();
                    beg -= begIndex;
                    karyLCPSASearch->suffixIndex2index(beg);
                    end -= begIndex;
                    karyLCPSASearch->getIndexRanks(beg, end);
                    beg += begIndex;
                    end += begIndex;
                    return;
                }
                
                beg = begIndex + getKaryIndexRank<k>(beg - begIndex, endIndex - begIndex, index2node<k>(endIndex - begIndex));
                end = begIndex + getKaryIndexRank<k>(end - begIndex, endIndex - begIndex, index2node<k>(endIndex - begIndex));
            }
            
            unsigned int* getKaryLUTLCPSA() { return this->karyLUTSA; }
        };
        
    }
}

#endif	/* KARYLUTLCPSASEARCHSTRATEGIES_H */

