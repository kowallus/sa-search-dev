/* 
 * File:   karySSELCPSASearchStrategies.h
 * Author: coach
 *
 * Created on 12 kwietnia 2016, 13:35
 */

#ifndef KARYSSELCPSASEARCHSTRATEGIES_H
#define	KARYSSELCPSASEARCHSTRATEGIES_H

#include "karyLCPSASearchStrategies.h"
#include "karyLUTLCPSASearchStrategies.h"

#include <immintrin.h>

using namespace std;

namespace SASearch {

    namespace kary {    
        
        typedef long long int v2di __attribute__ ((vector_size (16)));
        typedef double v2df __attribute__ ((vector_size (16)));
 /*
        typedef short int v4si __attribute__ ((vector_size (8)));
        v4si a = {10, 3, 4, 12};
        v4si b = {10, 10, 10, 10};
        cout << "TEST: " << (unsigned long long int) __builtin_ia32_pcmpgtw (b, a) << " " << __builtin_popcountll( (unsigned long long int) __builtin_ia32_pcmpgtw (b, a)) << "\n";

        
        v2di a = {10, 3};
        v2di b = {10, 10};
        v2di c = __builtin_ia32_pcmpgtq (b, a);
        unsigned long long int p0 = __builtin_popcountll(c[0]);
        unsigned long long int p1 = __builtin_popcountll(c[1]);
        unsigned long long int p0 = __builtin_popcountll(c[0]);
        unsigned long long int p1 = __builtin_popcountll(c[1]);
        cout << "TEST: " << c[0] << " " << c[1] << " " << p0 << " " << p1 << "\n";
 */ 
        
        template<int lcpLevels, class StringComparisonStrategyClass, int plusK = 0>
        class K17arySSELCPSASearch: public KaryLCPSASearchTemplate<17, 3, lcpLevels, 2, Swapped64StrSECmp, StringComparisonStrategyClass, plusK, K17arySSELCPSASearch<lcpLevels, StringComparisonStrategyClass, plusK>> {
        private:
            typedef K17arySSELCPSASearch<lcpLevels,StringComparisonStrategyClass, plusK> ThisType;
            
            const int k = 17;
            const int step = 3;
            
            //FIXME: limited lenght of a pattern
            unsigned char revPattern[1024];
            unsigned char* revPtrn;
            
        public:
            K17arySSELCPSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : KaryLCPSASearchTemplate<17, 3, lcpLevels, 2, Swapped64StrSECmp, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "SSELCPSA")
            { 
                for(unsigned int i = 0; i < this->prefixesCount; i++)
                    prefixReverse<8>(this->prefixes[i]);
            }

            K17arySSELCPSASearch(unsigned char* txt)
            : KaryLCPSASearchTemplate<17, 3, lcpLevels, 2, Swapped64StrSECmp, StringComparisonStrategyClass, plusK, ThisType>(txt, "SSELCPSA")
            { 
            }
            
            ~K17arySSELCPSASearch() {
            }

            inline void lcpInit(const unsigned char* pattern, int patternLength) {
                LCPSASearchCoordinator<17, lcpLevels, 2, Swapped64StrSECmp>::lcpInit(pattern, patternLength);
                const unsigned char* p = pattern + patternLength;
                unsigned char* q = this->revPattern;
                for(--p; p >= pattern; --p)
                    *(q++) = *p;
                this->revPtrn = this->revPattern + patternLength - 8;
            }
            
            inline void lcpUpdate(int child, unsigned char* prevPrefix, unsigned char* currentPrefix) {
                this->revPtrn += this->lcp;
                if (child > 0) {
                    this->lPrefix = prevPrefix;
                    this->lLCP = this->lcp;
                    if (child < k - 1) {
                        this->rPrefix = currentPrefix;
                        this->rLCP = this->lcp;
                        this->lcp += revstrlcp(this->lPrefix, this->rPrefix, 2 * sizeof(unsigned int));
                    } else {
                        int lcpShift = this->lcp - this->rLCP;
                        this->lcp += revstrlcp(this->lPrefix + lcpShift, this->rPrefix, 2 * sizeof(unsigned int) - lcpShift);
                    }
                } else {
                    this->rPrefix = currentPrefix;
                    this->rLCP = this->lcp;
                    int lcpShift = this->lcp - this->lLCP;
                    this->lcp += revstrlcp(this->lPrefix, this->rPrefix + lcpShift, 2 * sizeof(unsigned int) - lcpShift);
                }
                this->ptrn = this->patternPtr + this->lcp;
                this->revPtrn -= this->lcp;
                this->stxt = this->txtStart + this->lcp;
            }
            
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned char* prefixPtr, unsigned int *sufOffset) {
                int cmpres = this->strncmpCompare->compare(this->revPtrn, prefixPtr, 8);
                #ifdef LCP_STATS_ON
                this->updateStats(cmpres, level);
                #endif
                return (cmpres < 0 || (cmpres == 0 && this->strcmpStrategy(this->ptrn + 8, this->stxt + *sufOffset + 8, patternLength - this->lcp - 8 + 1) <= 0));
            }   
                   
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned int suffixIndex) {
                return isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->prefixes[suffixIndex], this->karySA + suffixIndex);
            }
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCache(int patternLength, unsigned char* fstPrefixPtr, unsigned int *fstSufOffset, unsigned char* sndPrefixPtr, unsigned int *sndSufOffset) {
                if (isPatternLowerThanSuffixUsingPrefixesCache(patternLength, fstPrefixPtr, fstSufOffset))
                    return 0;
                if (isPatternLowerThanSuffixUsingPrefixesCache(patternLength, sndPrefixPtr, sndSufOffset))
                    return 1;
                return 2;
            }
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCache(int patternLength, unsigned int fstSuffixIndex, unsigned int sndSuffixIndex) {
                return firstSuffixGreaterOrEqualToPatternUsingPrefixesCache(patternLength, this->prefixes[fstSuffixIndex], this->karySA + fstSuffixIndex, this->prefixes[sndSuffixIndex], this->karySA + sndSuffixIndex);
            }

            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(int patternLength, unsigned char* fstPrefixPtr, unsigned int *fstSufOffset) {
                v2di suffixesVector = { *((long long int*) fstPrefixPtr) - 0x8000000000000000, *((long long int*) (fstPrefixPtr+8)) - 0x8000000000000000};
                v2di patternVector = {*((long long int*) this->revPtrn) - 0x8000000000000000, *((long long int*) this->revPtrn) - 0x8000000000000000};
                v2di cmpVector = __builtin_ia32_pcmpgtq(patternVector, suffixesVector);
                int suffixesLowerThenPattern = (int) ((cmpVector[0] & 1)+(cmpVector[1] & 1));
                if (suffixesLowerThenPattern == 2)
                    return 2;
                cmpVector = __builtin_ia32_pcmpgtq (suffixesVector, patternVector);
                int suffixesGreaterThenPattern = (int) ((cmpVector[0] & 1)+(cmpVector[1] & 1));                
                if (suffixesGreaterThenPattern == 2)
                    return 0;
                if (suffixesGreaterThenPattern == 1) {
                    if (suffixesLowerThenPattern == 1)
                        return 1;
                    return 1 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *fstSufOffset + 8, patternLength - this->lcp - 8 + 1) <= 0);
                }
                if (suffixesLowerThenPattern == 1)
                    return 2 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *(fstSufOffset + 1) + 8, patternLength - this->lcp - 8 + 1) <= 0);
                if (this->strcmpStrategy(this->ptrn + 8, this->stxt + *fstSufOffset + 8, patternLength - this->lcp - 8 + 1) <= 0)
                    return 0;
                return 2 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *(fstSufOffset + 1) + 8, patternLength - this->lcp - 8 + 1) <= 0);
            }    
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(int patternLength, unsigned int fstSuffixIndex) {
                return firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(patternLength, this->prefixes[fstSuffixIndex], this->karySA + fstSuffixIndex);
            }
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(int patternLength, unsigned char* fstPrefixPtr, unsigned int *fstSufOffset, unsigned char* sndPrefixPtr, unsigned int *sndSufOffset) {
                v2di suffixesVector = { *((long long int*) fstPrefixPtr) - (long long int) 0x8000000000000000, *((long long int*) (sndPrefixPtr)) - (long long int) 0x8000000000000000};
                v2di patternVector = {*((long long int*) this->revPtrn) - (long long int) 0x8000000000000000, *((long long int*) this->revPtrn) - (long long int) 0x8000000000000000};
//                v2di cmpVector = { (long long int) -(suffixesVector[0] < patternVector[0]), (long long int) -(suffixesVector[1] < patternVector[1]) } ;
                v2di cmpVector = __builtin_ia32_pcmpgtq(patternVector, suffixesVector);
                int suffixesLowerThenPatternMM = __builtin_ia32_movmskpd((v2df) cmpVector);
                if (suffixesLowerThenPatternMM == 3)
                    return 2;
//                cmpVector = (v2di) { (long long int) -(suffixesVector[0] > patternVector[0]), (long long int) -(suffixesVector[1] > patternVector[1]) };
                cmpVector = __builtin_ia32_pcmpgtq (suffixesVector, patternVector);
                int suffixesGreaterThenPatternMM = __builtin_ia32_movmskpd((v2df) cmpVector);
                if (suffixesGreaterThenPatternMM == 3)
                    return 0;
                if (suffixesGreaterThenPatternMM == 2) {
                    if (suffixesLowerThenPatternMM == 1)
                        return 1;
                    return 1 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *fstSufOffset + 8, patternLength - this->lcp - 8 + 1) < 0);
                }
                if (suffixesLowerThenPatternMM == 1)
                    return 2 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *sndSufOffset + 8, patternLength - this->lcp - 8 + 1) < 0);
                if (this->strcmpStrategy(this->ptrn + 8, this->stxt + *fstSufOffset + 8, patternLength - this->lcp - 8 + 1) < 0)
                    return 0;
                return 2 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *sndSufOffset + 8, patternLength - this->lcp - 8 + 1) < 0);
            }    
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(int patternLength, unsigned int fstSuffixIndex, unsigned int sndSuffixIndex) {
                return firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(patternLength, this->prefixes[fstSuffixIndex], this->karySA + fstSuffixIndex, this->prefixes[sndSuffixIndex], this->karySA + sndSuffixIndex);
            }
/*
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<17>(node) + startElement;

                int c, res = 2;
                for(c = startElement; c < ELEMENTSINNODE; c += 2) {
                    if ((res = this->firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(patternLength, j)) < 2) {
                        c += res;
                        j += res;
                        i = j;
                        break;
                    }
                    j += 2;
                }
                
                this->lcpUpdate(c, this->prefixes[j - 1], this->prefixes[j]);
                return c;
            }
*/            
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<17>(node) + startElement + (step - 1);
// WITHSSE               
                int c, res = 2;
                for(c = startElement + step - 1; c + step < ELEMENTSINNODE; c += 2 * step) {
                    if ((res = this->firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(patternLength, j, j + step)) < 2) {
                        c += step * res;
                        j += step * res;
                        i = j;
                        break;
                    }
                    j += 2 * step;
                }
                
                if (res == 2 && c < ELEMENTSINNODE) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, j))
                        i = j;
                    else {
                        c += step;
                        j += step;
                    }
                }
/* // WITHOUT SSE           
                int c;
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }
*/
                int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;

                j -= step - 1;
                for(c -= step - 1; c < guard; c++) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j++;
                }

                this->lcpUpdate(c, this->prefixes[j - 1], this->prefixes[j]);
                return c;
            }

            inline int searchNodeImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<17>(node) + startElement + (step - 1);
                int c;
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                return c;
            }
               
        };
        
        template<int lcpLevels, class StringComparisonStrategyClass, int plusK = 0>
        class K9arySSELCPSASearch: public KaryLCPSASearchTemplate<9, 2, lcpLevels, 2, Swapped64StrSECmp, StringComparisonStrategyClass, plusK, K9arySSELCPSASearch<lcpLevels, StringComparisonStrategyClass, plusK>> {
        private:
            typedef K9arySSELCPSASearch<lcpLevels,StringComparisonStrategyClass, plusK> ThisType;
            
            const int k = 9;
            const int step = 2;
            
            //FIXME: limited lenght of a pattern
            unsigned char revPattern[1024];
            v2di patternVector;
            
            unsigned char* revPtrn;
            
        public:
            K9arySSELCPSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : KaryLCPSASearchTemplate<9, 2, lcpLevels, 2, Swapped64StrSECmp, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "SSELCPSA")
            { 
                for(unsigned int i = 0; i < this->prefixesCount; i++) {
                    prefixReverse<8>(this->prefixes[i]);
                    this->prefixes[i][7] -= 128;
                }
            }

            K9arySSELCPSASearch(unsigned char* txt)
            : KaryLCPSASearchTemplate<9, 2, lcpLevels, 2, Swapped64StrSECmp, StringComparisonStrategyClass, plusK, ThisType>(txt, "SSELCPSA")
            { 
            }
            
            ~K9arySSELCPSASearch() {
            }

            inline void lcpInit(const unsigned char* pattern, int patternLength) {
                LCPSASearchCoordinator<9, lcpLevels, 2, Swapped64StrSECmp>::lcpInit(pattern, patternLength);
                const unsigned char* p = pattern + patternLength;
                unsigned char* q = this->revPattern;
                for(--p; p >= pattern; --p)
                    *(q++) = *p;
                this->revPtrn = this->revPattern + patternLength - 8;
                patternVector = (v2di) {*((long long int*) this->revPtrn) - (long long int) 0x8000000000000000, *((long long int*) this->revPtrn) - (long long int) 0x8000000000000000};
            }
            
            inline void lcpUpdate(int child, unsigned char* prevPrefix, unsigned char* currentPrefix) {
                this->revPtrn += this->lcp;
                if (child > 0) {
                    this->lPrefix = prevPrefix;
                    this->lLCP = this->lcp;
                    if (child < k - 1) {
                        this->rPrefix = currentPrefix;
                        this->rLCP = this->lcp;
                        this->lcp += revstrlcp(this->lPrefix, this->rPrefix, 2 * sizeof(unsigned int));
                    } else {
                        int lcpShift = this->lcp - this->rLCP;
                        this->lcp += revSignedStrlcp(this->lPrefix, lcpShift, this->rPrefix, 2 * sizeof(unsigned int) - lcpShift);
                    }
                } else {
                    this->rPrefix = currentPrefix;
                    this->rLCP = this->lcp;
                    int lcpShift = this->lcp - this->lLCP;
                    this->lcp += revSignedStrlcp(this->rPrefix, lcpShift, this->lPrefix, 2 * sizeof(unsigned int) - lcpShift);
                }
                this->ptrn = this->patternPtr + this->lcp;
                this->revPtrn -= this->lcp;
                this->stxt = this->txtStart + this->lcp;
                patternVector = (v2di) {*((long long int*) this->revPtrn) - (long long int) 0x8000000000000000, *((long long int*) this->revPtrn) - (long long int) 0x8000000000000000};
            }
            
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned char* prefixPtr, unsigned int *sufOffset) {
                int cmpres = this->strncmpCompare->compare(this->revPtrn, prefixPtr, 8);
                #ifdef LCP_STATS_ON
                this->updateStats(cmpres, level);
                #endif
                return (cmpres < 0 || (cmpres == 0 && this->strcmpStrategy(this->ptrn + 8, this->stxt + *sufOffset + 8, patternLength - this->lcp - 8 + 1) <= 0));
            }   
                   
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned int suffixIndex) {
                return isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->prefixes[suffixIndex], this->karySA + suffixIndex);
            }
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCache(int patternLength, unsigned char* fstPrefixPtr, unsigned int *fstSufOffset, unsigned char* sndPrefixPtr, unsigned int *sndSufOffset) {
                if (isPatternLowerThanSuffixUsingPrefixesCache(patternLength, fstPrefixPtr, fstSufOffset))
                    return 0;
                if (isPatternLowerThanSuffixUsingPrefixesCache(patternLength, sndPrefixPtr, sndSufOffset))
                    return 1;
                return 2;
            }
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCache(int patternLength, unsigned int fstSuffixIndex, unsigned int sndSuffixIndex) {
                return firstSuffixGreaterOrEqualToPatternUsingPrefixesCache(patternLength, this->prefixes[fstSuffixIndex], this->karySA + fstSuffixIndex, this->prefixes[sndSuffixIndex], this->karySA + sndSuffixIndex);
            }

            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(int patternLength, unsigned char* fstPrefixPtr, unsigned int *fstSufOffset) {
                v2di cmpVector = __builtin_ia32_pcmpgtq(patternVector, *((v2di*) fstPrefixPtr));
                int suffixesLowerThenPatternMM = __builtin_ia32_movmskpd((v2df) cmpVector);
                if (suffixesLowerThenPatternMM == 3)
                    return 2;
                cmpVector = __builtin_ia32_pcmpgtq (*((v2di*) fstPrefixPtr), patternVector);
                int suffixesGreaterThenPatternMM = __builtin_ia32_movmskpd((v2df) cmpVector);
                if (suffixesGreaterThenPatternMM == 3)
                    return 0;
                if (suffixesGreaterThenPatternMM == 2) {
                    if (suffixesLowerThenPatternMM == 1)
                        return 1;
                    return 1 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *fstSufOffset + 8, patternLength - this->lcp - 8 + 1) < 0);
                }
                if (suffixesLowerThenPatternMM == 1)
                    return 2 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *(fstSufOffset + 1) + 8, patternLength - this->lcp - 8 + 1) < 0);
                if (this->strcmpStrategy(this->ptrn + 8, this->stxt + *fstSufOffset + 8, patternLength - this->lcp - 8 + 1) < 0)
                    return 0;
                return 2 - (this->strcmpStrategy(this->ptrn + 8, this->stxt + *(fstSufOffset + 1) + 8, patternLength - this->lcp - 8 + 1) < 0);
            }    
            
            inline int firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(int patternLength, unsigned int fstSuffixIndex) {
                return firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(patternLength, this->prefixes[fstSuffixIndex], this->karySA + fstSuffixIndex);
            }
            
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<9>(node) + startElement;
// WITHSSE               
                int c, res = 2;
                for(c = startElement; c < ELEMENTSINNODE; c += 2) {
                    if ((res = this->firstSuffixGreaterOrEqualToPatternUsingPrefixesCacheAndSSE(patternLength, j)) < 2) {
                        c += res;
                        j += res;
                        i = j;
                        break;
                    }
                    j += 2;
                }
                
                this->lcpUpdate(c, this->prefixes[j - 1], this->prefixes[j]);
                return c;
            }

            inline int searchNodeImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<9>(node) + startElement + (step - 1);
                int c;
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                return c;
            }
               
        };
    }
}

#endif	/* KARYSSELCPSASEARCHSTRATEGIES_H */

