/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SAHelpers.h
 * Author: coach
 *
 * Created on 15 lutego 2016, 21:20
 */

#ifndef SAHELPERS_H
#define SAHELPERS_H

#define POW(B, E) Pow<B, E>::result
#define MLARYNESS POW(k, localLevelsCount)

#define ALIGN_MEMORY
#define ALIGN_MEM2N 12

#include <cstdlib>
#include <string>
#include <sstream>

// Generic exponent compile-time calculations. Pow<2,3>::result == 2^3
template <unsigned long B, unsigned long E>
struct Pow
{
    static const unsigned long result = B * Pow<B, E-1>::result;
};

template <unsigned long B>
struct Pow<B, 0>
{
    static const unsigned long result = 1;
};

using namespace std;
/*
void *malloc128(size_t s) {
    unsigned char *p;
    unsigned char *porig = (unsigned char*) malloc (s + 128);
    if (porig == NULL) return NULL;             
    p = porig + (128 - ((size_t) porig % 128));
    *(p-1) = p - porig;                         
    return p;
}

void free128(void *p) {
    if (p == NULL)
        return;
    
    unsigned char *porig = (unsigned char*) p;
    porig = porig - *(porig-1);
    free (porig);                  
}
*/
string toString(unsigned long long value);
string toStringDouble(double value);

void *malloc2n(size_t s);

void free2n(void *p);

bool incrementPattern(unsigned char* pattern, int patternLength);

void decrementPattern(unsigned char* pattern, int patternLength);

 
unsigned long djb2(unsigned char *str);

#endif /* SAHELPERS_H */

