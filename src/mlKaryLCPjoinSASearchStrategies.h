/* 
 * File:   mlKaryLCPjoinSASearchStrategies.h
 * Author: coach
 *
 * Created on 19 czerwca 2015, 16:45
 */

#ifndef MLKARYLCPJOINSASEARCHSTRATEGIES_H
#define	MLKARYLCPJOINSASEARCHSTRATEGIES_H

#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"
#include "mlKaryLCPSASearchStrategies.h"

using namespace std;

namespace SASearch {

    namespace kary {    
        
        template<int localLevelsCount, int k, int step, int lcpMLLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK = 0>
        class MLKaryLCPjoinSASearch: 
                public MLKaryLCPSASearchTemplate<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, MLKaryLCPjoinSASearch<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK>> {
        private:
            typedef MLKaryLCPjoinSASearch<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK> ThisType;

        protected:
            unsigned int* mlJoinSA;
            
            unsigned int firstNoPrefixNode;
            unsigned int firstNoPrefixIndex;
            unsigned int* firstNoPrefixElem;
            
            inline unsigned int* firstInNodePrefixElemIndex2Ptr(unsigned int index) {
                return mlJoinSA + index * (1 + prefixLengthInUints);
            }
            
            inline unsigned int* noPrefixElemIndex2Ptr(unsigned int index) {
                return firstNoPrefixElem + (index - firstNoPrefixIndex);
            }

            inline unsigned int* firstInNodePrefixElemPtr2firstPrefix(unsigned int* ptr) {
                return ptr + (k - 1);
            }

        public:
            MLKaryLCPjoinSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
                    : MLKaryLCPSASearchTemplate<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "LCPjoinSA"),
                    mlJoinSA(joinKaryWithPrefixes<k, prefixLengthInUints>(this->mlKarySA, this->mlPrefixes, txtSize, localLevelsCount * lcpMLLevels)),
                    firstNoPrefixNode((localLevelsCount * lcpMLLevels < this->levelsCount)?karyNodes(k, localLevelsCount * lcpMLLevels):(this->lastNode + 1)),
                    firstNoPrefixIndex(node2index<k>(firstNoPrefixNode)),
                    firstNoPrefixElem(firstInNodePrefixElemIndex2Ptr(firstNoPrefixIndex))
            {
            }

            ~MLKaryLCPjoinSASearch() {
                #ifdef ALIGN_MEMORY
                free2n(mlJoinSA);
                #else
                delete[] mlJoinSA;
                #endif
            }
            
            inline bool findPlusKOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, int counter) {
                unsigned int* ptr;
                if (this->firstNoPrefixIndex < i)
                    ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                else {
                    int c = numOfElementInNode<k>(i);
                    ptr = this->firstInNodePrefixElemIndex2Ptr(i - c) + c;
                }

                if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                    return true;
                
                int mlLevel;
                unsigned int mlNodeIndex;
                unsigned int mlNodeElements;
                unsigned int mlNodeInMLLevel;
                unsigned int mlNode;
                
                this->indexMLCoordinates(i, mlLevel, mlNodeIndex, mlNodeElements, mlNodeInMLLevel);
                if (mlLevel == this->mlLevelsCount - 1)     
                    mlNode = this->nonLeafMLNodes + mlNodeInMLLevel;
                else
                    mlNode = index2node<MLARYNESS>(mlNodeIndex);
                
                while (counter--) {
                    if (!this->incrementMLKaryIndex(i, mlLevel, mlNode, mlNodeIndex, mlNodeElements))
                        return true;

                    if (this->firstNoPrefixIndex < i)
                        ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                    else {
                        int c = numOfElementInNode<k>(i);
                        ptr = this->firstInNodePrefixElemIndex2Ptr(i - c) + c;
                    }
                        
                    if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                        return true;
                }
                
                return false;
            }   
            
            virtual unsigned int getSuffixOffset(unsigned int index) { 
                unsigned int nodeStartIndex = index - index % (k - 1);
                return *(((firstNoPrefixIndex < nodeStartIndex)?noPrefixElemIndex2Ptr(nodeStartIndex):firstInNodePrefixElemIndex2Ptr(nodeStartIndex)) + index % (k - 1));
            };
            
            unsigned int* getMLKaryLCPjoinSA() { return this->mlJoinSA; }
            
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + firstElementIndex;
                int c;
                   
                unsigned int* ptr = this->firstInNodePrefixElemIndex2Ptr(j);
                unsigned int* prefixPtr = this->firstInNodePrefixElemPtr2firstPrefix(ptr) + (startElement + step - 1) * prefixLengthInUints;
                unsigned int* sufPtr = (unsigned int*)(ptr) + startElement + step - 1;

                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, (unsigned char*) prefixPtr, sufPtr)) {
                        i = j + c;
                        break;
                    }
                    sufPtr += step;
                    prefixPtr += step * prefixLengthInUints;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    sufPtr -= step - 1; 
                    prefixPtr -= (step - 1) * prefixLengthInUints;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, (unsigned char*) prefixPtr, sufPtr)) {
                            i = j + c;
                            break;
                        }
                        sufPtr++;
                        prefixPtr += prefixLengthInUints;
                    }
                }
                 
                this->lcpUpdate(c, (unsigned char*) (prefixPtr - prefixLengthInUints), (unsigned char*) prefixPtr);
                        
                return c;
            }
            
            inline int searchNodeImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + firstElementIndex;
                int c;
                   
                unsigned int* sufPtr = this->noPrefixElemIndex2Ptr(j) + startElement + step - 1;
                    
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(patternLength, sufPtr)) {
                        i = j + c;
                        break;
                    }
                    sufPtr += step;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    sufPtr -= step - 1; 
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(patternLength, sufPtr)) {
                            i = j + c;
                            break;
                        }
                        sufPtr++;
                    }
                }
                         
                return c;
            }
          
        };
    }
}

#endif	/* MLKARYLCPJOINSASEARCHSTRATEGIES_H */
