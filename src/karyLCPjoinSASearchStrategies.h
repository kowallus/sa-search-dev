/* 
 * File:   karyLCPjoinSASearchStrategies.h
 * Author: coach
 *
 * Created on 9 czerwca 2015, 12:06
 */

#ifndef KARYLCPJOINSASEARCHSTRATEGIES_H
#define	KARYLCPJOINSASEARCHSTRATEGIES_H

#include "karyLCPSASearchStrategies.h"

using namespace std;

namespace SASearch {

    namespace kary {    
 
        template<int k, int step, int lcpLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK = 0>
        class KaryLCPjoinSASearch: public KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, KaryLCPjoinSASearch<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK>> {
        private:
            typedef KaryLCPjoinSASearch<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK> ThisType;
            typedef StringComparisonStrategyInterface<StrNCmp32bitsComparisonStrategyClass> StrNCmpComparisionStrategy;
        
        protected:
            
            unsigned int* joinSA;
            
            unsigned int firstNoPrefixNode;
            unsigned int firstNoPrefixIndex;
            unsigned int* firstNoPrefixElem;
            
            inline unsigned int* firstInNodePrefixElemIndex2Ptr(unsigned int index) {
                return joinSA + index * (1 + prefixLengthInUints);
            }
            
            inline unsigned int* noPrefixElemIndex2Ptr(unsigned int index) {
                return firstNoPrefixElem + (index - firstNoPrefixIndex);
            }

            inline unsigned int* firstInNodePrefixElemPtr2firstPrefix(unsigned int* ptr) {
                return ptr + (k - 1);
            }
            
        public:
            KaryLCPjoinSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "LCPjoinSA"),
                    joinSA(joinKaryWithPrefixes<k, prefixLengthInUints>(this->karySA, this->prefixes, txtSize, lcpLevels)),
                    firstNoPrefixNode((lcpLevels < this->levelsCount)?karyNodes(k, lcpLevels):(this->lastNode + 1)),
                    firstNoPrefixIndex(node2index<k>(firstNoPrefixNode)),
                    firstNoPrefixElem(firstInNodePrefixElemIndex2Ptr(firstNoPrefixIndex))
            {
            }

            KaryLCPjoinSASearch(unsigned char* txt)
            : KaryLCPSASearchTemplate<k, step, lcpLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(txt, "LCPjoinSA"),
                    firstNoPrefixNode(karyNodes(k, lcpLevels)),
                    firstNoPrefixIndex(node2index<k>(firstNoPrefixNode))
            {
            }

            inline void setKaryConfiguration(unsigned int* karyLCPSA, unsigned int size) {
                this->txtSize = size - this->prefixesSizeInUints;
                this->joinSA = karyLCPSA;
                this->firstNoPrefixElem = firstInNodePrefixElemIndex2Ptr(firstNoPrefixIndex);
                this->lastNode = (this->txtSize - 1) / (k-1);
            }
            
            inline void index2suffixIndex(unsigned int &index) {
                if (index >= firstNoPrefixIndex)
                    index += this->prefixesSizeInUints;
                else 
                    index += (index / (k - 1)) * (k - 1) * prefixLengthInUints;
            }
            
            inline void suffixIndex2index(unsigned int &suffixIndex) {
                if (suffixIndex > firstNoPrefixIndex + this->prefixesSizeInUints)
                    suffixIndex -= this->prefixesSizeInUints;
                else 
                    suffixIndex -= (suffixIndex / ((k - 1) * (1 + prefixLengthInUints))) * (k - 1) * prefixLengthInUints;
            }
            
            void indexRankingCorrection() {
                unsigned int* suffixPtr = noPrefixElemIndex2Ptr(this->txtSize - 1);
                while (*suffixPtr == *(suffixPtr - 1)) {
                    this->txtSize--;
                    suffixPtr--;
                }
            }
            
            void nullKaryConfiguration() {
                this->joinSA = NULL;
            }
            
            unsigned int* createLCPSAkary(unsigned int *sa, unsigned int saSize, unsigned int* dest) { 
                unsigned int* karySA = createSAkarySimple<k>(sa, saSize);
                int size = (((saSize - 1) / (k - 1)) + 1) * (k - 1);
                prefixarray<prefixLengthInUints>* prefixes = createPrefixesCache<k, prefixLengthInUints>(karySA, this->txt, saSize, lcpLevels);

                unsigned int* joinSA = joinKaryWithPrefixes<k, prefixLengthInUints>(karySA, prefixes, saSize, lcpLevels);
                
                std::copy(joinSA, joinSA + size + this->prefixesSizeInUints, dest);

                #ifdef ALIGN_MEMORY
                free2n(karySA);
                free2n(prefixes);
                free2n(joinSA);
                #else
                delete[] karySA;
                delete[] prefixes;
                delete[] joinSA;
                #endif
            }
            
            ~KaryLCPjoinSASearch() {
                #ifdef ALIGN_MEMORY
                free2n(joinSA);
                #else
                delete[] joinSA;
                #endif
            } 
            
            inline bool findPlusKOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, int counter) {
                unsigned int* ptr;
                if (this->firstNoPrefixIndex < i)
                    ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                else {
                    int c = numOfElementInNode<k>(i);
                    ptr = this->firstInNodePrefixElemIndex2Ptr(i - c) + c;
                }

                if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                    return true;
                
                while (counter--) {
                    if (!this->incrementKaryIndex(i))
                        return true;

                    if (this->firstNoPrefixIndex < i)
                        ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                    else {
                        int c = numOfElementInNode<k>(i);
                        ptr = this->firstInNodePrefixElemIndex2Ptr(i - c) + c;
                    }
                        
                    if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                        return true;
                }
                    
                return false;
            }
            
            virtual unsigned int getSuffixOffset(unsigned int index) { 
                unsigned int nodeStartIndex = index - index % (k - 1);
                return *(((firstNoPrefixIndex < nodeStartIndex)?noPrefixElemIndex2Ptr(nodeStartIndex):firstInNodePrefixElemIndex2Ptr(nodeStartIndex)) + index % (k - 1));
            };
            
            unsigned int* getKaryLCPjoinSA() { return this->joinSA; }
             
            void copyLocationsImpl(unsigned int beg, unsigned int end, vector<unsigned int>& res) {
                res.reserve(res.size() + end - beg);
                if (beg < this->firstNoPrefixIndex) {
                    unsigned int* ptr = joinSA + beg * (1 + prefixLengthInUints) - (beg % (k - 1)) * prefixLengthInUints;
                    unsigned int* endPtr = joinSA + end * (1 + prefixLengthInUints) - (end % (k - 1)) * prefixLengthInUints;
                    unsigned int* rPtr = ptr + (k - 1) - (beg % (k - 1));
                    while (rPtr < endPtr && ptr != this->firstNoPrefixElem) {
                        res.insert(res.end(), ptr, rPtr);
                        beg += k - 1;
                        ptr = rPtr + (k - 1) * prefixLengthInUints;
                        rPtr = ptr + (k - 1);
                    }
                    if (endPtr < rPtr) {
                        res.insert(res.end(), ptr, endPtr);
                        return;
                    }
                }

                if (beg < end)
                    res.insert(res.end(), this->noPrefixElemIndex2Ptr(beg), this->noPrefixElemIndex2Ptr(end));
            }
            
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node);
                int c;
                unsigned int* ptr = this->firstInNodePrefixElemIndex2Ptr(j);
                unsigned int* prefixPtr = this->firstInNodePrefixElemPtr2firstPrefix(ptr) + (startElement + step - 1) * prefixLengthInUints;
                unsigned int* sufPtr = ptr + startElement + step - 1;

                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, (unsigned char*) prefixPtr, sufPtr)) {
                        i = j + c;
                        break;
                    }
                    sufPtr += step;
                    prefixPtr += step * prefixLengthInUints;
                }
                
                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    sufPtr -= step - 1; 
                    prefixPtr -= (step - 1) * prefixLengthInUints;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, (unsigned char*) prefixPtr, sufPtr)) {
                            i = j + c;
                            break;
                        }
                        sufPtr++;
                        prefixPtr += prefixLengthInUints;
                    }
                }
                    
                this->lcpUpdate(c, (unsigned char*) (prefixPtr - prefixLengthInUints), (unsigned char*) prefixPtr);
                return c;
            };
                
            inline int searchNodeImpl(int patternLength, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node);
                int c;
                unsigned int* sufPtr ;
                if (node <= this->lastNodeWithPrefixes)
                    sufPtr = this->firstInNodePrefixElemIndex2Ptr(j) + startElement + step - 1;
                else
                    sufPtr = this->noPrefixElemIndex2Ptr(j) + startElement + step - 1;
                    
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(patternLength, sufPtr)) {
                        i = j + c;
                        break;
                    }
                    sufPtr += step;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    sufPtr -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(patternLength, sufPtr)) {
                            i = j + c;
                            break;
                        }
                        sufPtr++;
                    }
                }
                
                return c;
            }
        };
    }
}

#endif	/* KARYLCPJOINSASEARCHSTRATEGIES_H */

