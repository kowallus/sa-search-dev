#include "karySASearchStrategies.h"


namespace SASearch {

    namespace kary {    
        
        string stepString(int step) {
            return (step > 1?(toString(step) + "-step "):"");
        }
    }
    
}