/* 
 * File:   testKaryLUTSASuite.h
 * Author: coach
 *
 * Created on 7 grudnia 2015, 09:55
 */

#ifndef TESTKARYLUTSASUITE_H
#define	TESTKARYLUTSASUITE_H

#include "testingRoutines.h"

template<class PreSearchStrategy, class StrCmpStrategy, int PlusK>
void karyLUTSAVariantsTestsSet(PreSearchStrategy* preSearch);

template<class PreSearchStrategy, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsTestsSet(unsigned int maxAdditionalSizeInBytes);

template<class PreSearchStrategy, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsMinimalTestsSet(unsigned int maxAdditionalSizeInBytes);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsMinimalTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedTestsSet(unsigned int maxAdditionalSizeInBytes);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedLargeKTestsSet(unsigned int maxAdditionalSizeInBytes);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedLargeKTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsBestTestsSet(unsigned int maxAdditionalSizeInBytes);

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsBestTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor);

template<class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsFinalTestsSet(unsigned int maxAdditionalSizeInBytes);

template<class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsFinalTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor);

template<int k, int step, class HuffCoderClass, class StrCmpStrategy>
void karyLUTHuffVariantsTestsSet(bool extended) {
    HuffCoderClass* coder = new HuffCoderClass((const char*) txt, txtSize);
    SAHuffLookupWithSATwist<HuffCoderClass, 15>* karyLUThuffTwisted15 = new SAHuffLookupWithSATwist<HuffCoderClass, 15>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 15>, KaryLUTWithSATwistSearch<k, step, AsmLibStrCmp, SAHuffLookupWithSATwist<HuffCoderClass, 15>, 0>>(karyLUThuffTwisted15);
    SAHuffLookupWithSATwist<HuffCoderClass, 19>* karyLUThuffTwisted19 = new SAHuffLookupWithSATwist<HuffCoderClass, 19>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 19>, KaryLUTWithSATwistSearch<k, step, AsmLibStrCmp, SAHuffLookupWithSATwist<HuffCoderClass, 19>, 0>>(karyLUThuffTwisted19);
    SAHuffLookupWithSATwist<HuffCoderClass, 23>* karyLUThuffTwisted23 = new SAHuffLookupWithSATwist<HuffCoderClass, 23>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 23>, KaryLUTWithSATwistSearch<k, step, AsmLibStrCmp, SAHuffLookupWithSATwist<HuffCoderClass, 23>, 0>>(karyLUThuffTwisted23);
    if (extended) {
        SAHuffLookupWithSATwist<HuffCoderClass, 25>* karyLUThuffTwisted25 = new SAHuffLookupWithSATwist<HuffCoderClass, 25>(saArray, txt, txtSize, coder);
        testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 25>, KaryLUTWithSATwistSearch<k, step, AsmLibStrCmp, SAHuffLookupWithSATwist<HuffCoderClass, 25>, 0>>(karyLUThuffTwisted25);
        SAHuffLookupWithSATwist<HuffCoderClass, 26>* karyLUThuffTwisted26 = new SAHuffLookupWithSATwist<HuffCoderClass, 26>(saArray, txt, txtSize, coder);
        testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 26>, KaryLUTWithSATwistSearch<k, step, AsmLibStrCmp, SAHuffLookupWithSATwist<HuffCoderClass, 26>, 0>>(karyLUThuffTwisted26);
        SAHuffLookupWithSATwist<HuffCoderClass, 27>* karyLUThuffTwisted27 = new SAHuffLookupWithSATwist<HuffCoderClass, 27>(saArray, txt, txtSize, coder);
        testSATwoStageKaryLUTSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 27>, KaryLUTWithSATwistSearch<k, step, AsmLibStrCmp, SAHuffLookupWithSATwist<HuffCoderClass, 27>, 0>>(karyLUThuffTwisted27);
        delete karyLUThuffTwisted25; 
        delete karyLUThuffTwisted26;
        delete karyLUThuffTwisted27;
    }
    delete karyLUThuffTwisted15;
    delete karyLUThuffTwisted19;
    delete karyLUThuffTwisted23;
    delete coder;
}

template<class HuffCoderClass, class StrCmpStrategy>
void karyLUTHuffSelectedVariantsTestsSet(bool extended) {
    karyLUTHuffVariantsTestsSet<2, 1, HuffCoderClass, StrCmpStrategy>(extended);
    karyLUTHuffVariantsTestsSet<3, 1, HuffCoderClass, StrCmpStrategy>(extended);
    karyLUTHuffVariantsTestsSet<5, 1, HuffCoderClass, StrCmpStrategy>(extended);
    karyLUTHuffVariantsTestsSet<17, 3, HuffCoderClass, StrCmpStrategy>(extended);
    karyLUTHuffVariantsTestsSet<33, 5, HuffCoderClass, StrCmpStrategy>(extended);
}

template<class StrCmpStrategy, int PlusK>
void karyLUTSAVariantsPlannedTestsSet(int hash_k) {
    
    karyLUTSAVariantsTestsSet<SALookupOn2Symbols, StrCmpStrategy, PlusK>(saLUT2);
    karyLUTSAVariantsTestsSet<SALookupOn3Symbols, StrCmpStrategy, PlusK>(saLUT3);
    
    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash50p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, hash_k<patternLength?hash_k:patternLength-1, 50);
    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash90p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, hash_k<patternLength?hash_k:patternLength-1, 90);
    
    karyLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, PlusK>(saHash50p);
    karyLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, PlusK>(saHash90p);
}

template<int k, int step, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsDiffPrefixPlannedTestsSet() {
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 1, 0>(10800000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(10800000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 3, 0>(10800000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 4, 0>(10800000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 5, 0>(10800000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 6, 0>(10800000);
    
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 1, 0>(47500000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(47500000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 3, 0>(47500000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 4, 0>(47500000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 5, 0>(47500000);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 6, 0>(47500000);
    
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 1, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 3, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 4, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 5, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 6, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 1, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 3, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 4, 0>(47500000, 8<patternLength?8:patternLength-1, 90);    
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 5, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsMinimalTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 6, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
}

template<int k, int step, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsDiffKPlannedTestsSet() {
    karyLCPLUTSAVariantsExtendedTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(10800000);    
    karyLCPLUTSAVariantsExtendedTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(47500000);
    karyLCPLUTSAVariantsExtendedTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsExtendedTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
}

template<int k, int step, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsDiffKLargeKPlannedTestsSet() {
    karyLCPLUTSAVariantsExtendedLargeKTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(10800000);    
    karyLCPLUTSAVariantsExtendedLargeKTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(47500000);
    karyLCPLUTSAVariantsExtendedLargeKTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsExtendedLargeKTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
}

template<int k, int step, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsBestPlannedTestsSet() {
    karyLCPLUTSAVariantsBestTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(10800000);
    karyLCPLUTSAVariantsBestTestsSet<k, step, SALookupOn3Symbols, StrCmpStrategy, 2, 0>(47500000);
    karyLCPLUTSAVariantsBestTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(10800000, 8<patternLength?8:patternLength-1, 50);
    karyLCPLUTSAVariantsBestTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(47500000, 8<patternLength?8:patternLength-1, 50);
    karyLCPLUTSAVariantsBestTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(10800000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsBestTestsSet<k, step, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(47500000, 8<patternLength?8:patternLength-1, 90);
}

template<class StrCmpStrategy>
void karyLCPLUTSAVariantsFinalTestsSet(int hash_k) {
/*    karyLCPLUTSAVariantsFinalTestsSet<SALookupOn3Symbols, StrCmpStrategy, 2, 0>(10800000);    
    karyLCPLUTSAVariantsFinalTestsSet<SALookupOn3Symbols, StrCmpStrategy, 2, 0>(22600000);
    karyLCPLUTSAVariantsFinalTestsSet<SALookupOn3Symbols, StrCmpStrategy, 2, 0>(47500000);
    karyLCPLUTSAVariantsFinalTestsSet<SALookupOn3Symbols, StrCmpStrategy, 2, 0>(99900000);
    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(10800000, hash_k<patternLength?hash_k:patternLength-1, 50);*/
    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(22600000, hash_k<patternLength?hash_k:patternLength-1, 50);
//    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(47500000, hash_k<patternLength?hash_k:patternLength-1, 50);
    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(99900000, hash_k<patternLength?hash_k:patternLength-1, 50);
    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(10800000, hash_k<patternLength?hash_k:patternLength-1, 90);
    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(22600000, hash_k<patternLength?hash_k:patternLength-1, 90);
    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(47500000, hash_k<patternLength?hash_k:patternLength-1, 90);
    karyLCPLUTSAVariantsFinalTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 2, 0>(99900000, hash_k<patternLength?hash_k:patternLength-1, 90);
}

template<class HuffCoderClass, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSHuffVariantsFinalTestsSet(unsigned int maxAdditionalSizeInBytes, bool extended);

template<class HuffCoderClass, int k, int step, class StrCmpStrategy, class KaryLCPSAStrategyClass, int PlusK>
void karyLCPLUTSHuffVariantsTestsSet(unsigned int maxAdditionalSizeInBytes, bool extended) {
    HuffCoderClass* coder = new HuffCoderClass((const char*) txt, txtSize);
    SAHuffLookupWithSATwist<HuffCoderClass, 15>* karyLUThuffTwisted15 = new SAHuffLookupWithSATwist<HuffCoderClass, 15>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTandLCPSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 15>, KaryLUTWithSATwistLCPSASearch<k, step, StrCmpStrategy, SAHuffLookupWithSATwist<HuffCoderClass, 15>, KaryLCPSAStrategyClass, PlusK>>(maxAdditionalSizeInBytes, karyLUThuffTwisted15);
    SAHuffLookupWithSATwist<HuffCoderClass, 19>* karyLUThuffTwisted19 = new SAHuffLookupWithSATwist<HuffCoderClass, 19>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTandLCPSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 19>, KaryLUTWithSATwistLCPSASearch<k, step, StrCmpStrategy, SAHuffLookupWithSATwist<HuffCoderClass, 19>, KaryLCPSAStrategyClass, PlusK>>(maxAdditionalSizeInBytes, karyLUThuffTwisted19);
    SAHuffLookupWithSATwist<HuffCoderClass, 23>* karyLUThuffTwisted23 = new SAHuffLookupWithSATwist<HuffCoderClass, 23>(saArray, txt, txtSize, coder);
    testSATwoStageKaryLUTandLCPSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 23>, KaryLUTWithSATwistLCPSASearch<k, step, StrCmpStrategy, SAHuffLookupWithSATwist<HuffCoderClass, 23>, KaryLCPSAStrategyClass, PlusK>>(maxAdditionalSizeInBytes, karyLUThuffTwisted23);
    if (extended) {
        SAHuffLookupWithSATwist<HuffCoderClass, 25>* karyLUThuffTwisted25 = new SAHuffLookupWithSATwist<HuffCoderClass, 25>(saArray, txt, txtSize, coder);
        testSATwoStageKaryLUTandLCPSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 25>, KaryLUTWithSATwistLCPSASearch<k, step, StrCmpStrategy, SAHuffLookupWithSATwist<HuffCoderClass, 25>, KaryLCPSAStrategyClass, PlusK>>(maxAdditionalSizeInBytes, karyLUThuffTwisted25);
        SAHuffLookupWithSATwist<HuffCoderClass, 26>* karyLUThuffTwisted26 = new SAHuffLookupWithSATwist<HuffCoderClass, 26>(saArray, txt, txtSize, coder);
        testSATwoStageKaryLUTandLCPSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 26>, KaryLUTWithSATwistLCPSASearch<k, step, StrCmpStrategy, SAHuffLookupWithSATwist<HuffCoderClass, 26>, KaryLCPSAStrategyClass, PlusK>>(maxAdditionalSizeInBytes, karyLUThuffTwisted26);
        SAHuffLookupWithSATwist<HuffCoderClass, 27>* karyLUThuffTwisted27 = new SAHuffLookupWithSATwist<HuffCoderClass, 27>(saArray, txt, txtSize, coder);
        testSATwoStageKaryLUTandLCPSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 27>, KaryLUTWithSATwistLCPSASearch<k, step, StrCmpStrategy, SAHuffLookupWithSATwist<HuffCoderClass, 27>, KaryLCPSAStrategyClass, PlusK>>(maxAdditionalSizeInBytes, karyLUThuffTwisted27);
        delete karyLUThuffTwisted25; 
        delete karyLUThuffTwisted26; 
        delete karyLUThuffTwisted27;
    }
    delete karyLUThuffTwisted15;
    delete karyLUThuffTwisted19;
    delete karyLUThuffTwisted23;
    delete coder;
}

template<class StrCmpStrategy, class HuffCoderClass>
void karyLCPLUTSHuffVariantsFinalTestsSet(bool extended) {
    karyLCPLUTSHuffVariantsFinalTestsSet<HuffCoderClass, StrCmpStrategy, 2, 0>(10800000, extended);
    karyLCPLUTSHuffVariantsFinalTestsSet<HuffCoderClass, StrCmpStrategy, 2, 0>(22600000, extended);
    karyLCPLUTSHuffVariantsFinalTestsSet<HuffCoderClass, StrCmpStrategy, 2, 0>(47500000, extended);
    karyLCPLUTSHuffVariantsFinalTestsSet<HuffCoderClass, StrCmpStrategy, 2, 0>(99900000, extended);
}


template<class StrCmpStrategy>
void preliminaryKaryLUTSAVariantsTestsSet() {

    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash50p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, 8<patternLength?8:patternLength-1, 50); 
    SAHashLookup<StandardCStrNCmp, SALookupOn2Symbols>* saHash90 = new SAHashLookup<StandardCStrNCmp, SALookupOn2Symbols>(saArray, txt, txtSize, 8<patternLength?8:patternLength-1, 90);
    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash90p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, 8<patternLength?8:patternLength-1, 90); 
    

    karyLUTSAVariantsTestsSet<SALookupOn2Symbols, StrCmpStrategy, 0>(saLUT2);
    karyLUTSAVariantsTestsSet<SALookupOn3Symbols, StrCmpStrategy, 0>(saLUT3);
    karyLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(saHash50p);
    karyLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(saHash90p);

    karyLCPSAVariantsPlannedTestsSet<StrCmpStrategy, 0>();
    karyLCPLUTSAVariantsTestsSet<SALookupOn3Symbols, StrCmpStrategy, 0>(6000000);
    karyLCPLUTSAVariantsTestsSet<SALookupOn3Symbols, StrCmpStrategy, 0>(21000000);
    karyLCPLUTSAVariantsTestsSet<SALookupOn3Symbols, StrCmpStrategy, 0>(61000000);
    karyLCPLUTSAVariantsTestsSet<SALookupOn3Symbols, StrCmpStrategy, 0>(117000000);

    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(6000000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(21000000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(61000000, 8<patternLength?8:patternLength-1, 90);
    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(117000000, 8<patternLength?8:patternLength-1, 90);

    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(6000000, 8<patternLength?8:patternLength-1, 50);
    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(21000000, 8<patternLength?8:patternLength-1, 50);
    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(61000000, 8<patternLength?8:patternLength-1, 50);
    karyLCPLUTSAVariantsTestsSet<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, StrCmpStrategy, 0>(117000000, 8<patternLength?8:patternLength-1, 50);

    karyLUTSAVariantsTestsSet<SAHashLookup<StandardCStrNCmp, SALookupOn2Symbols>, StrCmpStrategy, 0>(saHash90);
}

template<class StrCmpStrategy>
void variousKaryLUTSAVariantsTestsSet() {
    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash50p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, 8<patternLength?8:patternLength-1, 50); 
    
    testSATwoStageSearchStrategy<SALookupOn3Symbols, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saLUT3);
    testSATwoStageKaryLUTSearchStrategy<SALookupOn3Symbols, KaryLUTSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols>>(saLUT3);

    testSATwoStageSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saHash50p);
    testSATwoStageKaryLUTSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>>>(saHash50p);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 8, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 7, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 6, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 5, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 4, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000, 8<patternLength?8:patternLength-1, 50);

    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 8, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 7, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 6, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 5, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000, 8<patternLength?8:patternLength-1, 50);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 4, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000, 8<patternLength?8:patternLength-1, 50);

    testSATwoStageSearchStrategy<SALookupOn3Symbols, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(saLUT3);

    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 11, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 10, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 9, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 8, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 7, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 6, 2, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 11, 1, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 10, 1, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 9, 1, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 8, 1, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 7, 1, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 6, 1, BSwap32StrNECmp, StrCmpStrategy>>>(20000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 11, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 10, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 9, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 8, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 7, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000);
    testSATwoStageKaryLUTandLCPSearchStrategy<SALookupOn3Symbols, KaryLUTLCPSASearch<4, 1, StrCmpStrategy, SALookupOn3Symbols, KaryLCPSASearch<3, 1, 6, 2, BSwap32StrNECmp, StrCmpStrategy>>>(50000000);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<3, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 5, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(6000000, 8<patternLength?8:patternLength-1, 90);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<5, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 5, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(6000000, 8<patternLength?8:patternLength-1, 90);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<3, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 5, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(61000000, 8<patternLength?8:patternLength-1, 90);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<5, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<3, 1, 5, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(61000000, 8<patternLength?8:patternLength-1, 90);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<3, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<5, 1, 4, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(6000000, 8<patternLength?8:patternLength-1, 90);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<5, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<5, 1, 4, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(6000000, 8<patternLength?8:patternLength-1, 90);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<3, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<5, 1, 4, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(61000000, 8<patternLength?8:patternLength-1, 90);
    testSATwoStageKaryLUTandLCPSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLUTLCPSASearch<5, 1, AsmLibStrCmp, SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, KaryLCPSASearch<5, 1, 4, 2, BSwap32StrNECmp, AsmLibStrCmp, 0>, 0>>(61000000, 8<patternLength?8:patternLength-1, 90);
}

template<class PreSearchStrategy, class StrCmpStrategy, int PlusK>
void karyLUTSAVariantsTestsSet(PreSearchStrategy* preSearch) {
    cout << endl << "K-ary LUTSA variants " << endl;

    testSATwoStageSearchStrategy<PreSearchStrategy, SASimpleBinarySearch<AsmLibStrCmp, PLUSK_DOUBLING>>(preSearch);
    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<3, 1, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
//    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<9, 2, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<17, 3, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
//    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<25, 4, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<33, 5, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
//    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<41, 6, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
//    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<49, 7, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);

    // CONTROL - WEAKER VARIANTS
    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<2, 1, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
//    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<57, 8, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
//    testSATwoStageKaryLUTSearchStrategy<PreSearchStrategy, KaryLUTSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, PlusK>>(preSearch);
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsTestsSet(unsigned int maxAdditionalSizeInBytes) {
    cout << endl << "K-ary LCPLUTSA variants " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 8, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 7, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 6, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 5, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 3, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 2, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 1, 3, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 1, 2, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 1, 1, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
}

template<class PreSearchStrategy, class StrCmpStrategy, int PlusK>
void karyLCPLUTSAVariantsTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor) {
    cout << endl << "K-ary LCPLUTSA variants (hash) " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 7, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 6, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 3, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 5, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 3, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 2, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 1, 3, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 1, 2, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 1, 1, 2, StrCmpStrategy, AsmLibStrCmp, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);    

}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsMinimalTestsSet(unsigned int maxAdditionalSizeInBytes) {
    cout << endl << "K-ary LCPLUTSA variants " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 6, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsMinimalTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor) {
    cout << endl << "K-ary LCPLUTSA variants (hash) " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 6, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedTestsSet(unsigned int maxAdditionalSizeInBytes) {
    cout << endl << "K-ary LCPLUTSA variants " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 8, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 7, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 6, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 6, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
/*    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<17, 3, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<17, 3, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<17, 3, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
*/    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 1, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor) {
    cout << endl << "K-ary LCPLUTSA variants (hash) " << endl;
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 7, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 6, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 6, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<9, 2, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<17, 3, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<17, 3, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<17, 3, 1, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 1, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedLargeKTestsSet(unsigned int maxAdditionalSizeInBytes) {
    cout << endl << "K-ary LCPLUTSA variants " << endl;
  
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
/*    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<65, 9, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<65, 9, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<65, 9, 1, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);*/
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsExtendedLargeKTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor) {
    cout << endl << "K-ary LCPLUTSA variants (hash) " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 1, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<65, 9, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<65, 9, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<65, 9, 1, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsBestTestsSet(unsigned int maxAdditionalSizeInBytes) {
    cout << endl << "K-ary LCPLUTSA variants " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);   
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
}

template<int k, int step, class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsBestTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor) {
    cout << endl << "K-ary LCPLUTSA variants (hash) " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<k, step, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
}

template<class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsFinalTestsSet(unsigned int maxAdditionalSizeInBytes) {
    cout << endl << "K-ary LCPLUTSA final variants " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<33, 5, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);   
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<33, 5, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);   
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes);
}

template<class PreSearchStrategy, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSAVariantsFinalTestsSet(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor) {
    cout << endl << "K-ary LCPLUTSA final variants (hash) " << endl;

    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<5, 1, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);

//    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<33, 5, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
//    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<33, 5, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
//    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
//    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<5, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);
    testSATwoStageKaryLUTandLCPSearchStrategy<PreSearchStrategy, KaryLUTLCPSASearch<65, 9, StrCmpStrategy, PreSearchStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>>(maxAdditionalSizeInBytes, hashK, loadFactor);    
}

template<class HuffCoderClass, class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPLUTSHuffVariantsFinalTestsSet(unsigned int maxAdditionalSizeInBytes, bool extended) {
    cout << endl << "K-ary LCPLUTSA final variants (huff) " << endl;

    karyLCPLUTSHuffVariantsTestsSet<HuffCoderClass, 5, 1, StrCmpStrategy, KaryLCPSASearch<3, 1, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>(maxAdditionalSizeInBytes, extended);
    karyLCPLUTSHuffVariantsTestsSet<HuffCoderClass, 5, 1, StrCmpStrategy, KaryLCPSASearch<25, 4, 2, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>(maxAdditionalSizeInBytes, extended);
    karyLCPLUTSHuffVariantsTestsSet<HuffCoderClass, 5, 1, StrCmpStrategy, KaryLCPSASearch<25, 4, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>(maxAdditionalSizeInBytes, extended);
    karyLCPLUTSHuffVariantsTestsSet<HuffCoderClass, 5, 1, StrCmpStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>(maxAdditionalSizeInBytes, extended);
    
    karyLCPLUTSHuffVariantsTestsSet<HuffCoderClass, 65, 9, StrCmpStrategy, KaryLCPSASearch<3, 1, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>(maxAdditionalSizeInBytes, extended);

    karyLCPLUTSHuffVariantsTestsSet<HuffCoderClass, 65, 9, StrCmpStrategy, KaryLCPSASearch<33, 5, 3, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>, PlusK>(maxAdditionalSizeInBytes, extended);
}


#endif	/* TESTKARYLUTSASUITE_H */

