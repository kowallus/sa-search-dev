/* 
 * File:   mlKaryLCPSASearchStrategies.h
 * Author: coach
 *
 * Created on 12 czerwca 2015, 13:20
 */

#ifndef MLKARYLCPSASEARCHSTRATEGIES_H
#define	MLKARYLCPSASEARCHSTRATEGIES_H

#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"
#include "karyLCPSASearchStrategies.h"

using namespace std;

namespace SASearch {

    namespace kary {    
        
        template<int localLevelsCount, int k, int step, int lcpMLLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK, class MLKaryLCPSASearchClass>
        class MLKaryLCPSASearchTemplate: 
                public MLKarySASearchTemplate<localLevelsCount, k, step, StringComparisonStrategyClass, plusK, MLKaryLCPSASearchClass>,
                public LCPSASearchCoordinator<k, lcpMLLevels * localLevelsCount, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass> {
        protected:
            prefixarray<prefixLengthInUints>* mlPrefixes;
            
        public:
            MLKaryLCPSASearchTemplate(unsigned int *sa, unsigned char* txt, unsigned int txtSize, string name = "unnamed")
                    : MLKarySASearchTemplate<localLevelsCount, k, step, StringComparisonStrategyClass, plusK, MLKaryLCPSASearchClass>(sa, txt, txtSize, "[LCP: " + toString(lcpMLLevels * localLevelsCount) + "-levels " + toString(prefixLengthInUints * sizeof(unsigned int)) + "-chars (" + StrNCmp32bitsComparisonStrategyClass::getInstance()->getStrategyName() + ")] " + name),
                    LCPSASearchCoordinator<k, lcpMLLevels * localLevelsCount, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass>(this->karySA, txt, txtSize, this->name),
                    mlPrefixes((prefixarray<prefixLengthInUints>*) createMLkAry<localLevelsCount, k, prefixLengthInUints>((unsigned int*) this->prefixes, this->prefixesCount))
            {
                this->sizeInBytes += getKaryPrefixesSizeInBytes<k>(prefixLengthInUints * sizeof(unsigned int), lcpMLLevels * localLevelsCount, txtSize);
            }

            ~MLKaryLCPSASearchTemplate() {
                #ifdef ALIGN_MEMORY
                free2n(this->mlPrefixes);
                #else
                delete[] this->mlPrefixes;
                #endif
            }
            
            inline bool isPatternLowerThanSuffix(int patternLength, unsigned int* sufOffset) {
                return (this->strcmpStrategy(this->ptrn, this->stxt + *sufOffset, patternLength - this->lcp + 1) <= 0);
            }
            
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned char* prefixPtr, unsigned int *sufOffset) {
                int cmpres = this->strncmpCompare->compare(this->ptrn, prefixPtr, prefixLengthInUints * sizeof(unsigned int));
                #ifdef LCP_STATS_ON
                this->updateStats(cmpres, level);
                #endif
                return (cmpres < 0 || (cmpres == 0 && this->strcmpStrategy(this->ptrn + prefixLengthInUints * sizeof(unsigned int), this->stxt + *sufOffset + prefixLengthInUints * sizeof(unsigned int), patternLength - this->lcp - prefixLengthInUints * sizeof(unsigned int) + 1) <= 0));
            }   
            
            inline bool isPatternLowerThanSuffix(int patternLength, unsigned int suffixIndex) {
                return isPatternLowerThanSuffix(patternLength, this->mlKarySA + suffixIndex);
            }
       
            inline bool isPatternLowerThanSuffixUsingPrefixesCache(int patternLength, unsigned int suffixIndex) {
                return isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->mlPrefixes[suffixIndex], this->mlKarySA + suffixIndex);
            } 
            
            inline unsigned int determineMLNodeChildUsingPrefixesCache(int patternLength, unsigned int firstElementIndex, unsigned int &i, int lastLocalNode) {
                return static_cast<MLKaryLCPSASearchClass*>(this)->determineMLNodeChildUsingPrefixesCacheImpl(patternLength, firstElementIndex, i, lastLocalNode);
            }
            
            inline unsigned int determineMLNodeChild(int patternLength, unsigned int firstElementIndex, unsigned int &i, int lastLocalNode) {
                return static_cast<MLKaryLCPSASearchClass*>(this)->determineMLNodeChildImpl(patternLength, firstElementIndex, i, lastLocalNode);
            }
        
            inline int searchNode(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement = 0) { return static_cast<MLKaryLCPSASearchClass*>(this)->searchNodeImpl(patternLength, firstElementIndex, i, node, startElement); }
            
            inline int searchNodeUsingPrefixesCache(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement = 0) { return static_cast<MLKaryLCPSASearchClass*>(this)->searchNodeUsingPrefixesCacheImpl(patternLength, firstElementIndex, i, node, startElement); }
            
            inline void findFirstOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, unsigned int mlNode = 0) {
                i = this->txtSize;
                
                this->lcpInit(pattern);
                
                int mlLevel = 0;
                unsigned int firstElementIndex;
                int lastLocalNode;
                
                while (mlLevel < this->mlLevelsCount) {
                    
                    this->mlNodeCoordinates(mlNode, mlLevel, firstElementIndex, lastLocalNode);
                    
                    int c; 
                    if (mlLevel++ < lcpMLLevels) 
                        c = determineMLNodeChildUsingPrefixesCache(patternLength, firstElementIndex, i, lastLocalNode);
                    else
                        c = determineMLNodeChild(patternLength, firstElementIndex, i, lastLocalNode);
                    
                    mlNode = childNode<MLARYNESS>(mlNode, c);
                }
            }
        
            inline unsigned int determineMLNodeChildUsingPrefixesCacheImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, int lastLocalNode) {
                int node = 0;                
                
                while (node <= lastLocalNode)
                    node = childNode<k>(node, searchNodeUsingPrefixesCache(patternLength, firstElementIndex, i, node));
                
                return node - lastLocalNode - 1;
            }
            
            inline unsigned int determineMLNodeChildImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, int lastLocalNode) {
                int node = 0;                
                
                while (node <= lastLocalNode)
                    node = childNode<k>(node, searchNode(patternLength, firstElementIndex, i, node));
                
                return node - lastLocalNode - 1;
            }
        };
        
        template<int localLevelsCount, int k, int step, int lcpMLLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK = 0>
        class MLKaryLCPSASearch: public MLKaryLCPSASearchTemplate<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, MLKaryLCPSASearch<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK>> {
        private:
            typedef MLKaryLCPSASearch<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK> ThisType;

        public:
            MLKaryLCPSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
            : MLKaryLCPSASearchTemplate<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "")
            {}

            ~MLKaryLCPSASearch() { }

            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + firstElementIndex + startElement + step - 1;
                int c;

                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                
                this->lcpUpdate(c, this->mlPrefixes[j - 1], this->mlPrefixes[j]);    
                return c;
            }
            
            inline int searchNodeImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + firstElementIndex + startElement + step - 1;
                int c;

                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(patternLength, j)) {
                        i = j;
                        break;
                    }
                    j += step;
                }
                
                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    j -= step - 1;
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(patternLength, j)) {
                            i = j;
                            break;
                        }
                        j++;
                    }
                }
                
                return c;
            }
        };
    }
}

#endif	/* MLKARYLCPSASEARCHSTRATEGIES_H */
