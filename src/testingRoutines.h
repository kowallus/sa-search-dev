/* 
 * File:   testingRoutines.h
 * Author: coach
 *
 * Created on 4 grudnia 2015, 13:28
 */

#ifndef TESTINGROUTINES_H
#define	TESTINGROUTINES_H

#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include <cstring>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include "asmlib.h"
#include "interface.h"

#include "SASearchStrategyBase.h"
#include "StrCmpStrategies.h"
#include "SALookupStrategies.h"
#include "SAHuffLookupStrategies.h"
#include "SAHashStrategies.h"
#include "SABinarySearchStrategies.h"
#include "karyLCPmixSASearchStrategies.h"
#include "karyLCPjoinSASearchStrategies.h"
#include "mlKarySASearchStrategies.h"
#include "mlKaryLCPmixSASearchStrategies.h"
#include "mlKaryLCPjoinSASearchStrategies.h"
#include "karyLUTLCPSASearchStrategies.h"
#include "karySSELCPSASearchStrategies.h"

#include "createSAkary.h"
#include "SAkaryHelpers.h"

#ifndef BENCH_BUILD
#include "testingBench.h"
#else
#include "bench/testingBench.h"
#endif

using namespace std;
using namespace SASearch;
using namespace kary;

#ifndef uchar
#define uchar unsigned char
#endif
#ifndef ulong
#define ulong unsigned long
#endif

template<class SASearchStrategyClass>
double testSASearchStrategy(int repeats = REPEATS_DEFAULT) {
    SASearchStrategyClass saSearchStrategy(saArray, txt, txtSize);
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyClass, class BinSearchStrategyClass>
double testSATwoStageSearchStrategy(PreSearchStrategyClass* preSearchStrategy, int repeats = REPEATS_DEFAULT) {
    BinSearchStrategyClass saBinarySearchStrategy(saArray, txt, txtSize, preSearchStrategy->getLCP());
    SATwoStageSearchStrategy<PreSearchStrategyClass, BinSearchStrategyClass>
                saSearchStrategy(preSearchStrategy, &saBinarySearchStrategy);    
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyWithSATwistClass, class BinSearchStrategyClass>
double testSATwoStageSearchWithSATwistStrategy(PreSearchStrategyWithSATwistClass* preSearchWithSATwistStrategy, int repeats = REPEATS_DEFAULT) {
    BinSearchStrategyClass saBinarySearchStrategy(preSearchWithSATwistStrategy->getTwistedSA(), txt, txtSize);
    SATwoStageSearchWithSATwistStrategy<PreSearchStrategyWithSATwistClass, BinSearchStrategyClass>
                saSearchStrategy(preSearchWithSATwistStrategy, &saBinarySearchStrategy);    
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyWithSATwistClass, class BinSearchStrategyClass>
double testSATwistStrategy(PreSearchStrategyWithSATwistClass* preSearchWithSATwistStrategy, int repeats = REPEATS_DEFAULT) {
    BinSearchStrategyClass saBinarySearchStrategy(preSearchWithSATwistStrategy->getTwistedSA(), txt, txtSize);
    SATwistStrategy<PreSearchStrategyWithSATwistClass, BinSearchStrategyClass>
                saSearchStrategy(preSearchWithSATwistStrategy->getTwistedSA(), preSearchWithSATwistStrategy, &saBinarySearchStrategy);    
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyClass, class KarySearchStrategyClass>
double testSATwoStageKaryLUTSearchStrategy(PreSearchStrategyClass* preSearchStrategy, int repeats = REPEATS_DEFAULT) {
    KarySearchStrategyClass saSearchStrategy(saArray, txt, txtSize, preSearchStrategy);
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyWithSATwistClass, class KarySearchStrategyClass>
double testSATwoStageKaryLUTSearchWithSATwistStrategy(PreSearchStrategyWithSATwistClass* preSearchWithSATwistStrategy, int repeats = REPEATS_DEFAULT) {
    KarySearchStrategyClass saSearchStrategy(preSearchWithSATwistStrategy->getTwistedSA(), txt, txtSize, preSearchWithSATwistStrategy);
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyClass, class KarySearchStrategyClass>
double testSATwoStageKaryLUTandLCPSearchStrategy(unsigned int maxAdditionalSizeInBytes, int repeats = REPEATS_DEFAULT) {
    KarySearchStrategyClass saSearchStrategy(saArray, txt, txtSize, maxAdditionalSizeInBytes);
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyClass, class KarySearchStrategyClass>
double testSATwoStageKaryLUTandLCPSearchStrategy(unsigned int maxAdditionalSizeInBytes, unsigned int hashK, unsigned int loadFactor, int repeats = REPEATS_DEFAULT) {
    KarySearchStrategyClass saSearchStrategy(saArray, txt, txtSize, maxAdditionalSizeInBytes, hashK, loadFactor);
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

template<class PreSearchStrategyWithSATwistClass, class KarySearchStrategyClass>
double testSATwoStageKaryLUTandLCPSearchWithSATwistStrategy(unsigned int maxAdditionalSizeInBytes, PreSearchStrategyWithSATwistClass* preSearchWithSATwistStrategy, int repeats = REPEATS_DEFAULT) {
    KarySearchStrategyClass saSearchStrategy(preSearchWithSATwistStrategy->getTwistedSA(), txt, txtSize, maxAdditionalSizeInBytes, preSearchWithSATwistStrategy);
    return testSASearchStrategy(&saSearchStrategy, repeats);
}

#endif	/* TESTINGROUTINES_H */
