/* 
 * File:   StrCmpStrategies.h
 * Author: coach
 *
 * Created on 25 maja 2015, 12:13
 */

#ifndef STRCMPSTRATEGIES_H
#define	STRCMPSTRATEGIES_H

#include "asmlib.h"
#include "../byteswap.h"
#include <string>
#include <cstring>

using namespace std;

namespace SASearch {
    
    class StandardCStrCmp: public StringComparisonStrategyInterface<StandardCStrCmp> {
    
    public:
        StandardCStrCmp()
        : StringComparisonStrategyInterface<StandardCStrCmp>("standard-strcmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) { 
            return strcmp((const char*) pattern, (const char*) suffix);
        }
    };
    
    class AsmLibStrCmp: public StringComparisonStrategyInterface<AsmLibStrCmp> {
    public:
        AsmLibStrCmp()
        : StringComparisonStrategyInterface<AsmLibStrCmp>("asmlib-strcmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) { 
            return A_strcmp((const char*) pattern, (const char*) suffix);
        }
    };
    
    class StandardCStrNCmp: public StringComparisonStrategyInterface<StandardCStrNCmp> {
    public:
        StandardCStrNCmp()
        : StringComparisonStrategyInterface<StandardCStrNCmp>("standard-strncmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength) { 
            return strncmp((const char*) pattern, (const char*) suffix, patternLength);
        }
    };
    
    class AsmLibModStrNCmp: public StringComparisonStrategyInterface<AsmLibModStrNCmp> {
    public:
        AsmLibModStrNCmp()
        : StringComparisonStrategyInterface<AsmLibModStrNCmp>("asmlib(mod)-strncmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength) { 
            char mem = *(suffix + patternLength);
            *(suffix + patternLength) = 0;
            int cmpRes = A_strcmp((const char*) pattern, (const char*) suffix);
            *(suffix + patternLength) = mem;
            return cmpRes;
        }
    };
    
    class LCPStrNCmp8: public StringComparisonStrategyInterface<LCPStrNCmp8> {
    public:
        LCPStrNCmp8()
        : StringComparisonStrategyInterface<LCPStrNCmp8>("lcp(8)-strncmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) {       
            int i = 0;

            while (i < patternLength) {
                i++;
                uint8_t patuint = *(uint8_t*)pattern++;
                uint8_t sufuint = *(uint8_t*)suffix++;
                if (patuint != sufuint)
                    return patuint > sufuint?i:-i;
            }

            return 0;
        }
    };
    
    class LCPStrNCmp16: public StringComparisonStrategyInterface<LCPStrNCmp16> {
    public:
        LCPStrNCmp16()
        : StringComparisonStrategyInterface<LCPStrNCmp16>("lcp(16)-strncmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) {
            int i = 0;

            while (patternLength - i >= 2) {
                if (*(uint16_t*)pattern != *(uint16_t*)suffix);
                    break;
                pattern += 2;
                suffix += 2;
                i += 2;
            }
            
            while (i < patternLength) {
                i++;
                uint8_t patuint = *(uint8_t*)pattern++;
                uint8_t sufuint = *(uint8_t*)suffix++;
                if (patuint != sufuint)
                    return patuint > sufuint?i:-i;
            }

            return 0;
        }
    };

    class LCPStrNCmp32: public StringComparisonStrategyInterface<LCPStrNCmp32> {
    public:
        LCPStrNCmp32()
        : StringComparisonStrategyInterface<LCPStrNCmp32>("lcp(32)-strncmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) {       
            int i = 0;

            while (patternLength - i >= 4) {
                if (*(uint32_t*)pattern != *(uint32_t*)suffix)
                    break;
                pattern += 4;
                suffix += 4;
                i += 4;
            }

            while (patternLength - i >= 2) {
                if (*(uint16_t*)pattern != *(uint16_t*)suffix);
                    break;
                pattern += 2;
                suffix += 2;
                i += 2;
            }
            
            while (i < patternLength) {
                i++;
                uint8_t patuint = *(uint8_t*)pattern++;
                uint8_t sufuint = *(uint8_t*)suffix++;
                if (patuint != sufuint)
                    return patuint > sufuint?i:-i;
            }

            return 0;
        }
    };    
    
    // UNSAFE (faster then asmlib :) does not test equality correctly
    class BSwap32StrNECmp: public StringComparisonStrategyInterface<BSwap32StrNECmp> {
    public:
        BSwap32StrNECmp()
        : StringComparisonStrategyInterface<BSwap32StrNECmp>("bswap32-strNEcmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) {       
            while (patternLength > 0) {
                uint32_t patuint = *(uint32_t*)pattern;
                uint32_t sufuint = *(uint32_t*)suffix;
                if (patuint != sufuint)
                    return bswap_32(patuint)>bswap_32(sufuint)?1:-1;
                pattern += 4;
                suffix += 4;
                patternLength -= 4;
            }

            return 0;
        }
    };
    
    class Swapped64StrSECmp: public StringComparisonStrategyInterface<Swapped64StrSECmp> {
    public:
        Swapped64StrSECmp()
        : StringComparisonStrategyInterface<Swapped64StrSECmp>("swapped64-strSEcmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) {       
            uint64_t patuint = *(uint64_t*)pattern;
            uint64_t sufuint = *(uint64_t*)suffix;
            if (patuint != sufuint)
                return patuint>sufuint?1:-1;

            return 0;
        }
    };
    
    // UNSAFE (faster then asmlib :) does not test equality correctly
    class BulitinBSwap32StrNECmp: public StringComparisonStrategyInterface<BulitinBSwap32StrNECmp> {
    public:
        BulitinBSwap32StrNECmp()
        : StringComparisonStrategyInterface<BulitinBSwap32StrNECmp>("bulitin-bswap32-strNEcmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) {       
            while (patternLength > 0) {
                uint32_t patuint = *(uint32_t*)pattern;
                uint32_t sufuint = *(uint32_t*)suffix;
                if (patuint != sufuint)
                    return __builtin_bswap32(patuint)>__builtin_bswap32(sufuint)?1:-1;
                pattern += 4;
                suffix += 4;
                patternLength -= 4;
            }

            return 0;
        }
    };
    
    // UNSAFE
    class BSwap32StrSECmp: public StringComparisonStrategyInterface<BSwap32StrSECmp> {
    public:
        BSwap32StrSECmp()
        : StringComparisonStrategyInterface<BSwap32StrSECmp>("bswap32-strSEcmp") {}
        
        inline int compareImpl(const unsigned char* pattern, unsigned char* suffix, int patternLength = 0) {       
            uint32_t patuint = *(uint32_t*)pattern;
            uint32_t sufuint = *(uint32_t*)suffix;
            if (patuint != sufuint)
                return bswap_32(patuint)>bswap_32(sufuint)?1:-1;
            
            return 0;
        }
    };
    
}

#endif	/* STRCMPSTRATEGIES_H */

