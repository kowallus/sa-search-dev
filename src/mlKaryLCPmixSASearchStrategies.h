/* 
 * File:   mlKaryLCPmixSASearchStrategies.h
 * Author: coach
 *
 * Created on 18 czerwca 2015, 22:00
 */

#ifndef MLKARYLCPMIXSASEARCHSTRATEGIES_H
#define	MLKARYLCPMIXSASEARCHSTRATEGIES_H

#include "SAkaryHelpers.h"
#include "SASearchStrategyBase.h"
#include "createSAkary.h"
#include "mlKaryLCPSASearchStrategies.h"

using namespace std;

namespace SASearch {

    namespace kary {    
        
        template<int localLevelsCount, int k, int step, int lcpMLLevels, int prefixLengthInUints, class StrNCmp32bitsComparisonStrategyClass, class StringComparisonStrategyClass, int plusK = 0>
        class MLKaryLCPmixSASearch: 
                public MLKaryLCPSASearchTemplate<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, MLKaryLCPmixSASearch<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK>> {
        private:
            typedef MLKaryLCPmixSASearch<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK> ThisType;
            
        protected:
            unsigned int* mlMixSA;
            
            unsigned int firstNoPrefixNode;
            unsigned int firstNoPrefixIndex;
            unsigned int* firstNoPrefixElem;
            
            inline unsigned int* prefixElemIndex2Ptr(unsigned int index) {
                return mlMixSA + index * (1 + prefixLengthInUints);
            }
            
            inline unsigned int* noPrefixElemIndex2Ptr(unsigned int index) {
                return firstNoPrefixElem + (index - firstNoPrefixIndex);
            }

            inline unsigned char* prefixElemPtr2Prefix(unsigned int* ptr) {
                return (unsigned char*) (ptr + 1);
            }
            
            inline unsigned int* elemPtr2SuffixPtr(unsigned int* ptr) {
                return ptr;
            }
            
        public:
            MLKaryLCPmixSASearch(unsigned int *sa, unsigned char* txt, unsigned int txtSize)
                    : MLKaryLCPSASearchTemplate<localLevelsCount, k, step, lcpMLLevels, prefixLengthInUints, StrNCmp32bitsComparisonStrategyClass, StringComparisonStrategyClass, plusK, ThisType>(sa, txt, txtSize, "LCPmixSA"),
                    mlMixSA(mixKaryWithPrefixes<k, prefixLengthInUints>(this->mlKarySA, this->mlPrefixes, txtSize, localLevelsCount * lcpMLLevels)),
                    firstNoPrefixNode((localLevelsCount * lcpMLLevels < this->levelsCount)?karyNodes(k, localLevelsCount * lcpMLLevels):(this->lastNode + 1)),
                    firstNoPrefixIndex(node2index<k>(firstNoPrefixNode)),
                    firstNoPrefixElem(prefixElemIndex2Ptr(firstNoPrefixIndex))
            {
            }

            ~MLKaryLCPmixSASearch() {
                #ifdef ALIGN_MEMORY
                free2n(mlMixSA);
                #else
                delete[] mlMixSA;
                #endif
            }
            
            inline bool findPlusKOccurrenceImpl(const unsigned char* pattern, int patternLength, unsigned int &i, int counter) {
                unsigned int* ptr;
                if (this->firstNoPrefixIndex < i)
                    ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                else
                    ptr = this->mlMixSA + i * (1 + prefixLengthInUints);

                if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                    return true;                
                
                int mlLevel;
                unsigned int mlNodeIndex;
                unsigned int mlNodeElements;
                unsigned int mlNodeInMLLevel;
                unsigned int mlNode;
                
                this->indexMLCoordinates(i, mlLevel, mlNodeIndex, mlNodeElements, mlNodeInMLLevel);
                if (mlLevel == this->mlLevelsCount - 1)     
                    mlNode = this->nonLeafMLNodes + mlNodeInMLLevel;
                else
                    mlNode = index2node<MLARYNESS>(mlNodeIndex);
                
                while (counter--) {
                    if (!this->incrementMLKaryIndex(i, mlLevel, mlNode, mlNodeIndex, mlNodeElements))
                        return true;
                    
                    if (this->firstNoPrefixIndex < i)
                        ptr = this->firstNoPrefixElem + (i - this->firstNoPrefixIndex);
                    else
                        ptr = this->mlMixSA + i * (1 + prefixLengthInUints);
                        
                    if (this->strcmpStrategy(pattern, this->txt + *ptr, patternLength + 1) <= 0)
                        return true;
                }
                
                return false;
            }   
            
            virtual unsigned int getSuffixOffset(unsigned int index) { 
                return *((firstNoPrefixIndex < index)?noPrefixElemIndex2Ptr(index):prefixElemIndex2Ptr(index));
            };
            
            unsigned int* getMLKaryLCPmixSA() { return this->mlMixSA; }
            
            inline int searchNodeUsingPrefixesCacheImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + firstElementIndex;
                int c;
                
                unsigned int* ptr = this->prefixElemIndex2Ptr(j + startElement + step - 1);
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->prefixElemPtr2Prefix(ptr), this->elemPtr2SuffixPtr(ptr))) {
                        i = j + c;
                        break;
                    }
                    ptr += step * (1 + prefixLengthInUints);
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    ptr -= (step - 1) * (1 + prefixLengthInUints);
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffixUsingPrefixesCache(patternLength, this->prefixElemPtr2Prefix(ptr), this->elemPtr2SuffixPtr(ptr))) {
                            i = j + c;
                            break;
                        }
                        ptr += (1 + prefixLengthInUints);
                    }
                }
                
                this->lcpUpdate(c, this->prefixElemPtr2Prefix(ptr - (1 + prefixLengthInUints)), this->prefixElemPtr2Prefix(ptr));

                return c;
            }
            
            inline int searchNodeImpl(int patternLength, unsigned int firstElementIndex, unsigned int &i, unsigned int node, int startElement) {
                unsigned int j = node2index<k>(node) + firstElementIndex;
                int c;
                
                unsigned int* ptr = this->noPrefixElemIndex2Ptr(j + startElement + step - 1);
                for(c = startElement + step - 1; c < ELEMENTSINNODE; c += step) {
                    if (this->isPatternLowerThanSuffix(patternLength, this->elemPtr2SuffixPtr(ptr))) {
                        i = j + c;
                        break;
                    }
                    ptr += step;
                }

                if (step > 1) {
                    int guard = c > ELEMENTSINNODE?ELEMENTSINNODE:c;
                    ptr -= (step - 1);
                    for(c -= step - 1; c < guard; c++) {
                        if (this->isPatternLowerThanSuffix(patternLength, this->elemPtr2SuffixPtr(ptr))) {
                            i = j + c;
                            break;
                        }
                        ptr++;
                    }
                }
  
                return c;
            }
        };
    }
}

#endif	/* MLKARYLCPMIXSASEARCHSTRATEGIES_H */
