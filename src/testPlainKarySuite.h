/* 
 * File:   testPlainKarySuite.h
 * Author: coach
 *
 * Created on 7 grudnia 2015, 09:51
 */

#ifndef TESTPLAINKARYSUITE_H
#define	TESTPLAINKARYSUITE_H

#include "testingRoutines.h"

template<int k>
void karyVariantsTestsSet();

template<int k, int step>
void karyStepVariantsTestsSet();

template<int k>
void karySimpleVariantsTestsSet();

template<int k>
void karySimpleCmpVariantsTestsSet();

template<int step, int PlusK>
void karyStepPlusKVariantsTestsSet();

template<class CmpStrategy, int PlusK>
void karyStepUpdatePlusKVariantsTestsSet();

template<class StrCmpStrategy, int PlusK>
void karyVariantsPlannedTestsSet();

template<class StrCmpStrategy, int PlusK>
void karyVariantsSelectedTestsSet();

template<class HuffCoderClass, class StrCmpStrategy, int PlusK>
void saLUTHuffCoderVariantsTestsSet() {
    HuffCoderClass* coder = new HuffCoderClass((const char*) txt, txtSize);
    SAHuffLookupWithSATwist<HuffCoderClass, 15>* saLUThuffTwisted15 = new SAHuffLookupWithSATwist<HuffCoderClass, 15>(saArray, txt, txtSize, coder);
    testSATwoStageSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 15>, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUThuffTwisted15);
    SAHuffLookupWithSATwist<HuffCoderClass, 19>* saLUThuffTwisted19 = new SAHuffLookupWithSATwist<HuffCoderClass, 19>(saArray, txt, txtSize, coder);
    testSATwoStageSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 19>, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUThuffTwisted19);
    SAHuffLookupWithSATwist<HuffCoderClass, 23>* saLUThuffTwisted23 = new SAHuffLookupWithSATwist<HuffCoderClass, 23>(saArray, txt, txtSize, coder);
    testSATwoStageSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 23>, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUThuffTwisted23);
    SAHuffLookupWithSATwist<HuffCoderClass, 25>* saLUThuffTwisted25 = new SAHuffLookupWithSATwist<HuffCoderClass, 25>(saArray, txt, txtSize, coder);
    testSATwoStageSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 25>, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUThuffTwisted25);
//    SAHuffLookupWithSATwist<HuffCoderClass, 27>* saLUThuffTwisted27 = new SAHuffLookupWithSATwist<HuffCoderClass, 27>(saArray, txt, txtSize, coder);
//    testSATwoStageSearchWithSATwistStrategy<SAHuffLookupWithSATwist<HuffCoderClass, 27>, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUThuffTwisted27);
    
    delete saLUThuffTwisted15;
    delete saLUThuffTwisted19;
    delete saLUThuffTwisted23;
    delete saLUThuffTwisted25; 
//    delete saLUThuffTwisted27;
    delete coder;
}

template<class StrCmpStrategy, int PlusK>
void saLUTHTVariantsTestsSet(unsigned int hashK) {
    
    testSASearchStrategy<SASimpleBinarySearch<StrCmpStrategy, PlusK>>();
   
//    testSATwoStageSearchStrategy<SALookupOn1Symbol, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUT1);
    testSATwoStageSearchStrategy<SALookupOn2Symbols, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUT2);
    testSATwoStageSearchStrategy<SALookupOn3Symbols, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saLUT3);

    saLUTHuffCoderVariantsTestsSet<CoderOrd0, StrCmpStrategy, PlusK>();
    
    SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>* saHash90p = new SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>(saArray, txt, txtSize, hashK<patternLength?hashK:patternLength-1, 90);
    testSATwoStageSearchStrategy<SAHashLookup<LCPStrNCmp32, SALookupOn2Symbols>, SASimpleBinarySearch<StrCmpStrategy, PlusK>>(saHash90p);
    delete saHash90p;
}

template<class StrCmpStrategy, int PlusK>
void saLUTHuffVariantsTestsSet() {   
    saLUTHuffCoderVariantsTestsSet<CoderOrd0, StrCmpStrategy, PlusK>();
    saLUTHuffCoderVariantsTestsSet<CoderOrd1, StrCmpStrategy, PlusK>();
    saLUTHuffCoderVariantsTestsSet<CoderOrd2, StrCmpStrategy, PlusK>();    
}

template<class StrCmpStrategy>
void preliminaryKaryVariantsTestsSet() {
          
    if (patternLength <= 12) {
        karyStepUpdatePlusKVariantsTestsSet<StrCmpStrategy, 1>();
    }       

    karyStepUpdatePlusKVariantsTestsSet<StrCmpStrategy, 0>();
    karyStepUpdatePlusKVariantsTestsSet<StrCmpStrategy,7>();
      
    karySimpleCmpVariantsTestsSet<0>();
    karySimpleCmpVariantsTestsSet<2>();
    karySimpleCmpVariantsTestsSet<6>();
    karySimpleVariantsTestsSet<6>();
    karySimpleVariantsTestsSet<7>();
   
    karyVariantsSelectedTestsSet<StrCmpStrategy, 0>();
    karyVariantsSelectedTestsSet<StrCmpStrategy, 1>();
    karyVariantsSelectedTestsSet<StrCmpStrategy, 2>();
    karyVariantsSelectedTestsSet<StrCmpStrategy, 4>();
    karyVariantsSelectedTestsSet<StrCmpStrategy, 6>();
    karyVariantsSelectedTestsSet<StrCmpStrategy, 8>();
}

template<class StrCmpStrategy>
void variousPlainKaryVariantsTestsSet() {
    testSASearchStrategy<EytzingerAdvancedSASearch<BSwap32StrNECmp>>();        
    testSASearchStrategy<KarySASearch<4, 1, BSwap32StrNECmp>>();
}

template<class StrCmpStrategy, int PlusK>
void karyVariantsSelectedTestsSet() {
    cout << endl << "K-ary variants " << endl;;

    testSASearchStrategy<KarySASearch<3, 1, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<5, 1, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<6, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<29, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<34, 5, StrCmpStrategy, PlusK>>();
    
    // CONTROL - WEAKER VARIANTS
    testSASearchStrategy<KarySASearch<4, 1, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<16, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<17, 3, StrCmpStrategy, PlusK>>();
}

template<int k>
void karyVariantsTestsSet() {    
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 1>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 2>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 3>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 4>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 5>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 6>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 7>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 8>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 9>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 10>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 16>>();
    testSASearchStrategy<KarySASearch<k, 1, BSwap32StrNECmp, 32>>();
    
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 1>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 2>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 3>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 4>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 5>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 6>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 7>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 8>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 9>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 10>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 16>>();
    testSASearchStrategy<KarySASearch<k, 1, AsmLibStrCmp, 32>>();
}

template<int k, int step>
void karyStepVariantsTestsSet() {

    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 1>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 2>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 3>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 4>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 5>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 6>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 7>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 8>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 9>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 10>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 16>>();
    testSASearchStrategy<KarySASearch<k, step, BSwap32StrNECmp, 32>>();
    
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 1>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 2>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 3>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 4>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 5>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 6>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 7>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 8>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 9>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 10>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 16>>();
    testSASearchStrategy<KarySASearch<k, step, AsmLibStrCmp, 32>>();
    
}

template<int PlusK>
void karySimpleVariantsTestsSet() {    
    testSASearchStrategy<KarySASearch<2, 1, BSwap32StrNECmp, PlusK>>();
    testSASearchStrategy<KarySASearch<3, 1, BSwap32StrNECmp, PlusK>>();
    testSASearchStrategy<KarySASearch<3, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<4, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<5, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<6, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<7, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<8, 1, AsmLibStrCmp, PlusK>>();
}

template<int PlusK>
void karySimpleCmpVariantsTestsSet() {    
    testSASearchStrategy<KarySASearch<2, 1, BSwap32StrNECmp, PlusK>>();
    testSASearchStrategy<KarySASearch<3, 1, BSwap32StrNECmp, PlusK>>();
    testSASearchStrategy<KarySASearch<4, 1, BSwap32StrNECmp, PlusK>>();
    testSASearchStrategy<KarySASearch<5, 1, BSwap32StrNECmp, PlusK>>();
    testSASearchStrategy<KarySASearch<6, 1, BSwap32StrNECmp, PlusK>>();
    testSASearchStrategy<KarySASearch<3, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<4, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<5, 1, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<6, 1, AsmLibStrCmp, PlusK>>();
}

template<int step, int PlusK>
void karyStepPlusKVariantsTestsSet() {
    testSASearchStrategy<KarySASearch<6, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<7, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<8, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<9, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<10, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<11, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<12, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<13, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<14, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<15, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<16, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<17, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<18, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<19, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<20, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<21, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<22, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<23, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<24, step, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KarySASearch<25, step, AsmLibStrCmp, PlusK>>();
}

template<class StrCmpStrategy, int PlusK>
void karyStepUpdatePlusKVariantsTestsSet() {  
    testSASearchStrategy<KarySASearch<5, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<6, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<7, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<8, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<9, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<10, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<11, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<11, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<12, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<13, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<14, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<15, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<16, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<17, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<18, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<19, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<21, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<22, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<23, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<24, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<26, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<27, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<28, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<29, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<28, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<29, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<30, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<31, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<32, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<33, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<34, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<35, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<35, 6, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<36, 6, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<37, 6, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<38, 6, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<39, 6, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<40, 6, StrCmpStrategy, PlusK>>();
}

template<class StrCmpStrategy, int PlusK>
void karyVariantsPlannedTestsSet() {
    cout << endl << "K-ary variants final plan" << endl;;

    testSASearchStrategy<KarySASearch<3, 1, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<5, 1, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<9, 2, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<17, 3, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<33, 5, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<41, 6, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<49, 7, StrCmpStrategy, PlusK>>();
    
    // CONTROL - WEAKER VARIANTS
    testSASearchStrategy<KarySASearch<2, 1, StrCmpStrategy, PlusK>>();
    
    testSASearchStrategy<KarySASearch<57, 8, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KarySASearch<65, 9, StrCmpStrategy, PlusK>>();
    
}

template<class StrCmpStrategy>
void karyVariantsPlusKPlannedTestsSet() {
    cout << endl << "K-ary variants PlusK final plan" << endl;;

    testSASearchStrategy<KarySASearch<3, 1, StrCmpStrategy, 0>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, 0>>();
    testSASearchStrategy<KarySASearch<3, 1, StrCmpStrategy, 1>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, 1>>();
    testSASearchStrategy<KarySASearch<3, 1, StrCmpStrategy, 2>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, 2>>();
    testSASearchStrategy<KarySASearch<3, 1, StrCmpStrategy, 4>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, 4>>();
    testSASearchStrategy<KarySASearch<3, 1, StrCmpStrategy, 8>>();
    testSASearchStrategy<KarySASearch<25, 4, StrCmpStrategy, 8>>();    
}



#endif	/* TESTPLAINKARYSUITE_H */

