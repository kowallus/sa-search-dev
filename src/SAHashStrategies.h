/* 
 * File:   SAHashStrategies.h
 * Author: coach
 *
 * Created on 25 maja 2015, 12:53
 */

#ifndef SAHASHSTRATEGIES_H
#define	SAHASHSTRATEGIES_H

#include <string>
#include <cassert>
#include "SABinarySearchStrategies.h"
#include "StrCmpStrategies.h"
#include "SASearchStrategyBase.h"
#include "../common/xxhash.h"
#include "SAkaryHelpers.h"

#define HASH_STATS_ON

using namespace std;

namespace SASearch {
    
    template<class StrNCmpComparisonStrategyClass, class LUTSearchStrategyClass>
    class SAHashLookup: public SASearchStrategyInterface<SAHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> {
    private:
        typedef unsigned int sa_uint;
        typedef SASearchStrategyInterface<LUTSearchStrategyClass> LUTSearchStrategy;
        typedef StringComparisonStrategyInterface<StrNCmpComparisonStrategyClass> StrNCmpComparisionStrategy;
        
        LUTSearchStrategy* lutSearch;
        StrNCmpComparisionStrategy* strncmpCompare;
        
        int lcp;
        sa_uint* sa;
        unsigned char* txt;
        long long txtSize;
        long long hTLen;
        sa_uint *hT;
        
        int loadFactor;
        
        unsigned char* cUpdateLUTPrefix;
        unsigned int cUpdateLUTBeg = 1;
        unsigned int cUpdateLUTEnd = 0;
        sa_uint *updateHT;
        
        // statistics
        
        unsigned long hashF(unsigned char* str) {
                //return rc_crc32(0, (const char *)str, strlen((const char *)str));
                //return murmur3_32(str, strlen((const char *)str), 0);
                //return djb2(str);
                //return sdbm(str);
                return XXH32(str, lcp, 0);
        }
        
        long long countHTLen(unsigned char *text, sa_uint *sa, long long textLen, long long saLen, int k, int loadFactor) {
            long long hTLen = 0;

            unsigned char *lastPattern = (unsigned char*)(text + sa[0]);
            ++hTLen;
            
            for (int i = 1; i < saLen; i++) {
                    if (sa[i] >= (sa_uint)(textLen - k)) {
                            continue;
                    }
                    if (strncmpCompare->compare((text + sa[i]), lastPattern, k) == 0) {
                            continue;
                    }
                    else {
                            ++hTLen;
                           lastPattern = (unsigned char*)(text + sa[i]);
                    }
            }

            hTLen = (long long)((double)hTLen * (100.0 / (double)loadFactor));
            return hTLen;
        }
        
        void fillSAHashTable(unsigned char* text, sa_uint *sa, long long textLen, long long saLen) {
            unsigned int hash = hTLen;
            hT = new sa_uint[2 * hTLen];

            for (long long i = 0; i < 2 * hTLen; ++i) {
                    hT[i] = (sa_uint)saLen;
            }

            unsigned char *lastPattern = new unsigned char[lcp + 1];
            for (int i = 0; i < lcp; ++i) {
                    lastPattern[i] = 255;
            }
            lastPattern[lcp] = '\0';

            unsigned char *pattern = new unsigned char[lcp + 1];
            strncpy((char *)pattern, (const char *)(text + sa[0]), lcp);
            pattern[lcp] = '\0';

            for (int i = 0; i < saLen; i++) {
                    if (sa[i] >(sa_uint)(textLen - lcp)) {
                            continue;
                    }
                    strncpy((char *)pattern, (const char *)(text + sa[i]), lcp);
                    pattern[lcp] = '\0';
                    if (strcmp((char *)pattern, (const char *)lastPattern) == 0) {
                            continue;
                    }
                    else {
                            if (hash != hTLen) hT[2 * hash + 1] = i;
                            hash = hashF((unsigned char*)pattern) % hTLen;
                            strcpy((char *)lastPattern, (const char *)pattern);
                    }
                    while (true) {
                            if (hT[2 * hash] == (unsigned long long)saLen) {
                                    hT[2 * hash] = i;
                                    break;
                            }
                            else {
                                    hash = (hash + 1) % hTLen;
                            }
                    }
            }

            delete[] lastPattern;
            delete[] pattern;
        }
        
    public:
        SAHashLookup(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int lcp, int loadFactor)
        : SASearchStrategyInterface<SAHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>>("hash" + toString(loadFactor) + "(k=" + toString(lcp) + "," + StrNCmpComparisonStrategyClass::getInstance()->getStrategyName() + ")", 0),
                lutSearch(new LUTSearchStrategyClass(sa, txt, txtSize)),
                strncmpCompare(StrNCmpComparisonStrategyClass::getInstance()),
                lcp(lcp),
                sa(sa),
                txt(txt),
                txtSize(txtSize),
                hTLen(countHTLen((unsigned char*) txt, sa, txtSize, txtSize, lcp, loadFactor)), 
                loadFactor(loadFactor) {
            fillSAHashTable(txt, sa, txtSize, txtSize);
            this->name +=  " on " + lutSearch->getStrategyName();
            this->sizeInBytes += lutSearch->getSizeInBytes();
            this->sizeInBytes += 2 * hTLen * sizeof(sa_uint);
            
            cUpdateLUTPrefix = new unsigned char[((LUTSearchStrategyClass*) lutSearch)->getLCP() + 1]();
            cUpdateLUTBeg = 1;
            updateHT = new sa_uint[2 * hTLen];
            for(int i = 0; i < 2 * hTLen; i++)
                updateHT[i] = hT[i];
            
        }
        
        inline void useAddress4DataImpl(unsigned int* addr) {
            sa = addr;
        }
        
        ~SAHashLookup() {
            delete(hT);
            delete(updateHT);
        }
               
        string getParamsString() { 
            return toString(lcp) + " " + toString(loadFactor);
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return lcp <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int begLut, endLut;
            lutSearch->search(pattern, patternLength, begLut, endLut);
            int hash = hashF(pattern) % hTLen;
            
            while (true) {
                    beg = hT[2 * hash];
                    if ((long long)beg == this->txtSize) {
                            beg = 0; end = 0;
                            break;
                    }

                    if (beg >= begLut && beg < endLut && strncmpCompare->compare(pattern, txt + sa[beg], lcp) == 0) {
                        end = hT[2 * hash + 1];
//                    if (beg >= begLut && beg < endLut && strncmpCompare->compare(pattern, txt + sa[(beg + end) / 2], lcp) == 0) {
                            break;
                    }
 
                    hash++;
                    if (hash == hTLen) {
                            hash = 0;
                    }
            }
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
            if (strncmp((char*) cUpdateLUTPrefix, (char*) pattern, ((LUTSearchStrategyClass*) lutSearch)->getLCP())) {
                ((LUTSearchStrategyClass*) lutSearch)->update(cUpdateLUTPrefix, cUpdateLUTBeg, beg);
                strncpy((char*) cUpdateLUTPrefix, (char*) pattern, ((LUTSearchStrategyClass*) lutSearch)->getLCP());
                cUpdateLUTBeg = beg;
            }
            cUpdateLUTEnd = end;
            
            unsigned int begLut, endLut;
            lutSearch->search(pattern, lcp, begLut, endLut);
            char kHTChar = pattern[lcp];
            pattern[lcp] = '\0';
            int hash = hashF(pattern) % hTLen;
            unsigned int oldBeg;
            while (true) {
                    oldBeg = hT[2 * hash];
                    if ((long long)oldBeg == this->txtSize) {
                        cout << "SA-Hash update miss-error!";
                        exit(0);
                    }
                    if (oldBeg >= begLut && oldBeg < endLut && strncmpCompare->compare(pattern, txt + sa[oldBeg], lcp) == 0) {
                        updateHT[2 * hash] = beg;
                        updateHT[2 * hash + 1] = end;
                        break;
                    }
                    hash++;
                    if (hash == hTLen) {
                            hash = 0;
                    }
            }
            pattern[lcp] = kHTChar;    
        }
        
        inline void finalizeUpdate(unsigned int *sa) {
            ((LUTSearchStrategyClass*) lutSearch)->update(cUpdateLUTPrefix, cUpdateLUTBeg, cUpdateLUTEnd);
            this->sa = sa;
            delete[] hT;
            hT = updateHT;
            updateHT = 0;
        }
        
        inline int getLCP() { return lcp; }
    };
    
    template<class StrNCmpComparisonStrategyClass, class LUTSearchStrategyClass>
    class SAHashExLookup: public SASearchStrategyInterface<SAHashExLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> {
    private:
        typedef unsigned int sa_uint;
        typedef SASearchStrategyInterface<LUTSearchStrategyClass> LUTSearchStrategy;
        typedef StringComparisonStrategyInterface<StrNCmpComparisonStrategyClass> StrNCmpComparisionStrategy;
        
        LUTSearchStrategy* lutSearch;
        StrNCmpComparisionStrategy* strncmpCompare;
        
        int lcp;
        sa_uint* sa;
        unsigned char* txt;
        long long txtSize;
        long long hTLen;
        sa_uint *hT;
        
        int loadFactor;
        
        unsigned char* cUpdateLUTPrefix;
        unsigned int cUpdateLUTBeg = 1;
        unsigned int cUpdateLUTEnd = 0;
        sa_uint *updateHT;
        
        // statistics
        
        unsigned long hashF(unsigned char* str) {
                //return rc_crc32(0, (const char *)str, strlen((const char *)str));
                //return murmur3_32(str, strlen((const char *)str), 0);
                //return djb2(str);
                //return sdbm(str);
                return XXH32(str, strlen((const char *)str), 0);
        }
        
        long long countHTLen(unsigned char *text, sa_uint *sa, long long textLen, long long saLen, int k, int loadFactor) {
            long long hTLen = 0;

            unsigned char *lastPattern = new unsigned char[k + 1];
            for (int i = 0; i < k; ++i) {
                    lastPattern[i] = 255;
            }
            lastPattern[k] = '\0';

            unsigned char *pattern = new unsigned char[k + 1];
            strncpy((char *)pattern, (const char*)(text + sa[0]), k);
            pattern[k] = '\0';

            for (int i = 0; i < saLen; i++) {
                    if (sa[i] >= (sa_uint)(textLen - k)) {
                            continue;
                    }
                    strncpy((char *)pattern, (const char*)(text + sa[i]), k);
                    pattern[k] = '\0';
                    if (strcmp((char *)pattern, (const char*)lastPattern) == 0) {
                            continue;
                    }
                    else {
                            ++hTLen;
                            strcpy((char *)lastPattern, (const char*)pattern);
                    }
            }

            delete[] lastPattern;
            delete[] pattern;

            hTLen = (long long)((double)hTLen * (100.0 / (double)loadFactor));
            return hTLen;
        }
        
        void fillSAHashTable(unsigned char* text, sa_uint *sa, long long textLen, long long saLen) {
            unsigned int hash = hTLen;
            hT = new sa_uint[3 * hTLen];

            for (long long i = 0; i < 3 * hTLen; ++i) {
                    hT[i] = (sa_uint)saLen;
            }

            unsigned char *lastPattern = new unsigned char[lcp + 1];
            for (int i = 0; i < lcp; ++i) {
                    lastPattern[i] = 255;
            }
            lastPattern[lcp] = '\0';

            unsigned char *pattern = new unsigned char[lcp + 1];
            strncpy((char *)pattern, (const char *)(text + sa[0]), lcp);
            pattern[lcp] = '\0';

            for (int i = 0; i < saLen; i++) {
                    if (sa[i] >(sa_uint)(textLen - lcp)) {
                            continue;
                    }
                    strncpy((char *)pattern, (const char *)(text + sa[i]), lcp);
                    pattern[lcp] = '\0';
                    if (strcmp((char *)pattern, (const char *)lastPattern) == 0) {
                            continue;
                    }
                    else {
                            if (hash != hTLen) {
                                hT[3 * hash + 1] = i;
                                hT[3 * hash + 2] = sa[i - 1];
                            }
                            hash = hashF((unsigned char*)pattern) % hTLen;
                            strcpy((char *)lastPattern, (const char *)pattern);
                    }
                    while (true) {
                            if (hT[3 * hash] == (unsigned long long)saLen) {
                                    hT[3 * hash] = i;
                                    break;
                            }
                            else {
                                    hash = (hash + 1) % hTLen;
                            }
                    }
            }

            delete[] lastPattern;
            delete[] pattern;
        }
        
    public:
        SAHashExLookup(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int lcp, int loadFactor)
        : SASearchStrategyInterface<SAHashExLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>>("hashEx" + toString(loadFactor) + "(k=" + toString(lcp) + "," + StrNCmpComparisonStrategyClass::getInstance()->getStrategyName() + ")", 0),
                lutSearch(new LUTSearchStrategyClass(sa, txt, txtSize)),
                strncmpCompare(StrNCmpComparisonStrategyClass::getInstance()),
                lcp(lcp),
                sa(sa),
                txt(txt),
                txtSize(txtSize),
                hTLen(countHTLen((unsigned char*) txt, sa, txtSize, txtSize, lcp, loadFactor)), 
                loadFactor(loadFactor) {
            fillSAHashTable(txt, sa, txtSize, txtSize);
            this->name +=  " on " + lutSearch->getStrategyName();
            this->sizeInBytes += lutSearch->getSizeInBytes();
            this->sizeInBytes += 3 * hTLen * sizeof(sa_uint);
            
            cUpdateLUTPrefix = new unsigned char[((LUTSearchStrategyClass*) lutSearch)->getLCP() + 1]();
            cUpdateLUTBeg = 1;
            updateHT = new sa_uint[3 * hTLen];
            for(int i = 0; i < 3 * hTLen; i++)
                updateHT[i] = hT[i];
            
        }
        
        inline void useAddress4DataImpl(unsigned int* addr) {
            sa = addr;
        }
        
        ~SAHashExLookup() {
            delete(hT);
            delete(updateHT);
        }
               
        string getParamsString() { 
            return toString(lcp) + " " + toString(loadFactor);
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return lcp <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int begLut, endLut;
            lutSearch->search(pattern, patternLength, begLut, endLut);
            char kHTChar = pattern[lcp];
            pattern[lcp] = '\0';
            int hash = hashF(pattern) % hTLen;
            
            while (true) {
                    beg = hT[3 * hash];
                    if ((long long)beg == this->txtSize) {
                            beg = 0; end = 0;
                            break;
                    }

                    if (beg >= begLut && beg < endLut && strncmpCompare->compare(pattern, txt + hT[3 * hash + 2], lcp) == 0) {
                        end = hT[3 * hash + 1];
//                    if (beg >= begLut && beg < endLut && strncmpCompare->compare(pattern, txt + sa[(beg + end) / 2], lcp) == 0) {
                            break;
                    }
 
                    hash++;
                    if (hash == hTLen) {
                            hash = 0;
                    }
            }
            pattern[lcp] = kHTChar;
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
            if (strncmp((char*) cUpdateLUTPrefix, (char*) pattern, ((LUTSearchStrategyClass*) lutSearch)->getLCP())) {
                ((LUTSearchStrategyClass*) lutSearch)->update(cUpdateLUTPrefix, cUpdateLUTBeg, beg);
                strncpy((char*) cUpdateLUTPrefix, (char*) pattern, ((LUTSearchStrategyClass*) lutSearch)->getLCP());
                cUpdateLUTBeg = beg;
            }
            cUpdateLUTEnd = end;
            
            unsigned int begLut, endLut;
            lutSearch->search(pattern, lcp, begLut, endLut);
            char kHTChar = pattern[lcp];
            pattern[lcp] = '\0';
            int hash = hashF(pattern) % hTLen;
            unsigned int oldBeg;
            while (true) {
                    oldBeg = hT[3 * hash];
                    if ((long long)oldBeg == this->txtSize) {
                        cout << "SA-Hash update miss-error!";
                        exit(0);
                    }
                    if (oldBeg >= begLut && oldBeg < endLut && strncmpCompare->compare(pattern, txt + hT[3 * hash + 2], lcp) == 0) {
                        updateHT[3 * hash] = beg;
                        updateHT[3 * hash + 1] = end;
                        updateHT[3 * hash + 2] = sa[beg];
                        break;
                    }
                    hash++;
                    if (hash == hTLen) {
                            hash = 0;
                    }
            }
            pattern[lcp] = kHTChar;    
        }
        
        inline void finalizeUpdate(unsigned int *sa) {
            ((LUTSearchStrategyClass*) lutSearch)->update(cUpdateLUTPrefix, cUpdateLUTBeg, cUpdateLUTEnd);
            this->sa = sa;
            delete[] hT;
            hT = updateHT;
            updateHT = 0;
        }
        
        inline int getLCP() { return lcp; }
    };

    using namespace kary;
    
    template<class StrNCmpComparisonStrategyClass, class LUTSearchStrategyClass, class SAVarKHashLookupClass>
    class SAVarKHashLookupTemplate: public SASearchStrategyInterface<SAVarKHashLookupClass> {
    protected:
        typedef unsigned int sa_uint;
        typedef SASearchStrategyInterface<LUTSearchStrategyClass> LUTSearchStrategy;
        typedef StringComparisonStrategyInterface<StrNCmpComparisonStrategyClass> StrNCmpComparisionStrategy;
        
        LUTSearchStrategy* lutSearch;
        StrNCmpComparisionStrategy* strncmpCompare;
        
        unsigned int expectedFilteredPrefixesStat, actualFilteredPrefixesStat;
        
        unsigned int expectedMaxRange;
        int maxLCP;
        sa_uint* sa;
        unsigned char* txt;
        long long saLen, txtSize;
        
        int elemShift;
        unsigned int* prefixFilter;
        long long  filterLen;
        
        int filterLoadFactor;
        
        long long hTLen;
        sa_uint *hT;
        
        int loadFactor;
        
        unsigned char* cUpdateLUTPrefix;
        unsigned int cUpdateLUTBeg = 1;
        unsigned int cUpdateLUTEnd = 0;
        sa_uint *updateHT;
        
        // statistics
        unsigned long hashF(unsigned char* str, size_t len) {
                //return rc_crc32(0, (const char *)str, strlen((const char *)str));
                //return murmur3_32(str, strlen((const char *)str), 0);
                //return djb2(str);
                //return sdbm(str);
                return XXH32(str, len, 0);
        }
        
        vector<int> findExpectedRanges() { 
            vector<int> idxs;
            int i = 0;
            while (this->sa[i] > (this->txtSize - this->maxLCP)) i++;
            idxs.push_back(i);
            int prevLCP = 0;
            while(i < this->saLen) {
                const unsigned char *lastPattern = (unsigned char*)(this->txt + this->sa[i]);
                int j = i + expectedMaxRange < this->saLen?(i + expectedMaxRange):this->saLen;
                int currLCP = strlcp((this->txt + this->sa[j]), lastPattern, this->maxLCP);
                if (currLCP < prevLCP)
                    currLCP = prevLCP;
                if (currLCP < this->maxLCP) {
                    j = i;
                    currLCP = this->upToValidLCP(currLCP + 1) - 1;
                } else 
                    currLCP = this->maxLCP - 1;
                while (++j < this->saLen && strlcp((this->txt + this->sa[j]), lastPattern, this->maxLCP) > currLCP);
                while (this->sa[j] > (this->txtSize - this->maxLCP)) j++;
                idxs.push_back(j);
                prevLCP = strlcp(this->txt + this->sa[j], lastPattern, this->maxLCP);
                i = j;
            }
            return idxs;
        }
        
        inline int findHashedPrefixLength(unsigned char* prefix, unsigned int& fullHash) { return static_cast<SAVarKHashLookupClass*>(this)->findHashedPrefixLengthImpl(prefix, fullHash); }        

        virtual bool isValidPrefix(int lcp) = 0;
        
        int countConsideredNotLongerPrefixes(int lcp, int commonPrevLCP) {
            int count = 0;
            while (lcp > commonPrevLCP) {
                if (lcp < this->maxLCP && isValidPrefix(lcp))
                    count++;
                lcp--;
            };
            return count;
        }

        int downToValidLCP(int lcp) {
            while (!isValidPrefix(lcp)) lcp--;
            return lcp;
        }
        
        int upToValidLCP(int lcp) {
            while (!isValidPrefix(lcp)) lcp++;
            return lcp;
        }
            
        inline bool isPrefixHashed(unsigned char* prefix, int len, unsigned int& fullHash) {
                fullHash = this->hashF(prefix, len);
                unsigned int hash = fullHash % this->filterLen;
                long long elemIdx = hash >> this->elemShift;
                return (this->prefixFilter[elemIdx] >> (hash - (elemIdx << this->elemShift)) & 1);
        }
            
        long long countPrefixFilterLen(vector<int> &begIdxs) {
            long long count = 0;
            int commonPrevLCP = 0;
            for (int i = 0; i < begIdxs.size() - 1; i++) {
                int currLCP = strlcp(this->txt + this->sa[begIdxs[i]], this->txt + this->sa[begIdxs[i+1]], this->maxLCP);
                int lcp = (currLCP>commonPrevLCP?currLCP:commonPrevLCP) + 1;
                count += countConsideredNotLongerPrefixes(upToValidLCP(lcp), commonPrevLCP);
                commonPrevLCP = currLCP;
            }
            long long len = (long long)((double)count * (100.0 / (double)filterLoadFactor)); 
            return len;
        }
        
        void fillPrefixFilter(vector<int> &begIdxs) {
            unsigned int hash;
            prefixFilter =  new unsigned int[(filterLen >> elemShift) + 1]();
            int commonPrevLCP = 0;
            for (int i = 0; i < begIdxs.size() - 1; i++) {
                unsigned char* pattern = this->txt + this->sa[begIdxs[i]];
                int currLCP = strlcp(pattern, this->txt + this->sa[begIdxs[i+1]], this->maxLCP);
                int lcp = (currLCP>commonPrevLCP?currLCP:commonPrevLCP) + 1;
                lcp = upToValidLCP(lcp);
                if (lcp < maxLCP) {
                    hash = hashF(pattern, lcp) % filterLen;
                    long long elemIdx = hash >> elemShift;
                    prefixFilter[elemIdx] |= 1 << (hash - (elemIdx << elemShift));
                }
                commonPrevLCP = currLCP;
            }
        }
        
        long long countHTLen(vector<int> &begIdxs) {
            long long hTLen = 0;
            int i = 0;
            int j = 0;
            while(i < begIdxs.size() - 1) {
                ++hTLen;
                unsigned char *suffix = (unsigned char*)(txt + sa[begIdxs[i]]);
                assert(sa[begIdxs[i]] <= txtSize - maxLCP);
                unsigned int voidHash = 0;
                int currLCP = findHashedPrefixLength(suffix, voidHash);
                if (currLCP <= ((LUTSearchStrategyClass*) lutSearch)->getLCP()) {
                    currLCP = ((LUTSearchStrategyClass*) lutSearch)->getLCP();
                    hTLen--;
                }
                while (++i < begIdxs.size() - 1 && strlcp((txt + sa[begIdxs[i]]), suffix, maxLCP) >= currLCP);
                begIdxs[++j] = begIdxs[i];
            }
            begIdxs.resize(++j);
            hTLen = (long long)((double)hTLen * (100.0 / (double)loadFactor));
            return hTLen;
        }
        
        void fillSAHashTable(vector<int> &begIdxs) {
            unsigned int hash = hTLen;
            hT = new sa_uint[2 * hTLen];

            for (long long i = 0; i < 2 * hTLen; ++i) {
                    hT[i] = (sa_uint)saLen;
            }

            int i = -1;
            while(++i < begIdxs.size() - 1) {
                unsigned char *suffix = (unsigned char*)(txt + sa[begIdxs[i]]);
                if (hash != hTLen) {
                    int endIdx = begIdxs[i];
                    while (this->sa[endIdx - 1] > (sa_uint)(this->txtSize - maxLCP)) endIdx--;
                    hT[2 * hash + 1] = endIdx;
                }
                int currLCP = findHashedPrefixLength(suffix, hash);
                if (currLCP == ((LUTSearchStrategyClass*) lutSearch)->getLCP()) {
                    hash = hTLen;
                    continue;
                }
                hash %= hTLen;
                while (true) {
                    if (hT[2 * hash] == (unsigned long long)saLen) {
                        hT[2 * hash] = begIdxs[i];
                        break;
                    }
                    else {
                        hash = (hash + 1) % hTLen;
                    }
                } 
            }
        }
        
    public:
        SAVarKHashLookupTemplate(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int maxLCP, unsigned int expectedMaxRange, int filterLoadFactor, int loadFactor, string variantParams)
        : SASearchStrategyInterface<SAVarKHashLookupClass>("hash" + toString(loadFactor) + 
                "(fLF=" + toString(filterLoadFactor) + "," + variantParams + ",maxER=" + toString(expectedMaxRange) + "," + StrNCmpComparisonStrategyClass::getInstance()->getStrategyName() + ")", 0),
                lutSearch(new LUTSearchStrategyClass(sa, txt, txtSize)),
                strncmpCompare(StrNCmpComparisonStrategyClass::getInstance()),
                filterLoadFactor(filterLoadFactor),
                maxLCP(maxLCP),
                sa(sa),
                txt(txt),
                saLen(txtSize),
                txtSize(txtSize),
                loadFactor(loadFactor),
                expectedMaxRange(expectedMaxRange) {
        }
        
        void init() {
            elemShift = 0;
            while ((1 << (++elemShift)) < sizeof(unsigned int) * 8);
            vector<int> rangesBegIdx = findExpectedRanges();
            expectedFilteredPrefixesStat = rangesBegIdx.size();
            filterLen = countPrefixFilterLen(rangesBegIdx);
            fillPrefixFilter(rangesBegIdx);
            hTLen = countHTLen(rangesBegIdx);
            actualFilteredPrefixesStat = rangesBegIdx.size();
            fillSAHashTable(rangesBegIdx);
            this->name +=  " on " + lutSearch->getStrategyName();
            this->sizeInBytes += lutSearch->getSizeInBytes();
            this->sizeInBytes += 2 * hTLen * sizeof(sa_uint);
            this->sizeInBytes += filterLen / 8;
                    
            cUpdateLUTPrefix = new unsigned char[((LUTSearchStrategyClass*) lutSearch)->getLCP() + 1]();
            cUpdateLUTBeg = 1;
            updateHT = new sa_uint[2 * hTLen];
            for(int i = 0; i < 2 * hTLen; i++)
                updateHT[i] = hT[i];
        }
        
        inline void useAddress4DataImpl(unsigned int* addr) {
            sa = addr;
        }
        
        ~SAVarKHashLookupTemplate() {
            delete(hT);
            delete(updateHT);
            delete(prefixFilter);
        }
               
        string getParamsString() { 
            return toString(expectedMaxRange) + " " + toString(filterLoadFactor) + " " + toString(loadFactor);
        }
        
        bool isValidFor(unsigned char* pattern, int patternLength) { 
            return maxLCP <= patternLength;
        }   
        
        inline void searchImpl(unsigned char* pattern, int patternLength, unsigned int &beg, unsigned int &end) {
            unsigned int begLut, endLut;
            lutSearch->search(pattern, patternLength, begLut, endLut);
            unsigned int hash;
            int currLCP = findHashedPrefixLength(pattern, hash);
            if (currLCP == ((LUTSearchStrategyClass*) lutSearch)->getLCP()) {
                beg = begLut; end = endLut;
                return;
            }    
            hash %= hTLen;
            while (true) {
                    beg = hT[2 * hash];
                    if ((long long)beg == this->txtSize) {
                            beg = 0; end = 0;
                            break;
                    }
                    if (beg >= begLut && beg < endLut && strncmpCompare->compare(pattern, txt + sa[beg], currLCP) == 0) {
                        end = hT[2 * hash + 1];
                            break;
                    }
 
                    hash++;
                    if (hash == hTLen) {
                        hash = 0;
                    }
            }
        }
        
        inline void update(unsigned char* pattern, unsigned int beg, unsigned int end) {
            std::exit(-1); // not implemented correctly
            if (strncmp((char*) cUpdateLUTPrefix, (char*) pattern, ((LUTSearchStrategyClass*) lutSearch)->getLCP())) {
                ((LUTSearchStrategyClass*) lutSearch)->update(cUpdateLUTPrefix, cUpdateLUTBeg, beg);
                strncpy((char*) cUpdateLUTPrefix, (char*) pattern, ((LUTSearchStrategyClass*) lutSearch)->getLCP());
                cUpdateLUTBeg = beg;
            }
            cUpdateLUTEnd = end;
            
            unsigned int begLut, endLut;
            lutSearch->search(pattern, maxLCP, begLut, endLut);
            char kHTChar = pattern[maxLCP];
            pattern[maxLCP] = '\0';
            int hash = hashF(pattern) % hTLen;
            unsigned int oldBeg;
            while (true) {
                    oldBeg = hT[2 * hash];
                    if ((long long)oldBeg == this->txtSize) {
                        cout << "SA-Hash update miss-error!";
                        exit(0);
                    }
                    if (oldBeg >= begLut && oldBeg < endLut && strncmpCompare->compare(pattern, txt + sa[oldBeg], maxLCP) == 0) {
                        updateHT[2 * hash] = beg;
                        updateHT[2 * hash + 1] = end;
                        break;
                    }
                    hash++;
                    if (hash == hTLen) {
                            hash = 0;
                    }
            }
            pattern[maxLCP] = kHTChar;    
        }
        
        inline void finalizeUpdate(unsigned int *sa) {
            ((LUTSearchStrategyClass*) lutSearch)->update(cUpdateLUTPrefix, cUpdateLUTBeg, cUpdateLUTEnd);
            this->sa = sa;
            delete[] hT;
            hT = updateHT;
            updateHT = 0;
        }
    };
    
    
    template<class StrNCmpComparisonStrategyClass, class LUTSearchStrategyClass>
    class SARangeKHashLookup: public SAVarKHashLookupTemplate<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass, SARangeKHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> {
        private:
            int minLCP;
        
            typedef SAVarKHashLookupTemplate<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass, SARangeKHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> BaseClass;
           
        protected:
            
            bool isValidPrefix(int lcp) {
                return lcp>=this->minLCP && lcp<=this->maxLCP;
            }
            
        public:

            inline int findHashedPrefixLengthImpl(unsigned char* prefix, unsigned int& fullHash) {
                int currLCP = this->minLCP - 1;
                while (++currLCP < this->maxLCP && !this->isPrefixHashed(prefix, currLCP, fullHash));
                if (currLCP == this->maxLCP) 
                    fullHash = this->hashF(prefix, this->maxLCP);
                return currLCP;
            }
            
            SARangeKHashLookup(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int minLcp, int maxLcp, unsigned int expectedMaxRange, int filterLoadFactor, int loadFactor)
            : BaseClass(sa, txt, txtSize, maxLcp, expectedMaxRange, filterLoadFactor, loadFactor,
                    "k=" + toString(minLcp) + "..." + toString(maxLcp)),
                    minLCP(minLcp){
                this->init();
            }
            
            ~SARangeKHashLookup() {}
            
            string getParamsString() { 
                return toString(this->minLCP) + " " + toString(this->maxLCP) + " " + BaseClass::getParamsString();
            }
            
            inline int getLCP() { return minLCP; }
    };
    
    template<class StrNCmpComparisonStrategyClass, class LUTSearchStrategyClass>
    class SATwoKHashLookup: public SAVarKHashLookupTemplate<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass, SATwoKHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> {
        private:
            int minLCP;
        
            typedef SAVarKHashLookupTemplate<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass, SATwoKHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> BaseClass;
           
        protected:

            bool isValidPrefix(int lcp) {
                return (lcp==this->minLCP) || (lcp==this->maxLCP);
            }
            
        public:

            inline int findHashedPrefixLengthImpl(unsigned char* prefix, unsigned int& fullHash) {
                if (this->isPrefixHashed(prefix, this->minLCP, fullHash))
                    return this->minLCP;
                fullHash = this->hashF(prefix, this->maxLCP);
                return this->maxLCP;
            }
            
            SATwoKHashLookup(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int minLcp, int maxLcp, int expectedMaxRange, int filterLoadFactor, int loadFactor)
            : BaseClass(sa, txt, txtSize, maxLcp, expectedMaxRange, filterLoadFactor, loadFactor,
                    "k=" + toString(minLcp) + ";" + toString(maxLcp)),
                    minLCP(minLcp) {
                this->init();
            }
            
            ~SATwoKHashLookup() {}
            
            string getParamsString() { 
                return toString(this->minLCP) + " " + toString(this->maxLCP) + " " + BaseClass::getParamsString();
            }
            
            inline int getLCP() { return minLCP; }
    };
    
    template<class StrNCmpComparisonStrategyClass, class LUTSearchStrategyClass>
    class SAAutoKHashLookup: public SAVarKHashLookupTemplate<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass, SAAutoKHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> {
        private:
            int kCount;
            vector<int> ks;
            vector<bool> isValidK;
            
            typedef SAVarKHashLookupTemplate<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass, SAAutoKHashLookup<StrNCmpComparisonStrategyClass, LUTSearchStrategyClass>> BaseClass;
           
            void selectKs(bool useLUTlcp) {
                isValidK.resize(this->maxLCP + 1);
                fill(isValidK.begin(), isValidK.end(), true);
                vector<int> begIdxs = this->findExpectedRanges();
                vector<unsigned int> rangesCountPerLCP(this->maxLCP + 1, 0);
                int commonPrevLCP = 0;
                for (int i = 0; i < begIdxs.size() - 1; i++) {
                    int currLCP = strlcp(this->txt + this->sa[begIdxs[i]], this->txt + this->sa[begIdxs[i+1]], this->maxLCP);
                    int lcp = (currLCP>commonPrevLCP?currLCP:commonPrevLCP) + 1;
                    rangesCountPerLCP[lcp]++;
                    commonPrevLCP = currLCP;
                }
                int lutLCP = ((LUTSearchStrategyClass*) this->lutSearch)->getLCP();
                for(int i = 1; i < lutLCP; i++) {
                    isValidK[i] = false;
                    rangesCountPerLCP[lutLCP] += rangesCountPerLCP[i];
                }
                int minLCP = lutLCP + (useLUTlcp?1:0);
                int validKCount = this->maxLCP - lutLCP + 1;
                while (validKCount-- > kCount) {
                    unsigned int minSum = this->txtSize;
                    int lastLCP = minLCP;
                    int smallerIdx, smallIdx;
                    for(size_t i = lastLCP + 1; i <= this->maxLCP; ++i) 
                        if (isValidK[i]) {
                            int sum = rangesCountPerLCP[lastLCP] + rangesCountPerLCP[i];
                            if (sum < minSum) {
                                minSum = sum;
                                if (rangesCountPerLCP[lastLCP] > rangesCountPerLCP[i]) {
                                    smallerIdx = i;
                                    smallIdx = lastLCP;
                                } else {
                                    smallerIdx = lastLCP;
                                    smallIdx = i;
                                }
                            }
                            lastLCP = i;
                        }
                    rangesCountPerLCP[smallIdx] += rangesCountPerLCP[smallerIdx];
                    isValidK[smallerIdx] = false;
                    if (smallerIdx == minLCP)
                        minLCP = smallIdx;
                    else if (smallerIdx == this->maxLCP)
                        this->maxLCP = smallIdx;
                }
                for (int i = lutLCP; i <= this->maxLCP; i++) {
                    if (isValidK[i])
                        ks.push_back(i);                
                };
            }
            
        protected:

            bool isValidPrefix(int lcp) {
                return isValidK[lcp];
            }
            
        public:

            inline int findHashedPrefixLengthImpl(unsigned char* prefix, unsigned int& fullHash) {
                int i = -1;
                while (ks[++i] < this->maxLCP && !this->isPrefixHashed(prefix, ks[i], fullHash));
                if (ks[i] == this->maxLCP) 
                    fullHash = this->hashF(prefix, this->maxLCP);
                return ks[i];
            }
            
            SAAutoKHashLookup(unsigned int *sa, unsigned char* txt, unsigned int txtSize, int kCount, int maxLcp, int expectedMaxRange, int filterLoadFactor, int loadFactor)
            : BaseClass(sa, txt, txtSize, maxLcp, expectedMaxRange, filterLoadFactor, loadFactor,
                    "k(" + toString(kCount) + ")=..." + toString(maxLcp)),
                    kCount(kCount) {
                selectKs(false);
                this->init();
            }
            
            ~SAAutoKHashLookup() {}
            
            string getParamsString() { 
                std::stringstream ss;
                for(size_t i = 0; i < ks.size(); ++i)
                {
                  if(i != 0)
                    ss << " ";
                  ss << ks[i];
                }
                return toString(kCount) + " " + ss.str() + " " + BaseClass::getParamsString();
            }
            
            inline int getLCP() { return ks[0]; }
    };
}

#endif	/* SAHASHSTRATEGIES_H */
