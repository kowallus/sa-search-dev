/* 
 * File:   testKaryLCPSASuite.h
 * Author: coach
 *
 * Created on 7 grudnia 2015, 09:54
 */

#ifndef TESTKARYLCPSASUITE_H
#define	TESTKARYLCPSASUITE_H

#include "testingRoutines.h"

template<class StrCmpStrategy, int PlusK>
void karyLCPSAVariantsSelectedTestsSet();

template<class PrefixesStrCmpStrategy, int PlusK>
void karyLCPmixSAVariantsSelectedTestsSet();

template<class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPSAVariantsMinimalTestsSet();

template<class StrCmpStrategy, int PlusK>
void karyLCPSAVariantsPlannedTestsSet() {
    cout << endl << "K-ary LCPSA final variants " << endl;;

    testSASearchStrategy<KaryLCPSASearch<25, 4, 4, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<16, 3, 5, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<41, 6, 4, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<3, 1, 14, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<19, 3, 6, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    
    // ADDITIONAL - WEAKER? VARIANTS
    testSASearchStrategy<KaryLCPSASearch<3, 1, 13, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<3, 1, 15, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
}

template<class StrCmpStrategy, int PlusK>
void karyLCPSAVariantsDiffPrefixPlannedTestsSet() {
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 1, PlusK>();
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 2, PlusK>();
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 3, PlusK>();
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 4, PlusK>();    
}

template<class StrCmpStrategy>
void karyLCPSAVariantsPlusKPlannedTestsSet() {
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 2, 0>();
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 2, 1>();
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 2, 2>();
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 2, 4>();
    karyLCPSAVariantsMinimalTestsSet<StrCmpStrategy, 2, 8>();
}


template<class StrCmpStrategy, int PlusK>
void karyLCPSAVariantsSameSizePlannedTestsSet() {
    testSASearchStrategy<KaryLCPSASearch<7, 2, 8, 1, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<41, 6, 4, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<18, 3, 5, 3, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<17, 3, 5, 4, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<29, 5, 5, 1, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 2, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<23, 4, 5, 3, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<13, 3, 6, 4, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
}

template<class StrCmpStrategy, int prefixLengthInUints, int PlusK>
void karyLCPSAVariantsMinimalTestsSet() {
    testSASearchStrategy<KaryLCPSASearch<3, 1, 13, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<3, 1, 14, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 4, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, prefixLengthInUints, BSwap32StrNECmp, StrCmpStrategy, PlusK>>();
}

template<class PrefixesStrCmpStrategy>
void preliminaryKaryLCPSAVariantsTestsSet() {
      karyLCPSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 0>();
      karyLCPSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 1>();
      karyLCPSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 2>();
      karyLCPSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 4>();

      karyLCPmixSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 0>();
      karyLCPmixSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 1>();
      karyLCPmixSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 2>();
      karyLCPmixSAVariantsSelectedTestsSet<PrefixesStrCmpStrategy, 4>();
}

template<class StrCmpStrategy>
void variousKaryLCPSAVariantsTestsSet() {
    testSASearchStrategy<KaryLCPSASearch<16, 3, 5, 2, BSwap32StrNECmp, StrCmpStrategy>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 2, BSwap32StrNECmp, StrCmpStrategy>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 2, BSwap32StrNECmp, StrCmpStrategy, 2>>();
}

template<class PrefixesStrCmpStrategy, int PlusK>
void karyLCPSAVariantsSelectedTestsSet() {
    cout << endl << "K-ary LCPSA variants " << endl;;

    testSASearchStrategy<KaryLCPSASearch<16, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<16, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<16, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
  
    testSASearchStrategy<KaryLCPSASearch<24, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<24, 4, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<24, 4, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 4, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<25, 4, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();

    testSASearchStrategy<KaryLCPSASearch<29, 5, 3, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<29, 5, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();

    testSASearchStrategy<KaryLCPSASearch<41, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<41, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();

    
/*    testSASearchStrategy<KaryLCPSASearch<4, 1, 9, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<4, 1, 9, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<4, 1, 10, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<4, 1, 10, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<4, 1, 11, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<4, 1, 11, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<13, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<13, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<13, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<13, 3, 6, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<13, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<13, 3, 6, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<13, 3, 6, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<14, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<14, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<14, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<14, 3, 6, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<14, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<14, 3, 6, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
       
    testSASearchStrategy<KaryLCPSASearch<17, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<17, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<17, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<17, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<17, 3, 6, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<17, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<18, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<19, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<19, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<19, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<19, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<20, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<20, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<20, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<20, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<21, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<21, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
        
    testSASearchStrategy<KaryLCPSASearch<22, 3, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<22, 3, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<22, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<22, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<22, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<22, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<23, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<23, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<26, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<27, 4, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<27, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<27, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<28, 5, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<28, 5, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<28, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<29, 5, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<30, 5, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<30, 5, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<36, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<36, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<36, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<37, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<37, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<37, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<38, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<38, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<38, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<39, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<39, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<39, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<40, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<40, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<40, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPSASearch<41, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    // CONTROL - WEAKER VARIANTS
    
    testSASearchStrategy<KaryLCPSASearch<2, 1, 24, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<3, 1, 15, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<6, 2, 9, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPSASearch<29, 4, 3, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
*/
    
      testSASearchStrategy<KaryLCPSASearch<4, 1, 12, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<4, 1, 12, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<6, 2, 9, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<6, 2, 9, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<13, 3, 7, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<13, 3, 7, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<16, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<16, 3, 6, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<16, 3, 6, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<17, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<18, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<19, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<29, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<29, 4, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<29, 4, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<31, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<31, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<32, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<32, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<33, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<33, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<34, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPSASearch<34, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();    
    
}    
    
template<class PrefixesStrCmpStrategy, int PlusK>
void karyLCPmixSAVariantsSelectedTestsSet() {
    cout << endl << "K-ary LCPmixSA variants " << endl;;

    testSASearchStrategy<KaryLCPmixSASearch<16, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<16, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<16, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<24, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<24, 4, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<24, 4, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 4, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<25, 4, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<29, 5, 3, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<29, 5, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<41, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<41, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();

/*    
    testSASearchStrategy<KaryLCPmixSASearch<4, 1, 9, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<4, 1, 9, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<4, 1, 10, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<4, 1, 10, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<4, 1, 11, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<4, 1, 11, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<13, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<13, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<13, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<13, 3, 6, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<13, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<13, 3, 6, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<13, 3, 6, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<14, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<14, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<14, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<14, 3, 6, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<14, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<14, 3, 6, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    
    testSASearchStrategy<KaryLCPmixSASearch<17, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<17, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<17, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<17, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<17, 3, 6, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<17, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<18, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<19, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<19, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<19, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<19, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<20, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<20, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<20, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<20, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<21, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<21, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
        
    testSASearchStrategy<KaryLCPmixSASearch<22, 3, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<22, 3, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<22, 3, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<22, 3, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<22, 3, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<22, 3, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<23, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<23, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<26, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<27, 4, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<27, 4, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<27, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<28, 5, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<28, 5, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<28, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<29, 5, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<30, 5, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<30, 5, 5, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<36, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<36, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<36, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<37, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<37, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<37, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<38, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<38, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<38, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<39, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<39, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<39, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<40, 6, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<40, 6, 4, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<40, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    testSASearchStrategy<KaryLCPmixSASearch<41, 6, 4, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    
    // CONTROL - WEAKER VARIANTS
    
    testSASearchStrategy<KaryLCPmixSASearch<2, 1, 24, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<3, 1, 15, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<6, 2, 8, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
    testSASearchStrategy<KaryLCPmixSASearch<29, 4, 4, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
*/

      testSASearchStrategy<KaryLCPmixSASearch<4, 1, 12, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<4, 1, 12, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<6, 2, 9, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<6, 2, 9, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<13, 3, 7, 1, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<13, 3, 7, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<16, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<16, 3, 6, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<16, 3, 6, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<17, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<18, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<19, 3, 6, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<29, 4, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<29, 4, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<29, 4, 5, 4, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<31, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<31, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<32, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<32, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<33, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<33, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<34, 5, 5, 2, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();
      testSASearchStrategy<KaryLCPmixSASearch<34, 5, 5, 3, PrefixesStrCmpStrategy, AsmLibStrCmp, PlusK>>();    
    
}

#endif	/* TESTKARYLCPSASUITE_H */

