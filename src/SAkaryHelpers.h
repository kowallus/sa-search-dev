/* 
 * File:   SAkaryHelpers.h
 * Author: coach
 *
 * Created on 31 maja 2015, 21:44
 */

#ifndef SAKARYHELPERS_H
#define	SAKARYHELPERS_H

#include "SAHelpers.h"

#define MIN_SIZE_FOR_KARY_LAYOUT 128
#define MAX_SIZE_FOR_LINEAR_SEARCH 16
#define MAXKARYLEVEL 32

namespace SASearch {    
    namespace kary {

        unsigned int karyNodes(int k, int levelsCount);
        
        template<int prefixLengthInUints>
        using prefixarray = unsigned char[prefixLengthInUints * sizeof(unsigned int)];
        
        template<int k>
        int karyLevels(int saSize) {
            int level = 1;
            unsigned long long elementsAtLevel = k - 1;
            unsigned long long int size = elementsAtLevel;
            while (saSize > size) {
                level++;
                elementsAtLevel *= k;
                size += elementsAtLevel;
            };
            return level;
        }
        
        template<int k>
        void calculateLevelsIndexes(unsigned int* offsetAtLevel, int saSize) {
            int level = 1;
            int elementsAtLevel = k - 1;
            offsetAtLevel[0] = 0;
            do {
                offsetAtLevel[level] = offsetAtLevel[level - 1] + elementsAtLevel;
                level++;
                elementsAtLevel *= k;
            } while (offsetAtLevel[level - 1] <= saSize);
            offsetAtLevel[level] = saSize;
        }
        
        template<int k>
        inline int node2index(int node) {
            return node * (k - 1);
        }
        
        template<int k>
        inline int index2node(int i) {
            return i / (k - 1);
        }
        
        template<int k>
        inline int childNode(int node, int c) {
            return k * node + 1 + c;
        }
        
        template<int k>
        inline int parentNode(int node) {
            return (node - 1) / k;
        }
        
        template<int k>
        inline int numOfElementInNode(int index) {
            return index % (k - 1);
        }
        
        template<int k>
        inline int childNodePrecedingIndex(int i) {
            return childNode<k>(index2node<k>(i), numOfElementInNode<k>(i));
        }
        
        template<int k>
        inline int parentsChildNum(int node) {
            return (node - 1) % k;
        }
        

        template<int k>
        int getKaryIndexRank(unsigned int index, unsigned int saSize, unsigned int lastNode) {
            if (index >= saSize)
                return saSize;
            
            unsigned int levelsIndexes[MAXKARYLEVEL];
            calculateLevelsIndexes<k>(levelsIndexes, saSize);
            int levelsCount = karyLevels<k>(saSize);
            
            unsigned int indexLevel = 0;
            while (levelsIndexes[indexLevel + 1] <= index)
                indexLevel++;
            
            unsigned int rank = 0;
            unsigned int node = index2node<k>(index);
            unsigned int i = index;
            int level = indexLevel;
            
            do {
                rank += i - levelsIndexes[level];
                unsigned int c = parentsChildNum<k>(node);
                node = parentNode<k>(node);
                i = node2index<k>(node) + c;
            } while(level-- > 0);
                
            node = childNodePrecedingIndex<k>(index);
            level = indexLevel;
            while (++level < levelsCount) {
                if (node > lastNode) {
                    rank += saSize - levelsIndexes[level];
                    break;
                }
                i = node2index<k>(node) + k - 1;
                rank += ((i > saSize)?(saSize):i) - levelsIndexes[level];
                node = childNode<k>(node, k - 1);
            }
            return rank;
        }
        
        inline int strlcp(const unsigned char* strA, const unsigned char* strB, int length) {       
            int i = -1;

            while (++i < length) {
                if (*(strA++) != *(strB++))
                    return i;
            }

            return i;
        }
        
        inline int revstrlcp(const unsigned char* strA, const unsigned char* strB, int length) {       
            int i = -1;
            strA += length - 1;
            strB += length - 1;
                    
            while (++i < length) {
                if (*(strA--) != *(strB--))
                    return i;
            }

            return i;
        }
        
        inline int revSignedStrlcp(const unsigned char* strA, int shift, unsigned char* strB, int length) {       
            strB += length - 1;
            int i = -1;
            if (shift == 0)
                strA += length - 1;
            else {
                strA += shift + length - 1;
                if ((unsigned char)(*(strA--)-128) != *(strB--))
                    return 0;
                ++i;
            }

            while (++i < length) {
                if (*(strA--) != *(strB--))
                    return i;
            }

            return i;
        }
        
        template<int prefixLength>
        inline void prefixReverse(unsigned char *p)
        {
            for(unsigned char *q = p + (prefixLength-1); p < q; ++p, --q) {
                unsigned char tmp = *q;
                *q = *p;
                *p = tmp;
            }
        }
        
        template<int k>
        unsigned int getKaryPrefixesSizeInBytes(int prefixesLength, int prefixesLevelsCount, unsigned int size) {
            unsigned int levelsIndexes[MAXKARYLEVEL];
            calculateLevelsIndexes<k>(levelsIndexes, size);
            int levelsCount = karyLevels<k>(size);
            
            unsigned int prefixesCount;
            if (prefixesLevelsCount < levelsCount) 
                prefixesCount = levelsIndexes[prefixesLevelsCount];
            else 
                prefixesCount = size;
            
            return prefixesCount * prefixesLength * sizeof(char);
        }
    }
}

#endif	/* SAKARYHELPERS_H */

