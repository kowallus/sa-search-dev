#include "SAkaryHelpers.h"

namespace SASearch {    
    namespace kary {

        unsigned int karyNodes(int k, int levelsCount) {
            unsigned int nodes = 0;
            unsigned long long nodesAtLevel = 1;
            int level = 0;
            while (level < levelsCount) {
                nodes += nodesAtLevel;
                level++;
                nodesAtLevel *= k;
            };
            return nodes;
        }
    }
}